var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var minify = require('gulp-minify-css');

var dashboard_js = [
    'public/assets/js/wysihtml5/wysihtml5-0.3.0.js',
    'public/assets/js/wysihtml5/wysihtml5ParserRules.js',
    'public/assets/js/jquery.1.10.2.js',
    'public/assets/js/bootstrap.min.js',
    'public/assets/js/jquery.popconfirm.js',
    'public/assets/js/prettify.js',
    'public/assets/js/wysihtml5/handlebars.runtime.min.js',
    'public/assets/js/wysihtml5/bootstrap3-wysihtml5.all.min.js',
    'public/assets/js/jquery.dataTables.min.js',
    'public/assets/js/datatables-bootstrap.js',
    'public/assets/js/bootstrap-pagination.js',
    'public/assets/js/datatables.fnReloadAjax.js',
    'public/assets/js/jquery.colorbox.js',
    'public/assets/js/jquery.form.js',
    'public/assets/js/jQueryFileUpload956/js/vendor/jquery.ui.widget.js',
    'public/assets/js/jQueryFileUpload956/js/jquery.iframe-transport.js',
    'public/assets/js/jQueryFileUpload956/js/jquery.fileupload.js',
    'public/assets/js/editor.js',
    'public/assets/js/render.js'
];


var dashboard_css = [
    'public/assets/css/bootstrap.min.css',
    'public/assets/css/bootstrap-theme.min.css',
    'public/assets/css/wysihtml5/bootstrap3-wysihtml5.min.css',
    'public/assets/css/jquery.fileupload-ui.css',
    'public/assets/css/jquery.fileupload.css',
    'public/assets/css/cbs.css'
];

var admin_css = [
    'public/assets/css/bootstrap.min.css',
    'public/assets/css/bootstrap-theme.min.css',
    'public/assets/css/datatables-bootstrap.css',
    'public/assets/css/wysihtml5/bootstrap3-wysihtml5.min.css',
    'public/assets/css/wysihtml5/prettify.css',
    'public/assets/css/colorbox.css',
    'public/assets/css/admin.css'
];

gulp.task('scripts', function() {
    gulp.src(dashboard_js)
        .pipe(concat('dashboard.js'))
        .pipe(uglify())
        .pipe(rename('dashboard.min.js'))
        .pipe(gulp.dest('public/assets/js'));
});

gulp.task('css', function() {
    gulp.src(dashboard_css)
        .pipe(concat('dashboard.css'))
        .pipe(minify())
        .pipe(rename('dashboard.min.css'))
        .pipe(gulp.dest('public/assets/css'));

    gulp.src(admin_css)
        .pipe(concat('admin.css'))
        .pipe(minify())
        .pipe(rename('admin.min.css'))
        .pipe(gulp.dest('public/assets/css'));
});

gulp.task('watch',  function() {
    gulp.watch(['public/assets/js/*'], ['scripts']);
});

gulp.task('default', ['scripts', 'css'])
