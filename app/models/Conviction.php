<?php

class Conviction extends Eloquent {

    protected $softDelete = true;

    protected $fillable = array(
        'month_occured',
        'year_occured',
        'details',
        'sentence',
        'user_id'
    );

    public static $rules = array(
        'month_occured' => 'required',
        'year_occured' => 'required',
        'details' => 'required',
        'sentence' => 'required'
    );

    public function getRules()
    {
        return self::$rules;
    }
}
