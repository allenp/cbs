<?php

class Membership extends Eloquent {

    protected $softDelete = true;

    protected $fillable = array(
        'name',
        'description',
        'since',
        'user_id'
    );

    public static $rules = array(
        'name' => 'required',
        'description' => 'required',
        'since' => 'required',
    );

    public function getRules()
    {
        return self::$rules;
    }
}
