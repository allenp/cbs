<?php

class JobMessageStatus extends Eloquent
{
    protected $fillable = array(
        'status',
        'job_message_id'
    );
    public function message()
    {
        return $this->belongsTo('JobMessage');
    }
}
