<?php

use Robbo\Presenter\PresentableInterface;

class JobMessage extends Eloquent implements PresentableInterface
{

    public static $rules = array(
        'job_id' => 'required',
        'msg' => 'required'
    );

    protected $fillable = array( 
        'msg',
        'job_id',
        'user_id'
    );

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function files()
    {
        return $this->belongsToMany('Attachment')->withPivot(array('job_message_id', 'attachment_id'));
    }

    public function status()
    {
        return $this->hasOne('JobMessageStatus');
    }
        
    public function getPresenter()
    {
        return new JobMessagePresenter($this);
    }
}
