<?php

class Incident extends Eloquent {
	protected $guarded = array();

    public static $rules = array(
        'month_occured' => 'required',
        'year_occured' => 'required',
        'details' => 'required'
    );

    protected $fillable = array(
        'user_id',
        'details',
        'month_occured',
        'year_occured',
        'is_accident'
    );

    public function getRules()
    {
        return self::$rules;
    }
}
