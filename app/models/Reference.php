<?php

class Reference extends Eloquent {

    protected $softDelete = true;

    protected $fillable = array(
        'first_name',
        'last_name',
        'position',
        'company',
        'phone',
        'address',
        'user_id',
        'email'
    );

    public static $rules = array(
        'first_name' => 'required',
        'last_name' => 'required',
        'position' => 'required',
        'company' => 'required',
        'phone' => 'required'
    );

    public function getRules()
    {
        return self::$rules;
    }
}
