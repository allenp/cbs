<?php

use Robbo\Presenter\PresentableInterface;

class Job extends Eloquent implements PresentableInterface {

	protected $guarded = array();

    protected $softDelete = true;

    public static $draftRules = array(
        'title' => 'required',
        'description' => 'required',
        'location' => 'required',
        'country_id' => 'required',
        'email' => 'required'
    );

    public static $directRecruitRules = array(
        'primary_contact_name' => 'required',
        'primary_contact_phone' => 'required',
        'start_salary' => 'required',
        'end_salary' => 'required',
        'vacancy_count' => 'required',
    );

    protected $fillable = array(
        'company_id',
        'title',
        'description',
        'country_id',
        'location',
        'email',
        'summary',
        'start_salary',
        'end_salary',
        'country_id',
        'vacancy_count',
        'keywords',
        'comments',
        'status',
        'recruitment',
        'primary_contact_name',
        'primary_contact_phone',
        'second_contact_name',
        'second_contact_phone',
        'vacancy_type',
        'aim_of_position',
        'last_incumbent1',
        'last_incumbent2',
        'reason_vacant',
        'hire_start_dt',
        'supervisor_profile',
        'supervisor_expectation',
        'company_culture',
        'advertised_before',
        'advertised_dt',
        'other_agency',
        'other_candidates',
        'permanent',
        'temporary',
        'full_time',
        'part_time',
        'working_conditions',
        'benefits',
        'core_competence',
    );

    public function getDates()
    {
        return array(
            'created_at',
            'start_dt',
            'end_dt',
            'deleted_at',
            'updated_at'
        );
    }

    public function getPresenter()
    {
        return new JobPresenter($this);
    }

    public function getDRRules()
    {
        return self::$directRecruitRules;
    }

    public function getRules()
    {
        return self::$draftRules;
    }

    public function company()
    {
        return $this->belongsTo('User', 'company_id', 'id');
    }

    public function country()
    {
        return $this->belongsTo('Country');
    }

    public function messages()
    {
        return $this->hasMany('JobMessage');
    }
}
