<?php

class Currency extends Eloquent {

	protected $guarded = array();

    public static $rules = array(
        'id' => 'required',
        'name' => 'required',
    );

    protected $fillable = array(
        'id',
        'name',
        'default',
        'rate'
    );

    public function getRules()
    {
        return self::$rules;
    }
}
