<?php

class Health extends Eloquent {

    protected $softDelete = true;

    protected $fillable = array(
        'smoke',
        'drink',
        'allergies',
        'physical_limitations',
        'limitations',
        'treated',
        'current_treatment',
        'user_id'
    );

    public static $rules = array(
    );

    public function getRules()
    {
        return self::$rules;
    }
}
