<?php

class Transport extends Eloquent {

    protected $softDelete = true;

    protected $fillable = array(
        'has_license',
        'method',
        'license_no',
        'state_issued',
        'operator',
        'commercial',
        'chauffeur',
        'expiry_dt',
        'user_id'
    );

    public static $rules = array(
    );

    public function getRules()
    {
        return self::$rules;
    }
}
