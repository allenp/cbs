<?php

use Robbo\Presenter\PresentableInterface;

class Attachment extends Eloquent implements PresentableInterface
{
    protected $fillable = array(
        'user_id',
        'path',
        'size',
        'type',
    );

    public function getPresenter()
    {
        return new AttachmentPresenter($this);
    }
}
