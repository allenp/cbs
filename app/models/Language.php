<?php

class Language extends Eloquent {
	protected $guarded = array();

    protected $fillable = array(
        'name',
        'user_id'
    );

    public static $rules = array(
        'name' => 'required',
    );

    public function getRules()
    {
        return self::$rules;
    }
}
