<?php

class Certification extends Eloquent {

    protected $softDelete = true;

    protected $fillable = array(
        'name',
        'year_issued',
        'type',
        'user_id'
    );

    public static $rules = array(
        'name' => 'required',
        'year_issued' => 'required',
    );

    public function getRules()
    {
        return self::$rules;
    }
}
