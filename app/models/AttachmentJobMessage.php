<?php

class AttachmentJobMessage extends Eloquent
{
    protected $fillable = array(
        'job_message_id',
        'attachment_id'
    );

    public function job_message()
    {
        return $this->belongsTo('JobMessage');
    }
}
