<?php

class Applicant extends Eloquent {
	protected $guarded = array();

    public static $rules = array(
        'job_id' => 'required',
    );

    protected $fillable = array(
        'availability',
        'job_id',
        'desired_salary',
        'earliest_start',
        'user_id'
    );

    public function getRules()
    {
        return self::$rules;
    }
}
