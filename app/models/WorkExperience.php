<?php

use Robbo\Presenter\PresentableInterface;

class WorkExperience extends Eloquent implements PresentableInterface {

	protected $guarded = array();

    protected $fillable = array(
        'company',
        'address',
        'phone',
        'supervisor',
        'start_month',
        'start_year',
        'end_month',
        'end_year',
        'start_salary',
        'end_salary',
        'job_title',
        'reason_for_leaving',
        'responsibility',
        'user_id'
    );

    public static $rules = array(
        'company' => 'required',
        'address' => 'required',
        'phone' => 'required',
        'supervisor' => 'required',
        'start_month' => 'required',
        'start_year' => 'required',
        'end_month' => 'required',
        'end_year' => 'required',
        'start_salary' => 'required|numeric',
        'end_salary' => 'required|numeric',
        'job_title' => 'required',
        'responsibility' => 'required',
    );

    public function getRules()
    {
        return self::$rules;
    }

    public function getPresenter()
    {
        return new WorkExperiencePresenter($this);
    }

}
