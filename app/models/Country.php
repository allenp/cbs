<?php

class Country extends Eloquent {

	protected $guarded = array();

    public static $rules = array(
        'id' => 'required',
        'name' => 'required',
    );

    protected $fillable = array(
        'id',
        'name',
    );

    public function getRules()
    {
        return self::$rules;
    }
}
