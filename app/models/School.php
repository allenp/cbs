<?php

use Robbo\Presenter\PresentableInterface;

class School extends Eloquent implements PresentableInterface {

    protected $guarded = array();

    protected $softDelete = true;

    protected $fillable = array(
        'type',
        'name',
        'location',
        'level_attained',
        'start_month',
        'start_year',
        'end_month',
        'end_year',
        'details',
        'user_id',
        'major',
        'minor'
    );

    public static $rules = array(
        'type' => 'required',
        'name' => 'required',
        'location' => 'required',
        'level_attained' => 'required',
        'start_month' => 'required',
        'start_year' => array('numeric', 'required'),
        'end_month' => 'required',
        'end_year' => array('numeric', 'required')

    );

    public function getRules()
    {
        return self::$rules;
    }

    public function getPresenter()
    {
        return new SchoolPresenter($this);
    }

}
