<?php

class Skill extends Eloquent {
    
    protected $softDelete = true;

    protected $fillable = array(
        'name',
        'user_id',
        'hidden'
    );

    public static $rules = array(
        'name' => 'required',
    );

    public function getRules()
    {
        return self::$rules;
    }
}
