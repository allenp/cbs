<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class RemoveCanContactFromApplicantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('applicants', function(Blueprint $table)
		{
            $table->dropColumn('can_contact_current_employer');
            $table->dropColumn('self_completed');
            $table->dropColumn('work_hours');

            $table->enum('availability', array('full-time', 'part-time', 'full-or-part-time'))->default('full-time')->after('desired_salary');
            $table->date('earliest_start');
            $table->integer('user_id')->references('id')->on('users')->after('id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('applicants', function(Blueprint $table)
		{
            $table->boolean('can_contact_current_employer')->default(0)->after('id');
            $table->boolean('self_completed')->default(1)->after('can_contact_current_employer');
            $table->string('work_hours')->after('desired_salary')->nullable();
            $table->dropColumn('availability');
            $table->dropColumn('earliest_start');
            $table->dropColumn('user_id');
			
		});
	}

}
