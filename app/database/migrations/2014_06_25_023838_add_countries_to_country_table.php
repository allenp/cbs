<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddCountriesToCountryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        $countries = [
            ['id' => 'JAM', 'name' => 'Jamaica', 'default' => 1, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => 'TTO', 'name' => 'Trinidad and Tobago', 'default' => 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ];

        DB::table('countries')->insert($countries);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::table('countries')->where('id', '=', 'JAM')->delete();
        DB::table('countries')->where('id', '=', 'TTO')->delete();
	}

}
