<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddManageJobsToPermissionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        $permissions = array(
            array(
                'name' => 'manage_jobs',
                'display_name' => 'Manage Jobs',
            ),
            array(
                'name' => 'manage_applicants',
                'display_name' => 'Manage Applicants',
            ),
            array(
                'name' => 'manage_users',
                'display_name' => 'Manage Users'
            ),
            array(
                'name' => 'manage_roles',
                'display_name' => 'Manage Roles',
            ),
        );
        DB::table('permissions')->insert($permissions);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Permission::find_by_name('manage_jobs')->delete();
        Permission::find_by_name('manage_applications')->delete();
        Permission::find_by_name('manage_roles')->delete();
	}

}
