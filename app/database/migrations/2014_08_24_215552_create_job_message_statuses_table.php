<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJobMessageStatusesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_message_statuses', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('job_message_id')->references('id')->on('job_messages');
            $table->enum('status', array('DRAFT', 'SUBMITTED', 'APPROVED', 'REJECTED', 'RECRUITING', 'SHORTLIST', 'CLOSED'));
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('job_message_statuses');
    }

}
