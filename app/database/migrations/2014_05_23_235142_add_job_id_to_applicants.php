<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddJobIdToApplicants extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('applicants', function(Blueprint $table) {
			$table->integer('job_id')->after('user_id')->references('id')->on('jobs')->nullable();
		});
        DB::statement('UPDATE applicants, jobs SET applicants.job_id = jobs.id WHERE applicants.position_applied_for = jobs.title');
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('applicants', function(Blueprint $table) {
			$table->dropColumn('job_id');
		});
	}

}
