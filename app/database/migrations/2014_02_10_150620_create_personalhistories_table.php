<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePersonalhistoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('personalhistories', function(Blueprint $table) {
			$table->increments('id');
			$table->boolean('smoke');
			$table->boolean('drink');
			$table->boolean('allergies');
			$table->boolean('physical_limitations');
			$table->string('current_treatment');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('personalhistories');
	}

}
