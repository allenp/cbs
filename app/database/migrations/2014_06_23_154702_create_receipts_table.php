<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReceiptsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('receipts', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('payment_ref');
            $table->decimal('amount', 12, 2);
            $table->string('currency_id')->references('id')->on('currencies');
            $table->string('card_holder');
            $table->string('email');
            $table->string('paymethod', 32);
            $table->string('card', 26);
            $table->string('card_type', 16);
            $table->string('auth_code', 16);
            $table->boolean('email_sent')->default(0);
            $table->string('address1');
            $table->string('address2')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('country_id', 3)->references('id')->on('countries');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('receipts');
	}

}
