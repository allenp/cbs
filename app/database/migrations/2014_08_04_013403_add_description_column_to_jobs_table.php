<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddDescriptionColumnToJobsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('jobs', function(Blueprint $table)
		{
            $table->text('description')->after('id')->nullable();
		});

        DB::statement('update jobs set description = concat(roles,requires)');
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('jobs', function(Blueprint $table)
		{
            $table->dropColumn('description');
		});
	}
}
