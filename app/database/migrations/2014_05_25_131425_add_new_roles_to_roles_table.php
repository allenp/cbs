<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddNewRolesToRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        $role = new Role;
        $role->name = 'admin';
        $role->save();

        $role = new Role;
        $role->name = 'applicant';
        $role->save();

        $role = new Role;
        $role->name = 'consultant';
        $role->save();

        $role = new Role;
        $role->name = 'company';
        $role->save();
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::table('roles')->where('name', '=', 'admin')->delete();
        DB::table('roles')->where('name', '=', 'applicant')->delete();
        DB::table('roles')->where('name', '=', 'consultant')->delete();
        DB::table('roles')->where('name', '=', 'company')->delete();
	}

}
