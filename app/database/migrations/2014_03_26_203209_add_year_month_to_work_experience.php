<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddYearMonthToWorkExperience extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('work_experiences', function(Blueprint $table)
		{
            $table->integer('end_month')->after('end_dt')->nullable();
            $table->integer('end_year')->after('end_month')->nullable();
            $table->integer('start_month')->after('start_dt')->nullable();
            $table->integer('start_year')->after('start_month')->nullable();
		});
        
        $query = 'update work_experiences set end_year = YEAR(end_dt), ' .
            'end_month = MONTH(end_dt), ' .
            'start_year = YEAR(start_dt), ' .
            'start_month = MONTH(start_dt)';

        try
        {
            DB::statement($query);
        } catch(\Illuminate\Database\QueryException $ex)
        {
        }
		Schema::table('work_experiences', function(Blueprint $table)
		{
            $table->dropColumn('end_dt');
            $table->dropColumn('start_dt');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('work_experiences', function(BLueprint $table)
        {
            $table->date('end_dt')->after('end_month')->nullable();
            $table->date('start_dt')->after('end_dt')->nullable();
        });

        $query = 'update work_experiences set end_dt = SUBDATE(DATE_ADD(MAKEDATE(end_year, 1), INTERVAL(end_month)  MONTH), 1), '.
            'start_dt = DATE_ADD(MAKEDATE(start_year, 1), INTERVAL(start_month) -1 MONTH)';

        DB::statement($query);
		Schema::table('work_experiences', function(Blueprint $table)
		{
            $table->dropColumn('end_month');
            $table->dropColumn('end_year');
            $table->dropColumn('start_month');
            $table->dropColumn('start_year');
		});
	}

}
