<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddCascadeDeleteOnUsersTable extends Migration {

    var $tables = array(
            'applicants',
            //'assigned_roles',
            'certifications',
            'convictions',
            'healths',
            'incidents',
            'languages',
            'memberships',
            'references',
            'schools',
            'skills',
            'transports',
            'work_experiences'
        );

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //add cascade delete to foreign keys via users table
        foreach($this->tables as $table_name) {
            DB::statement("ALTER TABLE  `$table_name` CHANGE  `user_id`  `user_id` INT( 10 ) UNSIGNED NOT NULL");
            Schema::table($table_name,
                function(Blueprint $table) use ($table_name) {
                    //$table->dropForeign($table_name .'_user_id_foreign');
                    $table->foreign('user_id')
                        ->references('id')->on('users')->onDelete('cascade');
                }
            );
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //remove cascade delete to foreign keys via users table
        foreach($this->tables as $table_name) {
            Schema::table($table_name,
                function(Blueprint $table) use ($table_name) {
                    $table->dropForeign($table_name .'_user_id_foreign');
                    $table->foreign('user_id')->references('id')->on('users');
                }
            );
        }
    }

}
