<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Profiles', function(Blueprint $table) {
			$table->increments('id');
			$table->boolean('convicted');
			$table->string('convicted_comments');
			$table->integer('convicted_count');
			$table->string('convicted_offense');
			$table->date('convicted_dt');
			$table->string('sentence');
			$table->boolean('drivers_license');
			$table->string('means_of_transport');
			$table->string('license_no');
			$table->date('expiry_dt');
			$table->integer('accidents_in_three_yrs');
			$table->integer('violations_in_three_yrs');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Profiles');
	}

}
