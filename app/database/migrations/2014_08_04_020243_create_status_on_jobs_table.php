<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStatusOnJobsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::statement('update jobs set status = \'APPROVED\', end_dt = DATE_ADD(NOW(), INTERVAL 30 DAY), recruitment=1');
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('update jobs set status = \'NULL\'');
	}

}
