<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReceiptItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('receipt_items', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->integer('receipt_id')->references('id')->on('receipts')->onDelete('cascade');
            $table->decimal('cost', 12, 2);
            $table->decimal('discount', 12, 2);
            $table->decimal('tax', 12, 2);
            $table->string('coupon')->nullable();
            $table->decimal('total', 12, 2);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('receipt_items');
	}

}
