<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFirstLastToReferencesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('references', function(Blueprint $table)
		{
            $table->string('first_name')->nullable()->after('name');
            $table->string('last_name')->nullable()->after('first_name');
		});

        $query = 'update `references` set first_name = SUBSTRING_INDEX(SUBSTRING_INDEX(name, \' \', 1), \' \', -1),
           last_name = SUBSTRING_INDEX(SUBSTRING_INDEX(name, \' \', 2), \' \', -1)';
        DB::statement($query);
        Schema::table('references', function(Blueprint $table)
        {
            $table->dropColumn('name');
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('references', function(Blueprint $table)
		{
            $table->string('name')->nullable()->after('last_name');
        });

        $query = 'update `references` set name = CONCAT(`first_name`, \' \', `last_name`)';
        DB::statement($query);
		Schema::table('references', function(Blueprint $table)
		{
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
		});
	}

}
