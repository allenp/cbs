<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorkexperiencesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_experiences', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->string('company');
            $table->string('address');
            $table->string('supervisor');
            $table->string('job_title');
            $table->string('phone');
            $table->date('start_dt');
            $table->date('end_dt');
            $table->float('start_salary');
            $table->float('end_salary');
            $table->string('reason_for_leaving');
            $table->string('comment');
            $table->string('responsibility');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('work_experiences');
    }

}
