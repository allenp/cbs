<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJobMessagesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_messages', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('job_id')->references('id')->on('jobs');
            $table->integer('user_id')->refernces('id')->on('users');
            $table->text('msg');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('job_messages');
    }

}
