<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductCostTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_cost', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->decimal('cost',12,2);
            $table->string('currency_id')->references('id')->on('currencies')->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_cost');
	}

}
