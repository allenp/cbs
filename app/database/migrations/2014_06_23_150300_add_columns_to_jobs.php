<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddColumnsToJobs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('jobs', function(Blueprint $table)
		{
            $table->string('summary', 255)->nullable()->after('active');
            $table->decimal('start_salary', 12, 2)->nullable()->after('summary');
            $table->decimal('end_salary', 12, 2)->nullable()->after('start_salary');
            $table->integer('company_id')->nullable()->references('id')->on('users')->after('end_salary');
            $table->string('country_id', 3)->nullable()->references('id')->on('countries')->after('company_id');
            $table->string('location')->nullable()->after('country_id');
            $table->integer('vacancy_count')->nullable()->after('location');
            $table->string('keywords')->nullable()->after('vacancy_count');
            $table->string('email')->nullable()->after('keywords');
            $table->string('comments')->nullable()->after('email');
            $table->enum('status', array('DRAFT', 'SUBMITTED', 'APPROVED', 'REJECTED', 'RECRUITING', 'SHORTLIST', 'CLOSED'))->nullable()->after('comments');
            $table->integer('num_days')->nullable()->after('status');
            $table->timestamp('start_dt')->nullable()->after('num_days');
            $table->timestamp('end_dt')->nullable()->after('start_dt');
            $table->boolean('recruitment')->nullable()->after('end_dt');
            $table->string('primary_contact_name')->nullable()->after('recruitment');
            $table->string('primary_contact_phone')->nullable()->after('primary_contact_name');
            $table->string('second_contact_name')->nullable()->after('primary_contact_phone');
            $table->string('second_contact_phone')->nullable()->after('second_contact_name');
            $table->enum('vacancy_type', array('NEW', 'REPLACEMENT'))->nullable()->after('second_contact_phone');
            $table->string('aim_of_position')->nullable()->after('vacancy_type');
            $table->string('last_incumbent1')->nullable()->after('aim_of_position');
            $table->string('last_incumbent2')->nullable()->after('last_incumbent1');
            $table->string('reason_vacant')->nullable()->after('last_incumbent2');
            $table->timestamp('hire_start_dt')->nullable()->after('reason_vacant');
            $table->text('supervisor_profile')->nullable()->after('hire_start_dt');
            $table->text('supervisor_expectation')->nullable()->after('supervisor_profile');
            $table->text('company_culture')->nullable()->after('supervisor_expectation');
            $table->boolean('advertised_before')->nullable()->after('company_culture');
            $table->timestamp('advertised_dt')->nullable()->after('advertised_before');
            $table->string('other_agency')->nullable()->after('advertised_dt');
            $table->text('other_candidates')->nullable()->after('other_agency');
            $table->string('job_desc_attachment')->nullable()->after('other_candidates');
            $table->boolean('permanent')->nullable()->after('job_desc_attachment');
            $table->boolean('temporary')->nullable()->after('permanent');
            $table->boolean('full_time')->nullable()->after('temporary');
            $table->boolean('part_time')->nullable()->after('full_time');
            $table->text('working_conditions')->nullable()->after('part_time');
            $table->text('benefits')->nullable()->after('working_conditions');
            $table->text('core_competence')->nullable()->after('benefits');
            $table->boolean('confidential')->nullable()->default(0)->after('core_competence');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('jobs', function(Blueprint $table)
		{
            $table->dropColumn('recruitment');
            $table->dropColumn('primary_contact_name');
            $table->dropColumn('primary_contact_phone');
            $table->dropColumn('second_contact_name');
            $table->dropColumn('second_contact_phone');
            $table->dropColumn('vacancy_type');
            $table->dropColumn('aim_of_position');
            $table->dropColumn('last_incumbent1');
            $table->dropColumn('last_incumbent2');
            $table->dropColumn('reason_vacant');
            $table->dropColumn('hire_start_dt');
            $table->dropColumn('supervisor_profile');
            $table->dropColumn('supervisor_expectation');
            $table->dropColumn('company_culture');
            $table->dropColumn('advertised_before');
            $table->dropColumn('advertised_dt');
            $table->dropColumn('other_agency');
            $table->dropColumn('other_candidates');
            $table->dropColumn('job_desc_attachment');
            $table->dropColumn('permanent');
            $table->dropColumn('temporary');
            $table->dropColumn('full_time');
            $table->dropColumn('part_time');
            $table->dropColumn('working_conditions');
            $table->dropColumn('benefits');
            $table->dropColumn('core_competence');
            $table->dropColumn('confidential');

			$table->dropColumn('summary');
			$table->dropColumn('start_salary');
			$table->dropColumn('end_salary');
			$table->dropColumn('company_id');
			$table->dropColumn('country_id');
            $table->dropColumn('location');
            $table->dropColumn('vacancy_count');
			$table->dropColumn('keywords');
			$table->dropColumn('email');
			$table->dropColumn('comments');
			$table->dropColumn('status');
			$table->dropColumn('num_days');
			$table->dropColumn('start_dt');
			$table->dropColumn('end_dt');
		});
	}

}
