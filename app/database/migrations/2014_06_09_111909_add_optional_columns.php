<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddOptionalColumns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::statement("ALTER TABLE  `work_experiences` CHANGE  `start_salary`  `start_salary` FLOAT( 8, 2 ) NOT NULL DEFAULT  '0';");
        DB::statement("ALTER TABLE  `work_experiences` CHANGE  `end_salary`  `end_salary` FLOAT( 8, 2 ) NOT NULL DEFAULT  '0';");
        DB::statement("ALTER TABLE  `work_experiences` CHANGE  `reason_for_leaving`  `reason_for_leaving` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ;");
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement("ALTER TABLE  `work_experiences` CHANGE  `start_salary`  `start_salary` FLOAT( 8, 2 ) NOT NULL;");
        DB::statement("ALTER TABLE  `work_experiences` CHANGE  `end_salary`  `end_salary` FLOAT( 8, 2 ) NOT NULL;");
        DB::statement("ALTER TABLE  `work_experiences` CHANGE  `reason_for_leaving`  `reason_for_leaving` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ;");

	}

}
