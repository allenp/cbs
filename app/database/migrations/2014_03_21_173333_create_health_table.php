<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHealthTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::rename('personalhistories', 'healths');

		Schema::table('healths', function(Blueprint $table) {
			$table->engine = 'InnoDB';
            $table->string('limitations')->after('physical_limitations')->nullable();
            $table->boolean('treated')->after('limitations')->nullable();
            $table->integer('user_id')->after('id')->nullable();
            $table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::rename('healths', 'personalhistories');
	}

}
