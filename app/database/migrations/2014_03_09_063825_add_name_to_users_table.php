<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddNameToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table) {
            $table->string('first_name')->after('email')->nullable();
            $table->string('last_name')->after('first_name')->nullable();
            $table->string('middle_name')->after('last_name')->nullable();
            $table->string('maiden_name')->after('middle_name')->nullable();
            $table->string('cellphone')->after('maiden_name')->nullable();
            $table->string('homephone')->after('cellphone')->nullable();
            $table->string('sex')->after('homephone')->nullable();
            $table->string('nis')->after('sex')->nullable();
            $table->string('trn')->after('nis')->nullable();
            $table->string('resume')->after('trn')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table) {
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('middle_name');
            $table->dropColumn('maiden_name');
            $table->dropColumn('cellphone');
            $table->dropColumn('homephone');
            $table->dropColumn('sex');
            $table->dropColumn('nis');
            $table->dropColumn('trn');
            $table->dropColumn('resume');
		});
	}

}
