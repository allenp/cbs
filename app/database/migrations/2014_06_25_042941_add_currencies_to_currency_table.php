<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddCurrenciesToCurrencyTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $currencies = [
            [
            'id' => 'JMD',
            'name' => 'JMD',
            'default' => 1,
            'rate' => 0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            ],
            [
            'id' => 'USD',
            'name' => 'USD',
            'default' => 0,
            'rate' => 0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            ],
            [
            'id' => 'TTD',
            'name' => 'TTD',
            'default' => 0,
            'rate' => 0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
            ]
        ];
        DB::table('currencies')->insert($currencies);
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('currencies')->where('id', '=', 'JMD')->delete();
        DB::table('currencies')->where('id', '=', 'USD')->delete();
        DB::table('currencies')->where('id', '=', 'TTD')->delete();
    }

}
