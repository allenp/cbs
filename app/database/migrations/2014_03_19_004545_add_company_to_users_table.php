<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddCompanyToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table) {
            $table->string('user_type')->after('last_name')->default('applicant');
            $table->string('company')->after('user_type')->nullable();
            $table->string('title')->after('company')->nullable();
            $table->string('address1')->after('resume')->nullable();
            $table->string('address2')->after('address1')->nullable();
            $table->string('address3')->after('address2')->nullable();
            $table->string('city')->after('address3')->nullable();
            $table->string('parish')->after('city')->nullable();
            $table->string('how_long')->after('parish')->nullable();
            $table->string('workphone')->after('parish')->nullable();
            $table->string('fax')->after('workphone')->nullable();
            $table->string('contact_email')->after('fax')->nullable();
            $table->string('baddress1')->after('contact_email')->nullable();
            $table->string('baddress2')->after('baddress1')->nullable();
            $table->string('baddress3')->after('baddress2')->nullable();
            $table->string('bcity')->after('baddress2')->nullable();
            $table->string('bparish')->after('bcity')->nullable();
            $table->string('about', 1024)->after('contact_email')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table) {
            $table->dropColumn('user_type');
            $table->dropColumn('company');
            $table->dropColumn('title');
            $table->dropColumn('address1');
            $table->dropColumn('address2');
            $table->dropColumn('address3');
            $table->dropColumn('city');
            $table->dropColumn('parish');
            $table->dropColumn('how_long');
            $table->dropColumn('workphone');
            $table->dropColumn('fax');
            $table->dropColumn('contact_email');
            $table->dropColumn('baddress1');
            $table->dropColumn('baddress2');
            $table->dropColumn('baddress3');
            $table->dropColumn('bcity');
            $table->dropColumn('bcity');
            $table->dropColumn('bparish');
            $table->dropColumn('about');
		});
	}

}
