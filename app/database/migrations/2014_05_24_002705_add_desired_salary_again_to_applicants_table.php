<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddDesiredSalaryAgainToApplicantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('applicants', function(Blueprint $table) {
            DB::statement('ALTER TABLE applicants CHANGE COLUMN desired_salary desired_salary DECIMAL(12,2) NOT NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('applicants', function(Blueprint $table) {
            DB::statement('ALTER TABLE applicants CHANGE COLUMN desired_salary desired_salary DECIMAL(8,2) NOT NULL');
		});
	}

}
