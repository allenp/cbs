<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transports', function(Blueprint $table) {
            $table->engine = 'InnoDB';
			$table->increments('id');
            $table->integer('user_id')->references('id')->on('users');
            $table->boolean('has_license')->default(0);
            $table->string('method')->nullable();
            $table->string('license_no')->nullable();
            $table->string('state_issued')->nullable();
            $table->boolean('operator')->default(0);
            $table->boolean('commercial')->default(0);
            $table->boolean('chauffeur')->default(0);
            $table->date('expiry_dt');
            $table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transports');
	}

}
