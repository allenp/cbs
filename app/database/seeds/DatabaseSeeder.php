<?php

class DatabaseSeeder extends Seeder {

    public function run()
    {
        Eloquent::unguard();

        // Add calls to Seeders here
        $this->call('UsersTableSeeder');
        $this->call('PostsTableSeeder');
        $this->call('CommentsTableSeeder');
        $this->call('RolesTableSeeder');
        $this->call('PermissionsTableSeeder');
		$this->call('SchoolsTableSeeder');
		$this->call('AddressesTableSeeder');
		$this->call('SubjectsTableSeeder');
		$this->call('DegreesTableSeeder');
		$this->call('CertificationsTableSeeder');
		$this->call('ProfilesTableSeeder');
		$this->call('SkillsTableSeeder');
		$this->call('ReferencesTableSeeder');
		$this->call('MembershipsTableSeeder');
		$this->call('PersonalhistoriesTableSeeder');
		$this->call('LanguagesTableSeeder');
		$this->call('WorkexperiencesTableSeeder');
		$this->call('ApplicantsTableSeeder');
	}

}
