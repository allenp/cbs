<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/** ------------------------------------------
 *  Route model binding
 *  ------------------------------------------
 */
if (!defined('ENV_PRODUCTION')) define('ENV_PRODUCTION', 'production');

Route::model('user', 'User');
Route::model('role', 'Role');
Route::model('job', 'Job');

/** ------------------------------------------
 *  Route constraint patterns
 *  ------------------------------------------
 */
Route::pattern('user', '[0-9]+');
Route::pattern('role', '[0-9]+');
Route::pattern('token', '[0-9a-z]+');
Route::pattern('status', '[0-9a-z]+');

/** ------------------------------------------
 *  Admin Routes
 *  ------------------------------------------
 */
Route::group(array('prefix' => 'admin', 'before' => 'auth'), function()
{

    # User Management
    Route::get('users/{user}/show', 'AdminUsersController@getShow');
    Route::get('users/{user}/edit', 'AdminUsersController@getEdit');
    Route::post('users/{user}/edit', 'AdminUsersController@postEdit');
    Route::get('users/{user}/delete', 'AdminUsersController@getDelete');
    Route::post('users/{user}/delete', 'AdminUsersController@postDelete');
    Route::controller('users', 'AdminUsersController');

    # User Role Management
    Route::get('roles/{role}/show', 'AdminRolesController@getShow');
    Route::get('roles/{role}/edit', 'AdminRolesController@getEdit');
    Route::post('roles/{role}/edit', 'AdminRolesController@postEdit');
    Route::get('roles/{role}/delete', 'AdminRolesController@getDelete');
    Route::post('roles/{role}/delete', 'AdminRolesController@postDelete');
    Route::controller('roles', 'AdminRolesController');

    #Jobs Management
    Route::get('jobs/{job}/show',    'AdminJobsController@getShow');
    Route::get('jobs/{job}/edit',    'AdminJobsController@getEdit');
    Route::get('jobs/{job}/delete',  'AdminJobsController@getDelete');
    Route::post('jobs/{job}/delete', 'AdminJobsController@postDelete');
    Route::post('jobs/{job}/edit',   'AdminJobsController@postEdit');
    Route::controller('jobs',        'AdminJobsController');

    #Applicants Management
    Route::get('applicants/skills/{id}/delete', 'AdminApplicantsController@delete');
    Route::controller('applicants', 'AdminApplicantsController');
    Route::get('applicants/createSkill/{id}', 'AdminApplicantsController@getCreate');
    Route::post('applicants/createSkill', 'AdminApplicantsController@postCreate');

    # Admin Dashboard
    Route::get('dashboard/contacts', 'AdminDashboardController@getContacts');

    # Search
    Route::get('search', 'AdminSearchController@getIndex');
    Route::controller('search', 'AdminSearchController');

    Route::controller('/', 'AdminDashboardController');
});


/** ------------------------------------------
 *  Frontend Routes
 *  ------------------------------------------
 */

// User reset routes
Route::get('user/reset/{token}', 'UserController@getReset');
// User password reset
Route::post('user/reset/{token}', 'UserController@postReset');
//:: User Account Routes ::
Route::post('user/{user}/edit', 'UserController@postEdit');

//:: User Account Routes ::
Route::post('user/login', 'UserController@postLogin');

# User RESTful Routes (Login, Logout, Register, etc)
Route::controller('user', 'UserController');

//:: Application Routes ::

# Filter for detect language
Route::when('contact-us','detectLang');

# Contact Us Static Page
Route::get('contact-us', function()
{
    // Return about us page
    return View::make('site/contact-us');
});

Route::resource('schools', 'SchoolsController');
Route::get('schools/{id}/delete', 'SchoolsController@delete');

Route::resource('addresses', 'AddressesController');

Route::resource('subjects', 'SubjectsController');

Route::resource('degrees', 'DegreesController');

Route::resource('certifications', 'CertificationsController');
Route::get('certification/{id}/delete', 'CertificationsController@delete');

Route::resource('skills', 'SkillsController');
Route::get('skills/{id}/delete', 'SkillsController@delete');

Route::resource('references', 'ReferencesController');
Route::get('references/{id}/delete', 'ReferencesController@delete');

Route::resource('memberships', 'MembershipsController');
Route::get('memberships/{id}/delete', 'MembershipsController@delete');

Route::resource('personalhistories', 'PersonalhistoriesController');

Route::resource('languages', 'LanguagesController');
Route::get('languages/{id}/delete', 'LanguagesController@delete');

Route::resource('workexperiences', 'WorkExperiencesController');
Route::get('workexperience/{id}/delete', 'WorkExperiencesController@delete');

Route::resource('transports', 'TransportsController');
Route::get('transport/{id}/delete', 'TransportsController@delete');

Route::resource('healths', 'HealthsController');
Route::get('healths/{id}/delete', 'HealthsController@delete');

Route::resource('conviction', 'ConvictionController');
Route::get('conviction/{id}/delete', 'ConvictionController@delete');

Route::get('applicants/{job}/create', array('before' => array('auth'), 'uses' => 'ApplicantsController@create'));

Route::resource('applicants', 'ApplicantsController');
Route::get('applicants/{id}/delete', 'ApplicantsController@delete');

Route::get('incidents/violation', 'IncidentsController@violation');
Route::resource('incidents', 'IncidentsController');

Route::get('profile/edit', array('before' => array('detectLang', 'auth'), 'uses' => 'DashboardController@getEdit'));
Route::post('profile/edit', array('before' => array('detectLang', 'auth'), 'uses' => 'DashboardController@postEdit'));

Route::get('profile/upload/{type}', array('before' => array('detectLang', 'auth'), 'uses' => 'DashboardController@getUpload'));
Route::post('profile/upload', array('before' => array('detectLang', 'auth'), 'uses' => 'DashboardController@postUpload'));

Route::get('profile/address', array('before' => array('detectLang', 'auth'), 'uses' => 'DashboardController@getEditAddress'));
Route::post('profile/address', array('before' => array('detectLang', 'auth'), 'uses' => 'DashboardController@postEditAddress'));

Route::get('company/edit', array('before' => array('auth'), 'uses' => 'CompanyController@getEdit'));
Route::get('company/edit_contact', array('before' => array('auth'), 'uses' => 'CompanyController@getEditContact'));
Route::post('company/edit', array('before' => array('auth'), 'uses' => 'CompanyController@postEdit'));

Route::get('jobs/{job}/review', 'JobsController@getReview');
Route::post('jobs/{job}/review', 'JobsController@postReview');
Route::get('jobs/{job}/status', 'JobsController@getStatusChange');
Route::post('jobs/{job}/status', 'JobsController@postStatusChange');
Route::get('jobs/{job}/show', 'JobsController@show');
Route::resource('jobs', 'JobsController');


Route::get('job_messages/{job}/create', 'JobMessageController@getCreate');
Route::post('job_messages', 'JobMessageController@postCreate');

//Not logged in, default.
Route::get('start', array('uses' => 'UserController@getCreate'));

$user = Auth::user();
//logged in defaults
if ($user == null || $user->user_type == 'applicant') {
    Route::get('/',
        array('before' => array('auth'),'uses' => 'DashboardController@getIndex')
    );
}

//company defaults
if ($user != null && $user->user_type == 'company') {
    Route::get('/',
        array('before' => array('auth'), 'uses' => 'CompanyController@getIndex')
    );
    Route::get('openings', 'CompanyController@getOpenings');
}
