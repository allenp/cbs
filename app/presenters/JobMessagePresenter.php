<?php

use Robbo\Presenter\Presenter;

class JobMessagePresenter extends Presenter
{

    public function author()
    {
        return $this->user->standardName();
    }

    public function when()
    {
        return $this->created_at->diffForHumans();
    }

    function get_label()
    {   
        if($this->status == null) return '';

        $status = strtolower($this->status->status);
        $labels = array(
            'draft' => 'default',
            'submitted' => 'warning',
            'approved' => 'success',
            'rejected' => 'danger',
            'recruiting' => 'info',
            'shortlist' => 'primary',
            'closed' => 'primary'
        );  
        return isset($labels[$status]) ? $labels[$status] : 'default';
    }
}
