<?php

use Robbo\Presenter\Presenter;

class SchoolPresenter extends Presenter
{
    public function period()
    {
        if (is_null($this->start_month)) {

            return $this->years_completed . ' years';

        } else {

            $start = date_create_from_format(
                '!j-m-Y',
                '1-'.$this->start_month.'-'.$this->start_year
            );

            $end = date_create_from_format(
                '!j-m-Y',
                '1-'.$this->end_month.'-'.$this->end_year
            );


            $interval = $start->diff($end);

            return $interval->format('%y years %m months');
        }
    }
}
