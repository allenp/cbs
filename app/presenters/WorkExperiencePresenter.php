<?php

use Robbo\Presenter\Presenter;

class WorkExperiencePresenter extends Presenter
{
    public function presentPeriod()
    {
        $start_mnth = date ("M", mktime(0,0,0,$this->start_month,1,0));
        $end_mnth = date ("M", mktime(0,0,0,$this->end_month,1,0));

        if ($this->start_year == $this->end_year){
            return $start_mnth.' - '. $end_mnth.' '. $this->start_year;
        } else {
            return $start_mnth . ' ' . $this->start_year.' - '. $end_mnth . ' ' . $this->end_year;
        }
    }
}
