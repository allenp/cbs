<?php

use Robbo\Presenter\Presenter;

class JobPresenter extends Presenter
{
    public function company_logo()
    {
        if ($this->recruitment == true) {
            return 'assets/img/default.png';
        } else {
            return $this->company_logo;
        }
    }

    public function company_name($ignore_confidential=false)
    {
        if (!$ignore_confidential && $this->recruitment == true || $this->company == null) {
            return 'Company Confidential';
        } else {
            return $this->company == null? 'Company Confidential' : $this->company->company;
        }
    }

    public function company_tagline()
    {
        if ($this->recruitment == true) {
            return '';
        } else {
            return $this->company != null ? $this->company->about: '';
        }
    }

    public function isRecent()
    {
        if ($this->start_dt != null) {
            return $this->start_dt
                ->diffInDays(Carbon::now('America/Jamaica')) < 1;
        } else {
            return $this->created_at
                ->diffInDays(Carbon::now('America/Jamaica')) < 2;
        }
    }

    public function location()
    {
        return $this->country !== null ?
            $this->location . ', ' . $this->country->name :
            $this->location;
    }

    public function preferred_job_type()
    {
        if ($this->full_time) {
            return 'Full Time';
        }

        if ($this->part_time) {
            return 'Part Time';
        }
    }

    public function job_type_classes()
    {
        $classes = array();
        if ($this->full_time) {
            $classes[] = 'full-time';
        }

        if ($this->part_time) {
            $classes[] = 'part-time';
        }

        if ($this->permanent) {
            $classes[] = 'permanent';
        }

        if ($this->temporary) {
            $classes[] = 'temporary';
        }

        return implode($classes);
    }

    function get_label()
    {   
        $status = strtolower($this->status);
        $labels = array(
            'draft' => 'default',
            'submitted' => 'warning',
            'approved' => 'success',
            'rejected' => 'danger',
            'recruiting' => 'info',
            'shortlist' => 'primary',
            'closed' => 'primary'
        );  
        return isset($labels[$status]) ? $labels[$status] : 'default';
    }
}
