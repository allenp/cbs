<?php

use Robbo\Presenter\Presenter;

class AttachmentPresenter extends Presenter
{

    public function human_filesize($decimals = 0)
    {   
        $sz = 'BKMGTP';
        $factor = floor((strlen($this->size) - 1) / 3); 
        return sprintf("%.{$decimals}f", $this->size / pow(1024, $factor)) . @$sz[$factor];
    }
}
