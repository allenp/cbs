<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>
        @yield('title')
    </title>
    @include('site.layouts.common-header')
</head>
<body class="company dashboard">
    <div class="container">
        <div class="page-header clearfix">
            <div class="row">
                <div class="col-md-8">
                    <h1 class="logo-title pull-left">Choice Business Solutions Limited</h1>
                    <div class="pull-right">
                        <div class="btn-group">
                            <a class="btn btn-default" href="{{{ action('UserController@getLogout') }}}">Log out</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
        @if(!Auth::user()->hasRole('admin'))
            <div class="row">
                <nav class="navbar navbar-default col-sm-12 col-lg-8" role="navigation">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#"></a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                            <ul class="nav navbar-nav">
                                <li class="{{ Request::is('/') ? 'active' : ''}}"><a href="{{ URL::to('/') }}">Profile</a></li>
                                <li class="{{ Request::is('openings*') ? 'active' : ''}}"><a href="{{ URL::to('openings') }}"><span class="glyphicon glyphicon-list-alt"></span> Jobs Listed</a></li>
                            </ul>
                            <div class="navbar-text pull-right">
                                <span class="glyphicon glyphicon-user"></span>
                                <span class="user-login"> {{ $user->email }}</span>
                            </div>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>
            </div>
            @endif
        </div>
        @yield('content')
        <div id="dialog" class="modal fade in">
            <div class="modal-dialog">
                <div class="modal-content">
                    <p style="text-align: center; margin-top: 2em;">
                        <img src="{{ asset('assets/img/loading.gif') }}" alt="Please wait. Loading..." />
                        <br />
                        Please wait a moment. Loading...
                    </p>
                </div>
            </div>
        </div>
        @include('site.layouts.common-footer')
        @yield('scripts')
    </div>
    <!-- /scripts -->
</body>
</html>
