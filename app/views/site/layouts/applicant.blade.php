<!DOCTYPE html>
<html>
<head>
    <title>
        {{{ $user->standardName() }}} - CBS Profile
    </title>
    @include('site.layouts.common-header')
    <link rel="stylesheet" href="{{ asset('assets/css/print.css') }}" media="print" />
    </head>
<body class="dashboard">
    <div class="container">
        <div class="page-header">
            <div class="pull-right hidden-print">
                <div class="btn-group">
                    <a class="btn btn-success" href="{{ action('JobsController@index') }}">List of applicants</a>
                    <!-- <a class="btn btn-success" href="">Account settings</a> -->
                    <a class="btn btn-success" href="{{{ action('UserController@getLogout') }}}">Log out</a>
                </div>
			</div>
            <h1 class="profile-title">{{{ $user->standardName() }}}<small class="hidden-xs hidden-print">applicant<!-- consultant --></small></h1>
        </div>
        <div class="view-only-content">
            <div class="row tab-pane active" id="tab1">
                <div class="col-sm-8 col-xs-12">
                    <div class="section clearfix">
                        <div class="title">Profile</div>
                        <div id="profile">
                            {{ $profile }}
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 hidden hidden-print">
                    <a id="dowload" href='{{ action("AdminApplicantsController@getDownload", $user->id) }}' class="btn btn-primary btn-lg" data-loading-text="Downloading PDF...">
                        Download profile as PDF 
                    </a>
                </div>
            </div>

            <div class="row tab-pane" id="tab2">
                <div class=" col-xs-12 col-sm-8">
                    <div class="section clearfix">
                        <div class="title">Education</div>
                        <div id="education">
                            {{ $education }}
                        </div>
                        <div id="certification">
                            {{ $certifications }}
                        </div>

                        <div id="skill">
                            {{ $skills }}
                        </div>
                        <div class="col-xs-12">
                            <ul class="nav navbar-nav pull-right">
                                <li>
<a data-toggle="modal" data-target='#dialog' href="{{ action('AdminApplicantsController@getCreate', ['id' => $user->id]) }}" class=""><span class="glyphicon glyphicon-pencil"></span> Add skill</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row tab-pane" id="tab3">
                <div class="col-xs-12 col-sm-8">
                    <div class="section clearfix">
                        <div class="title">Work Experience</div>
                        <div id="workexperience">
                        {{ $workexperiences }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="row tab-pane" id="tab4">
                <div class=" col-xs-12 col-sm-8">
                    <div class="section clearfix">
                        <div class="title">Employment Readiness</div>
                        <div id="health">
                            {{ $healths }}
                        </div>
                        <div class="form-group col-xs-12 dp-md">
                            <label>Criminal Conviction</label>
                        </div>
                        <div id="conviction">
                            {{ $convictions }}
                        </div>
                    </div>
                </div>
            </div>

            <! -- References -->
            <div class="row tab-pane" id="tab6">
                <div class="col-xs-12 col-sm-8">
                    <div class="section clearfix">
                        <div class="title">Reference</div>
                        <div id="reference">
                        {{ $references }}
                        </div>
                    </div>
                </div>
            </div>

            <! -- Transports -->
            <div class="row tab-pane" id="tab7">
                <div class="col-xs-12 col-sm-8">
                    <div class="section clearfix">
                        <div class="title">Transport</div>
                        <div id="transport">
                        {{ $transports }}
                        </div>
                        <div class="form-group dp-sm col-xs-12">
                            <label>Accidents</label>
                            <div>No accidents</div>
                        </div>
                        <div class="form-group dp-sm col-xs-12">
                            <label>Violations</label>
                            <div>No violations</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane row" id="tab5">
                <div class="col-xs-12 col-sm-8">
                    <div class="section clearfix">
                        <div class="title">Memberships</div>
                        <div id="membership">
                            {{ $memberships }}
                        </div>
                        <div class="form-group col-xs-12">
                            <label>Languages</label>
                            <div id="language">
                                {{ $languages }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container -->


<!-- modals -->

    <div id="dialog" class="modal fade in">
        <div class="modal-dialog">
            <div class="modal-content">
                <p style="text-align: center; margin-top: 2em;">
                    <img src="{{ asset('assets/img/loading.gif') }}" alt="Please wait. Loading..." />
                    <br />
                    Please wait a moment. Loading...
                </p>
            </div>
        </div>
    </div>
    @include('site.layouts.common-footer')
    <!-- /scripts -->
</body>
</html>
