@if(App::environment() == ENV_PRODUCTION)
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-3504355-19', 'choicebusinesssolutions.net');
      ga('send', 'pageview');
    </script>
@endif

@if(App::environment() == ENV_PRODUCTION)
    <script src="/assets/js/dashboard.min.js?v=0.6"></script>
@else

    <script src="/assets/js/wysihtml5/wysihtml5-0.3.0.js"></script>
    <script src="/assets/js/wysihtml5/wysihtml5ParserRules.js"></script>
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/jquery.popconfirm.js"></script>
    <script src="/assets/js/prettify.js"></script>
    <script src="/assets/js/wysihtml5/handlebars.runtime.min.js"></script>
    <script src="/assets/js/wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script src="/assets/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/datatables-bootstrap.js"></script>
    <script src="/assets/js/bootstrap-pagination.js"></script>
    <script src="/assets/js/datatables.fnReloadAjax.js"></script>
    <script src="/assets/js/jquery.colorbox.js"></script>
    <script src="/assets/js/jquery.form.js"></script>
    <script src="/assets/js/jQueryFileUpload956/js/vendor/jquery.ui.widget.js"></script>
    <script src="/assets/js/jQueryFileUpload956/js/jquery.iframe-transport.js"></script>
    <script src="/assets/js/jQueryFileUpload956/js/jquery.fileupload.js"></script>
    <script src="/assets/js/editor.js?ver=1.1"></script>
    <script src="/assets/js/render.js?ver=1.2"></script>

@endif
    <script type="text/javascript">
        $(function() {
            $(document).on('hidden.bs.modal', '#dialog', function (event) {
                $(this).removeData('bs.modal');
                $(this).find('.modal-content').html('<p style="text-align: center; margin-top: 2em;">' +
                    '<img src="{{ asset('assets/img/loading.gif') }}" alt="Please wait. Loading..." />' +
                    '<br /        >Please wait a moment. Loading...</p>');
            });
        });
    </script>
