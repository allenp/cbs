<!DOCTYPE html>
<html>
    <head>
        <title>
            {{{ $user->standardName() }}} - CBS Profile
        </title>
        @include('site.layouts.common-header')
        <link rel="stylesheet" href="{{ asset('assets/css/datatables-bootstrap.css') }}">
    </head>
    <body class="dashboard">
        <div class="container">
            <div class="page-header">
                <div class="pull-right">
                    <div class="btn-group">
                        <a class="btn btn-success" href="{{{ action('UserController@getLogout') }}}">Log out</a>
                    </div>
                </div>
                <h1 class="profile-title">List of applicants</h1>
            </div>
            <table id="applicants" class="table table-bordered">
            <thead>
                <tr><th>Id</th><th>First Name</th><th>Last Name</th><th>Email</th><th>Phone #</th><th>Resume</th><th>Job Applied for</th><th>Actions</th></tr>
            </thead>
            <tbody>
            </tbody>
            </table>
        </div>
        @include('site.layouts.common-footer')
        <!-- /scripts -->
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
        <script src="/assets/js/datatables-bootstrap.js"></script>
        <script src="/assets/js/datatables.fnReloadAjax.js"></script>
        <script type="text/javascript">
            var oTable;
            $(document).ready(function() {
                oTable = $('#applicants').dataTable({
                    "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
                    "sPaginationType": "bootstrap",
                    "oLanguage": {
                        "sLengthMenu": "_MENU_ records per page"
                    },
                    "bProcessing": true,
                    "bServerSide": true,
                    "sAjaxSource": "{{ URL::to('dashboard/data') }}"
                });
            });

        </script>
    </body>
</html>
