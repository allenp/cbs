<!doctype html>
<html>
    <head>
        <title>
            @section('title')
            CBS Profile - Login here
            @show
        </title>
        @include('site.layouts.common-header') 
    </head>
    <body id="body-login">
        <!-- Content -->
        @yield('content')
        <!-- ./ Content -->
        <!-- scripts -->
        @include('site.layouts.common-footer')
        <!-- /scripts -->
    </body>
</html>
