<!DOCTYPE html>
<html>
    <head>
        <title>Register for your CBS Profile</title>
        @include('site.layouts.common-header') 
    </head>
    {{ ''; $applicant_active = Input::old('user_type') === 'applicant' || Input::old('user_type') !== 'company' ? 'active' : '' }}
    {{ ''; $company_active = Input::old('user_type') === 'company' ? 'active' : '' }}
    <body class="account">
        <div class="container">
            <div class="signup-section">
                <div class="col-xs-12 col-sm-5 col-md-4 col-md-offset-1 intro">	
                    <h1 class="logo-title">Choice Business Solutions</h1>
                    <h3 class="dp-lg">Looking for a job?</h3>
                    <p>We will help you start a great career with the right company.</p>
                    <hr />
                    <h3 class="dp-lg">Are you an employer?</h3>
                    <p>Find the right people to join your team today! Give job seekers an easy and quick way to get an overview of your company.</p>
                    <hr />
                    <h3>Already a member?</h3>
                    <a class="btn btn-lg btn-primary dp-sm" href="{{ action('UserController@getLogin') }}">Log in to your CBS profile</a>
                </div>

                <div class="col-xs-12 col-sm-7 col-md-6 register">
                    <ul class="nav nav-tabs">
                        <li class="{{ $applicant_active }}"><a href="#applicantRegistration" data-toggle="tab"><strong>Sign up for job offers</strong></a></li>
                        <li class="{{ $company_active }}"><a href="#companyRegistration" data-toggle="tab"><strong>Sign up as a company</strong></a></li>
                    </ul>
                    <div class="tab-content">
                    {{ Form::open(array('url' => action('UserController@postIndex'), 'id' => 'applicantRegistration', 'class' => 'tab-pane fade in ' . $applicant_active .'  signup-form form-vertical clearfix')) }}
                        @if($applicant_active == 'active')
                            @if($errors->has())
                            <div class="alert alert-danger alert-block">
                                <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                                </ul>
                            </div>
                            @endif

                            @if ( Session::get('error') )
                                <div class="alert alert-danger">{{ HTML::ul(Session::get('error')) }}</div>
                            @endif

                            @if ( Session::get('notice') )
                                <div class="alert alert-info">{{ HTML::ul(Session::get('notice')) }}</div>
                            @endif
                        @endif

                        <div class="form-group">
                            <h4 class="form-text">Join us and find your new job</h4>
                            <hr />
                        </div>
                        <div class="form-group">
                            <label>Name</label>
                            <div class="row">
                                <div class="form-group col-xs-12 col-sm-6">
                                    {{ Form::text('firstname', Input::old('firstname'), array('class' => 'form-control', 'placeholder' => 'First name')) }}
                                </div>
                                <div class="form-group col-xs-12 col-sm-6">
                                    {{ Form::text('lastname', Input::old('lastname'), array('class' => 'form-control', 'placeholder' => 'Last name')) }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            {{ Form::email('email', Input::old('email'), array('class' => 'form-control', 'placeholder' => 'e.g. johndoe@email.com')) }}
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
                        </div>
                        <div class="form-group">
                            <label>Confirm Password</label>
                            {{ Form::password('password_confirmation', array('class' => 'form-control', 'placeholder' => 'Password')) }}
                        </div>
                            {{ Form::hidden('user_type', 'applicant') }}
                        <div class="form-group">
                            <hr />
                            {{ Form::submit('Register and setup my profile', array('class' => 'btn btn-lg btn-primary')) }}
                        </div>
                        
                    {{ Form::close() }}

                    {{ Form::open(array('url' => action('UserController@postIndex'), 'id' => 'companyRegistration', 'class' => 'tab-pane fade in ' . $company_active . ' signup-form form-vertical clearfix')) }}
                        @if($company_active == 'active')
                            @if($errors->has())
                            <div class="alert alert-danger alert-block">
                                <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                                </ul>
                            </div>
                            @endif

                            @if ( Session::get('error') )
                            <div class="alert alert-danger">{{ HTML::ul(Session::get('error')) }}</div>
                            @endif

                            @if ( Session::get('notice') )
                            <div class="alert alert-info">{{ HTML::ul(Session::get('notice')) }}</div>
                            @endif
                        @endif

                        <div class="form-group">
                            <h4 class="form-text">Let us help you build your team</h4>
                            <hr />
                        </div>
                        <div class="form-group">
                            <label>Company name</label>
                            {{ Form::text('company', Input::old('company'), array('class' => 'form-control', 'placeholder' => 'Acme Company Ltd.')) }}
                        </div>
                        <div class="form-group">
                            <label>Username</label>
                            {{ Form::text('username', Input::old('username'), array('class' => 'form-control', 'placeholder' => 'Username')) }}
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            {{ Form::email('email', Input::old('email'), array('class' => 'form-control', 'placeholder' => 'e.g. johndoe@email.com')) }}
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
                        </div>
                        <div class="form-group">
                            <label>Confirm Password</label>
                            {{ Form::password('password_confirmation', array('class' => 'form-control', 'placeholder' => 'Password')) }}
                        </div>
                            {{ Form::hidden('user_type', 'company') }}
                        <div class="form-group">
                            <hr />
                            {{ Form::submit('Register and setup my company profile', array('class' => 'btn btn-lg btn-primary')) }}
                        </div>
                        
                    {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
        @include('site.layouts.common-footer') 
        <script type="text/javascript">
            jQuery(function($) {
                $('.nav-tabs li a').click(function(e) {
                    e.preventDefault();
                    $(this).tab('show');
                });
            });
        </script>
        <!-- /scripts -->
    </body>
</html>
