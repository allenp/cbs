<meta name="environment" content="{{ App::environment() }}" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
@if(App::environment() == ENV_PRODUCTION)
<link rel="stylesheet" href="{{ asset('assets/css/dashboard.min.css?v=1.2') }}">
@else
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap-theme.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/wysihtml5/bootstrap3-wysihtml5.min.css') }}"> 
<link rel="stylesheet" href="{{ asset('assets/css/jquery.fileupload-ui.css') }}"> 
<link rel="stylesheet" href="{{ asset('assets/css/jquery.fileupload.css') }}"> 
<link rel="stylesheet" href="{{{ asset('assets/css/cbs.css') }}}" media="screen">
@endif
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{{ asset('assets/ico/flame144x144.png?v=0.1') }}}">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{{ asset('assets/ico/flame114x114.png?v=0.1') }}}">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{{ asset('assets/ico/flame72x72.png?v=0.1') }}}">
<link rel="apple-touch-icon-precomposed" href="{{{ asset('assets/ico/flame52x52.png?v=0.1') }}}">
<link rel="shortcut icon" href="{{{ asset('assets/ico/favicon.png?v=0.1') }}}">
