<!DOCTYPE html>
<html>
<head>
    <title>
        {{{ $user->standardName() }}} - CBS Profile
    </title>
    @include('site.layouts.common-header')
</head>
<body class="dashboard {{ Auth::user()->hasRole('admin') ? 'admin' : 'applicant' }} {{ App::environment() }}">
    @include('site.layouts.admin-nav')
    <div class="container">
        <nav class="navbar navbar-default hidden-sm hidden-md hidden-lg" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#tab1" data-toggle="tab">Profile</a></li>
                        <li><a href="#tab2" data-toggle="tab">Education</a></li>
                        <li><a href="#tab3" data-toggle="tab">Work Experience</a></li>
                        <li><a href="#tab4" data-toggle="tab">Health &amp; Fitness</a></li>
                        <li><a href="#tab5" data-toggle="tab">Memberships</a></li>
                        <li><a href="#tab6" data-toggle="tab">References</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <div class="page-header clearfix">
            <div class="row">
                <div class="col-md-8">
                    <div class="pull-left hidden-print">
                        <h1 class="logo-title">Choice Business Solutions Limited</h1>
                    </div>
                    @if(!Auth::user()->hasRole('admin'))
                    <div class="pull-right hidden-print">
                        <div class="btn-group">
                            <a href="{{{ action('JobsController@index') }}}" class="btn btn-primary"><span class="glyphicon"></span> See jobs available</a>
                            <a class="btn btn-default" href="{{{ action('UserController@getLogout') }}}">Log out</a>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-xs-12">
        <div class="row">
        <nav class="navbar navbar-default hidden-xs col-sm-12 col-lg-8" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#tab1" data-toggle="tab">Profile</a></li>
                        <li><a href="#tab2" data-toggle="tab">Education</a></li>
                        <li><a href="#tab3" data-toggle="tab">Work Experience</a></li>
                        <li><a href="#tab4" data-toggle="tab">Health &amp; Fitness</a></li>
                        <li><a href="#tab5" data-toggle="tab">Memberships</a></li>
                        <li><a href="#tab6" data-toggle="tab">References</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        </div>
        </div>

        <h1 class="profile-title">{{{ $user->standardName() }}}<!--<small class="hidden-xs">applicant</small>--></h1>
        <div class="tab-content">
            <div class="row tab-pane active" id="tab1">
                <div class="col-md-8 col-xs-12">
                    <div class="section clearfix">
                        <div class="title">Profile</div>

                        <div class="col-xs-12">
                            <ul class="nav navbar-nav pull-right">
                                <li><a data-toggle="modal" href="{{{ action('DashboardController@getEdit') }}}" data-target="#dialog"><span class="glyphicon glyphicon-pencil"></span> Edit profile</a></li>
                                <li><a data-toggle="modal" data-target="#dialog" href="{{ action('DashboardController@getEditAddress') }}" class=""><span class="glyphicon glyphicon-pencil"></span> Edit address</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Upload <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a data-toggle="modal" href="{{ action('DashboardController@getUpload', 'resume') }}" data-target="#dialog" class="">Résumé</a></li>
                                    <li><a data-toggle="modal" href="{{ action('DashboardController@getUpload', 'culture') }}" data-target="#dialog" class="">Culture test results</a></li>
                                    <li><a data-toggle="modal" href="{{ action('DashboardController@getUpload', 'disc') }}" data-target="#dialog" class="">DISC Index</a></li>
                                    <li><a data-toggle="modal" href="{{ action('DashboardController@getUpload', 'values') }}" data-target="#dialog" class="">VALUES Index</a></li>
                                </ul>
                                </li>
                            </ul>
                        </div>

                        <div id="profile" class="clearfix">
                            {{ $profile }}
                        </div>
                        <div id="applicant">
                            {{ $applicants }} 
                        </div>
                    </div>
                </div>
            </div>

            <div class="row tab-pane" id="tab2">
                <div class="col-xs-12 col-sm-4 pull-right">
                </div>

                <div class="col-xs-12 col-md-8">
                    <div class="section clearfix">
                        <div class="title">Education</div>

                        <div class="col-xs-12">
                            <ul class="nav navbar-nav pull-right">
                                <li>
<a data-toggle="modal" data-target="#dialog" href="{{ action('SchoolsController@create') }}" class=""><span class="glyphicon glyphicon-pencil"></span> Add school</a></li>
                                <li>
<a data-toggle="modal" data-target="#dialog" href="{{ action('CertificationsController@create') }}" class=""><span class="glyphicon glyphicon-pencil"></span> Add certification</a>
                                </li>
                                <li>
<a data-toggle="modal" data-target='#dialog' href="{{ action('SkillsController@create') }}" class=""><span class="glyphicon glyphicon-pencil"></span> Add skill</a>
                                </li>
                            </ul>
                        </div>

                        <div id="education">
                            {{ $education }}
                        </div>
                        <hr />
                        <div id="certification" class="">
                            {{ $certifications }}
                        </div>
                        <hr />
                        <div class="">
                            <div id="skill">
                            {{ $skills }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row tab-pane" id="tab3">
                <div class="col-xs-12 col-sm-4 pull-right">
                </div>
                <div class="col-xs-12 col-md-8">
                    <div class="section clearfix">
                        <div class="title">Work Experience</div>

                        <div class="col-xs-12">
                            <ul class="nav navbar-nav pull-right">
                                <li>
<a data-toggle="modal" data-target='#dialog' href="{{ action('WorkExperiencesController@create') }}" class=""><span class="glyphicon glyphicon-pencil"></span> Add work experience</a></li>
                            </ul>
                        </div>
                        <div id="workexperience">
                        {{ $workexperiences }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="row tab-pane" id="tab4">
                <div class="col-xs-12 col-sm-4 pull-right">
                </div>
                <div class=" col-xs-12 col-md-8">
                    <div class="section clearfix">
                        <div class="title">Employment Readiness</div>
                        <div class="col-xs-12">
                            <ul class="nav navbar-nav pull-right">
                                <li>
                        <a data-toggle="modal" data-target="#dialog" href="{{ action('HealthsController@create') }}" class=""><span class="glyphicon glyphicon-pencil"></span> Edit health information</a></li>

                        <li>
                            <a data-toggle="modal" data-target="#dialog" href="{{ action('ConvictionController@create') }}" class=""><span class="glyphicon glyphicon-pencil"></span> Add criminal conviction</a></li>

                            </ul>
                        </div>
                        <div id="health">
                            {{ $healths }}
                        </div>
                        <div class="form-group col-xs-12">
                            <hr />
                            <label>Criminal Conviction</label>
                        </div>
                        <div id="conviction">
                            {{ $convictions }}
                        </div>
                    </div>
                </div>
            </div>

            <!-- References -->
            <div class="row tab-pane" id="tab6">
                <div class="col-xs-12 col-sm-4 pull-right">
                    
                </div>
                <div class="col-xs-12 col-md-8">
                    <div class="section clearfix">
                        <div class="title">References</div>

                        <div class="col-xs-12">
                            <ul class="nav navbar-nav pull-right">
                                <li>
<a data-toggle="modal" data-target="#dialog" href="{{ action('ReferencesController@create') }}" class=""><span class="glyphicon glyphicon-pencil"></span> Add reference</a></li>
                            </ul>
                        </div>

                        <div id="reference">
                        {{ $references }}
                        </div>
                    </div>
                </div>
            </div>

            <!-- Transports 
            <div class="row tab-pane" id="tab7">
                <div class="col-xs-12 col-sm-4 pull-right">
                    <div class="btn-group-vertical">
                        <a data-toggle="modal" data-target="#dialog" href="{{ action('TransportsController@create') }}" class="btn-add btn btn-default"><span class="glyphicon glyphicon-pencil"></span> Edit Transport</a>
                        <a data-toggle="modal" data-target="#dialog" href="{{ action('IncidentsController@create') }}" class="btn btn-add btn-default"><span class="glyphicon glyphicon-pencil"></span> Add Accident</a>
                        <a data-toggle="modal" data-target="#dialog" href="{{ action('IncidentsController@violation') }}" class="btn btn-add btn-default"><span class="glyphicon glyphicon-pencil"></span> Add Violation</a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-8">
                    <div class="section clearfix">
                        <div class="title">Transport</div>
                        <div id="transport">
                        {{ $transports }}
                        </div>
                        <div class="form-group dp-sm col-xs-12">
                            <label>Accidents</label>
                            <div>No accidents</div>
                        </div>
                        <div class="form-group dp-sm col-xs-12">
                            <label>Violations</label>
                            <div>No violations</div>
                        </div>
                    </div>
                </div>
            </div>
-->
            <div class="tab-pane row" id="tab5">
                <div class="col-xs-12 col-sm-4 pull-right">
                </div>
                <div class="col-xs-12 col-md-8">
                    <div class="section clearfix">
                        <div class="title">Memberships</div>
                            <div class="col-xs-12">
                            <ul class="nav navbar-nav pull-right">
                                <li>
                                <a data-toggle="modal" data-target="#dialog" href="{{ action('MembershipsController@create') }}" class=""><span class="glyphicon glyphicon-pencil"></span> Add membership</a></li>
                                <li>
                                <a data-toggle="modal" data-target="#dialog" href="{{ action('LanguagesController@create') }}" class=""><span class="glyphicon glyphicon-pencil"></span> Add language</a></li>
                            </ul>
                        </div>
                        <div id="membership">
                            {{ $memberships }}
                        </div>
                        <div class="form-group col-xs-12">
                            <hr />
                            <h4>Languages</h4>
                            <div id="language">
                                {{ $languages }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container -->
    <footer>
    </footer>


    <!-- modal -->
    <div id="dialog" class="modal fade in">
        <div class="modal-dialog">
            <div class="modal-content">
                <p style="text-align: center; margin-top: 2em;">
                    <img src="{{ asset('assets/img/loading.gif') }}" alt="Please wait. Loading..." />
                    <br />
                    Please wait a moment. Loading...
                </p>
            </div>
        </div>
    </div>
    <div id="dialog_large" class="modal fade in">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <p style="text-align: center; margin-top: 2em;">
                    <img src="{{ asset('assets/img/loading.gif') }}" alt="Please wait. Loading..." />
                    <br />
                    Please wait a moment. Loading...
                </p>
            </div>
        </div>
    </div>
    <!-- //modal -->
    @include('site.layouts.common-footer')
    <!-- /scripts -->
</body>
</html>
