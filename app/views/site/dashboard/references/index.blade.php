<div class="col-xs-12">
<hr />
@if(count($references) > 0)
  @foreach($references as $reference)
<div class="row">
      <h3 class="col-xs-12">
          {{{ $reference->first_name . ' ' . $reference->last_name }}}
            @if (!$viewonly)
                  <a class="item-edit" data-toggle="modal" data-target="#dialog" href="{{ action('ReferencesController@edit', $reference->id) }}"><span class="glyphicon glyphicon-pencil"></span></a>
                  <a class="item-edit confirm-delete" href="#" data-delete-url="{{ action('ReferencesController@delete', $reference->id) }}"><span class="glyphicon glyphicon-trash"></span></a>
            @endif
       </h3>
      <div class="col-xs-12">
          {{{ $reference->position }}}
      </div>
      <div class="col-xs-12">
          {{{ $reference->company }}}<br />
          {{{ $reference->address }}}<br />
          {{{ $reference->email }}}<br />
          {{{ $reference->phone }}}
      </div>
</div>
  @endforeach
@else
    @if ($viewonly)
        <p>Applicant has not added any references yet.</p>
    @else
        <p>Please list at least two references other than relatives or previous employers.</p>
    @endif
@endif
</div>
