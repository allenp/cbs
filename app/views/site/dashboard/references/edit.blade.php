@if($reference->id > 0)
{{ Form::model($reference, array('method' => 'PUT', 'url' => action('ReferencesController@update', array('id' => $reference->id)), 'data-async' => 'true', 'data-target' => '#dialog')) }}
@else
    {{ Form::model($reference, array('url' => action('ReferencesController@store'), 'data-async' => 'true', 'data-target' => '#dialog')) }}
@endif
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            @if($reference->id > 0)
            <h4 class="modal-title">Edit reference</h4>
            @else
            <h4 class="modal-title">Add a new reference</h4>
            @endif
        </div>
        <div class="modal-body clearfix">
            @if($errors->count())
            <div class="alert alert-warning">
            {{ HTML::ul($errors->all()) }}
            </div>
            @endif
            <div class="form-text col-xs-12">
                <p>Your reference must not be a relative or previous employer.</p>
            </div>
            <div class="form-group col-xs-6">
                {{ Form::text('first_name', Input::old('first_name'), array('class' => 'form-control', 'placeholder' => 'First name')) }}
            </div>
            <div class="form-group col-xs-6">
                {{ Form::text('last_name', Input::old('last_name'), array('class' => 'form-control', 'placeholder' => 'Last name')) }}
            </div>
            <div class="form-group col-xs-6">
                {{ Form::email('email', Input::old('email'), array('class' => 'form-control', 'placeholder' => 'Email address')) }}
            </div>
            <div class="form-group col-xs-6">
                {{ Form::text('position', Input::old('position'), array('class' => 'form-control', 'placeholder' => 'Position or title')) }}
            </div>
            <div class="form-group col-xs-12">
                {{ Form::text('company', Input::old('company'), array('class' => 'form-control', 'placeholder' => 'Company')) }}
            </div>
            <div class="form-group col-xs-12">
                {{ Form::text('address', Input::old('address'), array('class' => 'form-control', 'placeholder' => 'Address')) }}
            </div>
            <div class="form-group col-xs-12">
                {{ Form::text('phone', Input::old('phone'), array('class' => 'form-control', 'placeholder' => 'Telephone')) }}
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {{ Form::submit('Add reference', array('class' => 'btn btn-primary')) }}
        </div>
    {{ Form::close() }}
