    {{ Form::model($incident, array('url' => action('IncidentsController@store'), 'data-async' => 'true', 'data-target' => '#dialog')) }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            @if($is_accident)
            <h4 class="modal-title">Add an accident</h4>
            @else
            <h4 class="modal-title">Add violation</h4>
            @endif
        </div>
        <div class="modal-body clearfix">
            @if($errors->count())
            <div class="alert alert-warning">
            {{ HTML::ul($errors->all()) }}
            </div>
            @endif
            <div class="form-text col-xs-12">
                <p>Add an accident that has occured within the last 3 years</p>
            </div>
            <div class="form-group col-xs-6">
                {{ Form::select('month_occured', $months, Input::old('month_occured'), array('class' => 'form-control')) }}
            </div>
            <div class="form-group col-xs-6">
                {{ Form::select('year_occured', $years, Input::old('month_occured'), array('class' => 'form-control')) }}
            </div>
            <div class="form-group col-xs-12">
                {{ Form::textarea('details', Input::old('details'), array('class' => 'form-control', 'placeholder' => 'Explain the details of the incident here.')) }}
            </div>
                {{ Form::hidden('is_accident', $is_accident) }}
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            @if($is_accident)
            {{ Form::submit('Save accident', array('class' => 'btn btn-primary')) }}
            @else
            {{ Form::submit('Save violation', array('class' => 'btn btn-primary')) }}
            @endif
        </div>
    {{ Form::close() }}
