<h3 class="col-xs-12">Accidents</h3>
@if(count($accidents) > 0)
  @foreach($accidents as $incident)
      {{ date('j', date("F", mktime(0, 0, 0, $incident->month_occured + 1, 0, 0))) }} {{ $incident->year_occured }}<a class="item-edit" data-toggle="modal" data-target="#dialog" href="{{ action('IncidentsController@edit', $incident->id) }}"><span class="glyphicon glyphicon-pencil"></span></a>
 <a class="item-edit" data-toggle="modal" data-target="#dialog" href="{{ action('IncidentsController@edit', $incident->id) }}"><span class="glyphicon glyphicon-pencil"></span></a>
      <a class="item-edit confirm-delete" href="#" data-delete-url="{{ action('IncidentsController@delete', $incident->id) }}"><span class="glyphicon glyphicon-trash"></span></a>                           </h3>
  <div class="col-xs-12">
      {{{ $incident->details }}}
  </div>
  @endforeach
@else
    <p class="text-center">Add a record for all accidents you have had within the last 3 years.</p>
@endif

<h3 class="col-xs-12">Violations</h3>
@if(count($violations) > 0)
  @foreach($violations as $incident)
      {{ date('j', date("F", mktime(0, 0, 0, $incident->month_occured + 1, 0, 0)) }}- {{ $incident->year_occured }}<a class="item-edit" data-toggle="modal" data-target="#dialog" href="{{ action('IncidentsController@edit', $incident->id) }}"><span class="glyphicon glyphicon-pencil"></span></a>
 <a class="item-edit" data-toggle="modal" data-target="#dialog" href="{{ action('IncidentsController@edit', $incident->id) }}"><span class="glyphicon glyphicon-pencil"></span></a>
      <a class="item-edit confirm-delete" href="#" data-delete-url="{{ action('IncidentsController@delete', $incident->id) }}"><span class="glyphicon glyphicon-trash"></span></a>                           </h3>
  <div class="col-xs-12">
      {{{ $incident->details }}}
  </div>
  @endforeach
@else
    <p class="text-center">Add a record for all accidents you have had within the last 3 years.</p>
@endif
