    @if($health->id > 0)
    {{ Form::model($health, array('method' => 'PUT', 'url' => action('HealthsController@update', array('id' => $health->id)), 'data-async' => 'true', 'data-target' => '#dialog')) }}
    @else
    {{ Form::model($health, array('url' => action('HealthsController@store'), 'data-async' => 'true', 'data-target' => '#dialog')) }}
    @endif
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Health Information</h4>
        </div>
        <div class="modal-body clearfix">
            @if($errors->count())
            <div class="alert alert-warning">
            {{ HTML::ul($errors->all()) }}
            </div>
            @endif
                <div class="form-group col-xs-12 ">
                    <label>Do you smoke?</label>
                        <label>{{ Form::radio('smoke', false, (Input::old('smoke') == false) , array('id'=>'no')) }}No </label>
                        <label>{{ Form::radio('smoke', true, (Input::old('smoke') == true) , array('id'=>'yes')) }}Yes </label>
                </div>
                <div class="form-group col-xs-12 ">
                    <label>Do you drink alcohol?</label>
                        <label>{{ Form::radio('drink', false, (Input::old('drink') == false), array('id'=>'no')) }}No </label>
                        <label>{{ Form::radio('drink', true, (Input::old('drink') == true), array('id'=>'yes')) }}Yes </label>
                </div>
                <div class="form-group col-xs-12 ">
                    <label>Do you have allergies?</label>
                        <label>{{ Form::radio('allergies', false, (Input::old('allergies') == false), array('id'=>'no')) }}No </label>
                        <label>{{ Form::radio('allergies', true, (Input::old('allergies') == true), array('id'=>'yes')) }}Yes </label>
                </div>
                <div class="form-group col-xs-12 ">
                    <label>Do you have any physical limitations?</label>
                        <label>{{ Form::radio('physical_limitations', false, (Input::old('physical_limitations') == false), array('id'=>'no')) }}No </label>
                        <label>{{ Form::radio('physical_limitations', true, (Input::old('physical_limitations') == true), array('id'=>'yes')) }}Yes </label>
                </div>
                <div class="form-group col-xs-12">
                    <label>Limitation</label>
                    {{ Form::text('limitations', Input::old('limitation'), array('class' => 'form-control', 'placeholder' => 'Limitations if any')) }}
                </div>
                <div class="form-group col-xs-12">
                <label>
                        Are you currently being treated for any problems that could affect your ability to perform the job 
                        description?
                </label>
                        <label>{{ Form::radio('treated', false, (Input::old('treated') == false), array('id'=>'no')) }}No </label>
                        <label>{{ Form::radio('treated', true, (Input::old('treated') == true), array('id'=>'yes')) }}Yes </label>
                </div>
                <div class="form-group col-xs-12">
                    <label>Treatment</label>
                    {{ Form::text('current_treatment', Input::old('current_treatment'), array('class' => 'form-control', 'placeholder' => 'State all treatment')) }}
                </div>
            </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {{ Form::submit('Save health record', array('class' => 'btn btn-primary')) }}
        </div>
    {{ Form::close() }}
