<div class="col-xs-12">
<hr />
@if(count($healths) > 0)
  @foreach($healths as $health)
                <div class="form-group col-xs-12 ">
                    <label>Do you smoke?</label>
                        <label class="{{ $health->smoke == false ? 'on' : 'off' }}">{{ Form::radio('smoke', null, $health->smoke == false, array('disabled' => 'disabled')) }}No </label>
                        <label class="{{ $health->smoke == true ? 'on' : 'off' }}">{{ Form::radio('smoke', null, $health->smoke, array('disabled' => 'disabled')) }}Yes </label>
                </div>
                <div class="form-group col-xs-12 ">
                    <label>Do you drink alcohol?</label>
                        <label class="{{ $health->drink == false ? 'on' : 'off' }}">{{ Form::radio('drink', null, $health->drink == false, array('disabled' => 'disabled')) }}No </label>
                        <label class="{{ $health->drink == true ? 'on' : 'off' }}">{{ Form::radio('drink', null, $health->drink, array('disabled' => 'disabled')) }}Yes </label>
                </div>
                <div class="form-group col-xs-12 ">
                    <label>Do you have allergies?</label>
                        <label class="{{ $health->allergies == false ? 'on' : 'off' }}">{{ Form::radio('allergies', null, $health->allergies == false, array('disabled' => 'disabled')) }}No </label>
                        <label class="{{ $health->allergies == true ? 'on' : 'off' }}">{{ Form::radio('allergies', null, $health->allergies, array('disabled' =>'disabled')) }}Yes </label>
                </div>
                <div class="form-group col-xs-12 ">
                    <label>Have you been any physical limitations?</label>
                        <label class="{{ $health->physical_limitations == false ? 'on' : 'off' }}">{{ Form::radio('physical_limitations', null, $health->physical_limitations == false, array('disabled' => 'disabled')) }}No </label>
                        <label class="{{ $health->physical_limitations == true ? 'on' : 'off' }}">{{ Form::radio('physical_limitations', null, $health->physical_limitations, array('disabled' => 'disabled')) }}Yes </label>
                </div>
                <div class="form-group col-xs-12">
                    <label>Limitation</label>
                    <div>{{ $health->limitations }}</div>
                </div>
                <div class="form-group col-xs-12">
                <label>
                        Are you currently being treated for any problems that could affect your ability to perform the job 
                        description?
                </label>
                        <label class="{{ $health->treated == false ? 'on' : 'off' }}">{{ Form::radio('treated', null, $health->treated == false, array('disabled' => 'disabled')) }}No </label>
                        <label class="{{ $health->treated == true ? 'on' : 'off' }}">{{ Form::radio('treated', null, $health->treated, array('disabled' => 'disabled')) }}Yes </label>
                </div>
                <div class="form-group col-xs-12">
                    <label>Treatment</label>
                    <div>{{ $health->current_treatment }}</div>
                </div>
  @endforeach
@else
    @if ($viewonly)
        <p class="">Applicant has not added any health information yet.</p>
    @else
        <p class="">You have not added any health information yet.</p>
    @endif
@endif
</div>
