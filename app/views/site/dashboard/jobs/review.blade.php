<!DOCTYPE html>
<html>
<head>
    <title>
       {{{ $job->title }}} - {{{ $job->company_name() }}}
    </title>
    @include('site.layouts.common-header')
</head>
<body class="dashboard company">
    <div class="container">
        <div class="page-header clearfix">
            <div class="row">
            <div class="col-xs-12">
                <h1 class="logo-title pull-left">Choice Business Solutions Limited</h1>
                <div class="pull-right">
                    <div class="btn-group">
                        @if(Auth::user()->hasRole('admin'))
                            <a class="btn btn-default" href="{{ action('AdminJobsController@getIndex') }}">Back to jobs</a>
                        @else
                            <a class="btn btn-default" href="{{ action('CompanyController@getOpenings') }}">Back to listings</a>
                        @endif
                        <a class="btn btn-default" href="{{{ action('UserController@getLogout') }}}">Log out</a>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <div class="tab-content">
            <div class="row tab-pane active">
                <div class="col-xs-12">
                    <div class="company">
                        <a class="avatar" href="#"></a>
                        <div class="title">
                            <span>
                            <hgroup>
                                <h2>{{ $job->company_name() }}</h2>
                                <h3 class="tagline">{{ $job->company_tagline() }}</h3>
                            </hgroup>
                            </span>
                        </div>
                    </div>
                </div>
                @if(Auth::user()->hasRole('admin') || Auth::user()->id == $job->company_id)
                <div class="col-sm-5 pull-right section">
                 
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#activity" role="tab" data-toggle="tab">Activity</a></li>
                        <li><a href="#details" role="tab" data-toggle="tab">Details</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="activity">
                            <div class="clearfix">
                                <ul class="nav navbar-nav pull-right">
                                    <li><a href="{{{ action('JobsController@edit', $job->id) }}}"><span class="glyphicon glyphicon-pencil"></span> Edit listing</a></li>
                                    @if(Auth::user()->hasRole('admin'))
                                        @if($job->status == 'DRAFT')
                                        <li><a href="{{ action('JobsController@getStatusChange', ['id' =>$job->id, 'status' => 'APPROVED']) }}" data-toggle="modal" data-target="#dialog"><span class="glyphicon glyphicon-ok"></span> Approve</a></li>
                                        @endif
                                        @if($job->status == 'APPROVED')
                                        <li><a href="{{ action('JobsController@getStatusChange', ['id' => $job->id, 'status' => 'DRAFT']) }}" data-toggle="modal" data-target="#dialog"><span class="glyphicon glyphicon-ok"></span> Draft</a></li>
                                        <li><a href="{{ action('JobsController@getStatusChange', ['id' => $job->id, 'status' => 'RECRUITING']) }}" data-toggle="modal" data-target="#dialog"><span class="glyphicon glyphicon-ok"></span> Recruiting</a></li>
                                        @endif
                                        @if($job->status == 'RECRUITING')
                                        <li><a href="{{ action('JobsController@getStatusChange', ['id' => $job->id, 'status' => 'SHORTLIST']) }}" data-toggle="modal" data-target="#dialog"><span class="glyphicon glyphicon-ok"></span> Shortlist</a></li>
                                        <li><a href="{{ action('JobsController@getStatusChange', ['id' => $job->id, 'status' => 'CLOSED']) }}" data-toggle="modal" data-target="#dialog"><span class="glyphicon glyphicon-ok"></span> Closed</a></li>
                                        @endif
                                        @if($job->status == 'SHORTLIST')
                                        <li><a href="{{ action('JobsController@getStatusChange', ['id' => $job->id, 'status' => 'CLOSED']) }}" data-toggle="modal" data-target="#dialog"><span class="glyphicon glyphicon-ok"></span> Closed</a></li>
                                        </li>
                                        @endif
                                    @endif
                                </ul>
                            </div>
                            <div class="row">
                                <div id="messages" class="col-xs-12">
                                    {{ $messages }}
                                </div>
                                <div id="comment" class="col-xs-12">
                                    @include('site.dashboard.job_messages.create')
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="details">
<h4>{{ $job->title }}</h4>
<table class="table table-bordered table-hover">
    <tbody>
        <tr><th>Company</th><td>{{ $job->company_name(true) }}</td>
        <tr><th>Send job updates</th><td>{{ $job->email }}</td></tr>
        <tr><th>Primary Contact</th><td>{{ $job->primary_contact_name }} <br />{{ $job->primary_contact_phone }}</td></tr>
        <tr><th>Secondary Contact</th><td>{{ $job->second_contact_name }} <br />{{ $job->second_contact_phone }}</td></tr>
        <tr><th>Location</th><td>{{ $job->location() }}</td></tr>
        <tr><th>Vacancies</th><td>{{ $job->vacancy_count }}</td></tr>
        <tr><th>Salary range</th><td>{{ $job->start_salary }} - {{ $job->end_salary }}</td></tr>
        <tr><th>Keywords</th><td>{{ $job->keywords }} </td></tr>
        <tr><th>Alloted days</th><td>{{ $job->alloted_days }}</td></tr>
        <tr><th>Starts</th><td>{{ $job->start_dt }}</td></tr>
        <tr><th>Ends</th><td>{{ $job->end_dt }}</td></tr>
        <tr><th>Notes for recruiter</th><td> {{ $job->comments }}</td></tr>
    </tbody>
</table>
                        </div>
                    </div>
                </div>
                @endif
                <div class="col-sm-7">
                    
                    <article class="listing section">
                        <div class="role">
                            <h1>{{ $job->title }}</h1>
                            <hr />
                            <section id="leader" class="clearfix row">
                                <span class="col-xs-2">@if(Auth::user()->hasRole('admin'))
                 @if($job->recruitment) <span class="label label-primary">Direct Recruit</span>@endif
                 @endif
                                </span>
                                <span class="col-xs-3 location">{{ $job->location() }}</span>
                                <span class="col-xs-2 {{ $job->job_type_classes() }}">{{ $job->preferred_job_type() }}</span>
                                @if(Auth::user()->hasRole('admin') || $job->company_id == Auth::user()->id)
                                <span class="col-xs-2 label label-{{ $job->get_label() }}">{{ $job->status }}</span>
                                @endif
                                <span class="pull-right col-xs-3">{{ $job->created_at->format('F j, Y') }}</span>
                            </section>
                            <section id="description">
                                {{ $job->description }}
                            </section>
                            <p id="applynote">
                            </p>
                            
                        </div>
                    </article>    
                </div>
            </div>
        </div> <!-- tab-content -->
    </div> <!-- container -->


    <div id="dialog" class="modal fade in">
        <div class="modal-dialog">
            <div class="modal-content">
                <p style="text-align: center; margin-top: 2em;">
                    <img src="{{ asset('assets/img/loading.gif') }}" alt="Please wait. Loading..." />
                    <br />
                    Please wait a moment. Loading...
                </p>
            </div>
        </div>
    </div>
    @include('site.layouts.common-footer')
    @section('scripts')
    <script type="text/javascript">
        $(document).on('job.approved', function(e, response) {
            window.location.reload();
        });
    </script>
    <!-- /scripts -->
</body>
</html>
