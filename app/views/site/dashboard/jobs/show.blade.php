<!DOCTYPE html>
<html>
<head>
    <title>
       {{{ $job->title }}} - {{{ $job->company_name() }}}
    </title>
    @include('site.layouts.common-header')
</head>
<body class="dashboard">
    <div class="container">
<div class="page-header clearfix">
    <div class="row">
        <div class="col-md-7">
            <h1 class="logo-title pull-left">Choice Business Solutions Limited</h1>
            <div class="pull-right">
            @if(!Auth::guest())
                <div class="btn-group">
                    <a class="btn btn-default" href="{{ URL::to('/') }}">Back to profile</a>
                    <a class="btn btn-default" href="{{{ action('UserController@getLogout') }}}">Log out</a>
                </div>
            @endif
            </div>
        </div>
    </div>
</div>
        <div class="tab-content">

            <div class="row tab-pane active">
                <div class="col-xs-12">
                    <div class="company">
                        <a class="avatar" href="#"></a>
                        <div class="title">
                            <span>
                            <hgroup>
                                <h2>{{ $job->company_name() }}</h2>
                                <h3 class="tagline">{{ $job->company_tagline() }}</h3>
                            </hgroup>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-5 pull-right">
                    <!-- Where the help stuff should go -->
                    @if(Auth::guest())
                    <h3>How to apply</h3>
                    <p>Log in to your account or create one to apply for this position.</p>
                    <p><a class="btn btn-default" href="/user/login?to=/jobs/{{$job->id}}/show">I want to log in to my account </a></p>
                    <p><a href="/start" class="btn btn-primary">I want to register now</a></p>
                    @else
                    <a data-target="#dialog" data-toggle="modal" href="{{ action('ApplicantsController@create', $job->id) }}" title="Apply for {{ $job->title }}" class="btn btn-primary btn-lg">Apply for this position</a>
                    @endif
                </div>
                <div class="col-md-7 col-xs-12">
                    
                    <article class="listing section">
                        <div class="role">
                            <h1>{{ $job->title }}</h1>
                            <hr />
                            <section id="leader" class="clearfix row">
                                <span class="col-xs-4 location">{{ $job->location() }}</span>
                                <span class="col-xs-2 {{ $job->job_type_classes() }}">{{ $job->preferred_job_type() }}</span>
                                <span class="pull-right col-xs-3">{{ $job->created_at->format('F j, Y') }}</span>
                            </section>
                            <section id="description">
                                {{ $job->description }}
                            </section>
                            <p id="applynote">
                            </p>
                            
                        </div>
                    </article>    
                </div>
            </div>
        </div> <!-- tab-content -->
    </div> <!-- container -->


    <div id="dialog" class="modal fade in">
        <div class="modal-dialog">
            <div class="modal-content">
                <p style="text-align: center; margin-top: 2em;">
                    <img src="{{ asset('assets/img/loading.gif') }}" alt="Please wait. Loading..." />
                    <br />
                    Please wait a moment. Loading...
                </p>
            </div>
        </div>
    </div>
    @include('site.layouts.common-footer')
    <!-- /scripts -->
</body>
</html>
