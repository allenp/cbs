{{ Form::model($job, array('action' => array('JobsController@postStatusChange', $job->id), 'data-async' => 'true', 'data-target' => '#dialog', 'class' => 'form-vertical')) }}
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Change job post status to {{ $status }}?</h4>
    </div>
    <div class="modal-body clearfix">
        {{ Form::hidden('id') }}
        {{ Form::hidden('status', $status) }}
        <p>You are about to change the status of this listing <strong>{{{ $job->title }}} to: {{{ $status }}}<br />
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {{ Form::submit( ucfirst($status) , array('class' => 'btn btn-primary')) }}
        </div>
    </div>
{{ Form::close() }}
