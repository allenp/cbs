<!DOCTYPE html>
<html>
<head>
    <title>
       Find Jobs 
    </title>
    @include('site.layouts.common-header')
</head>
<body class="dashboard">
    <div class="container">
        <div class="page-header clearfix">
            <div class="row">
                <div class="col-md-8">
                    <h1 class="logo-title pull-left">Choice Business Solutions Limited</h1>
                    <div class="pull-right">
                    @if(Auth::guest())
                    @else
                        <div class="btn-group">
                            <a class="btn btn-default" href="{{ URL::to('/') }}">Back to profile</a>
                            <a class="btn btn-default" href="{{{ action('UserController@getLogout') }}}">Log out</a>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="tab-content">
            <div class="row tab-pane active" id="tab1">
                <div class="col-xs-12 col-md-4 pull-right">
                    <!-- Where the help stuff should go -->
                </div>
                <div class="col-md-8 col-xs-12">
                    {{ Form::open(array('method' => 'GET', 'class' => 'form-horizontal')) }}
                        <div class="form-group">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="col-xs-6">
                                        {{ Form::text('search', Input::get('search'), array('class' => 'form-control', 'placeholder' => '')) }}
                                        <p class="help-block">Search by job title</p>
                                    </div>
                                    <div class="col-xs-4">
                                        {{ Form::text('location', Input::get('location'), array('class' => 'form-control')) }}
                                        <p class="help-block">Search by location</p>
                                    </div>
                                    <div class="col-xs-2">
                                        <input type="submit" value="Search" class="btn btn-block btn-default" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    {{ Form::close() }}
                    <div class="panel panel-default row">
                    <div class="panel-body">
                    <ul id="listings">
                        @foreach($jobs as $job)
                        <li class="{{ $job->job_type_classes() }} clearfix">
                            <a href="{{ action('JobsController@show', $job->id) }}">
                                <span>
                                    <img src="/assets/images/default-company.png" width="32px" height="32px" alt>
                                    <div class="role">
                                        <h3>{{ $job->title }}</h3>
                                        @if (strlen($job->company_name()))
                                        <h4>{{ $job->company_name() }}</h4>
                                        <span>{{ $job->company_tagline() }}</span>
                                        @endif
                                    </div>
                                    <span class="location">
                                        {{ $job->location() }}
                                    </span>
                                    <div class="meta">
                                        @if ($job->full_time)
                                        <strong class="type">{{ $job->preferred_job_type() }}</strong>
                                        @endif
                                        @if ($job->isRecent())
                                        <span class="new">New</span>
                                        @else
                                        <span class="timestamp">{{ $job->start_dt == null ? $job->created_at->diffForHumans() : $job->start_dt->diffForHumans() }}</span>
                                        @endif
                                    </div>
                                </span>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                    </div>
                    </div>
                </div>
            </div>
        </div> <!-- tab-content -->
    </div> <!-- container -->


    <div id="dialog" class="modal fade in">
        <div class="modal-dialog">
            <div class="modal-content">
                <p style="text-align: center; margin-top: 2em;">
                    <img src="{{ asset('assets/img/loading.gif') }}" alt="Please wait. Loading..." />
                    <br />
                    Please wait a moment. Loading...
                </p>
            </div>
        </div>
    </div>
    @include('site.layouts.common-footer')
    <!-- /scripts -->
</body>
</html>
