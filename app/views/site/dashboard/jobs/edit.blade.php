@extends('site.layouts.company')

@section('title')
    Create new job listing - {{{ $user->company }}}
@stop
@section('content')
    <div class="tab-content">
        <div class="row tab-pane active" id="tab1">
            <div class="col-xs-12 col-md-4 pull-right">
                <!-- Where the help stuff should go -->
            </div>
            <div class="col-md-8 col-xs-12">
                @if ($job->id < 1)
                <ul class="steps">
                    <li class="active first">Step 1: Create Your Job Listing</li>
                    <li class="last">Step 2: Review & Post Listing</li>
                </ul>
                @else
                <h2>Edit Job Listing</h2>
                @endif
                <div class="section clearfix">
                    <div id="job">

    @if($job->id > 0)
    {{ Form::model($job, array('method' => 'PUT', 'url' => action('JobsController@update', array('id' => $job->id)))) }}
    @else
    {{ Form::model($job, array('action' => array('JobsController@store'), 'class' => 'form-vertical')) }}
    @endif

    @if (Session::has('message'))
        <div class="alert alert-warning">{{ Session::get('message') }}</div>
    @endif

    @if($errors->any())
    <div class="alert alert-warning">
        <ul>
            {{ implode('', $errors->all('<li>:message</li>'))}}
        </ul>
    </div>
    @endif

    <div class="form-group col-xs-12">
        <label for="type">Job title/position</label>
        {{ Form::text('title', Input::old('title'), array('class' => 'form-control')) }}
    </div>
    <div class="form-group col-xs-12">
        <label for="type">Description</label>
        {{ Form::textarea('description', Input::old('description'), array('class' => 'form-control editable', 'rows' => 18, 'placeholder' => 'Talk about your company; the roles, responsibilities and skills you are looking for')) }}
    </div>
    <div class="form-group col-xs-12">
        <label for="salary">Salary range</label>
        <div class="row">
            <div class="col-xs-4">
                {{ Form::text('start_salary', Input::old('start_salary'), array('class' => 'form-control', 'placeholder' => 'Minimum')) }}
            </div>
            <div class="col-xs-4">
                {{ Form::text('end_salary', Input::old('end_salary'), array('class' => 'form-control', 'placeholder' => 'Maximum')) }}
            </div>
            <div class="col-xs-4">
                {{ Form::select('currency', $currencies, Input::old('currency'), array('class' => 'form-control')) }}
            </div>
        </div>
    </div>
    <div class="form-group col-xs-6">
        <label for="location">Location</label>
        {{ Form::text('location', Input::old('location'), array('class' => 'form-control', 'placeholder' => 'Kingston, Ocho Rios, Anywhere')) }}
    </div>
    <div class="form-group col-xs-6">
        <label for="country">Which country is the job in?</label>
        {{ Form::select('country_id', $countries, Input::old('country_id'), array('class' => 'form-control', 'id' => 'country')) }}
    </div>
    <div class="form-group col-xs-6">
        <label for="email">Send email updates to</label>
        {{ Form::email('email', Input::old('email'), array('class' => 'form-control', 'placeholder' => 'email@example.com')) }}
    </div>
    
    <div class="form-group col-xs-6">
        <label for="keywords">Additional keywords that describe this job posting (optional)</label>
        {{ Form::text('keywords', Input::old('keywords'), array('class' => 'form-control', 'placeholder' => 'accounting, finance, economics')) }}
    </div>
    <div class="form-group col-xs-12  bg-info">
        <div class="checkbox opt-recruit">
            <label style="display:block">
                {{ Form::checkbox('recruitment', '1', null, array('id' => 'recruitment')) }}
                Tick here to have us pre-screen candidates on your behalf, saving you time and money.
            </label>
            <a target="_blank">Learn more about our Direct Recruitment Services</a>
        </div>
    </div>
    <div class="row">
    <div class="direct-recruitment-form col-xs-12" style="display: {{ (isset($job) && $job->recruitment == 1) || Input::old('recruitment') == 1 ? 'visible' : 'none' }}">
        <div class="col-xs-12">
            <p class="note alert alert-warning row">Note: All details below are used internally when selecting a candidate on your behalf. This information is not posted to the online Job Board.</p>
        </div>
        <div class="panel panel-default col-xs-12">
            <div class="panel-body row">
                <div class="form-group col-xs-6">
                    <label>Primary contact</label>
                    {{ Form::text('primary_contact_name', Input::old('primary_contact_name'), array('class' => 'form-control')) }}
                </div>
                <div class="form-group col-xs-6">
                    <label>Phone number / extension</label>
                    {{ Form::text('primary_contact_phone', Input::old('primary_contact_phone'), array('class' => 'form-control')) }}
                </div>
                <div class="form-group col-xs-6">
                    <label>Secondary contact</label>
                    {{ Form::text('secondary_contact_name', Input::old('secondary_contact_name'), array('class' => 'form-control')) }}
                </div>
                <div class="form-group col-xs-6">
                    <label>Phone number / extension</label>
                    {{ Form::text('secondary_contact_phone', Input::old('secondary_contact_phone'), array('class' => 'form-control')) }}
                </div>
            </div>
        </div>

        <div class="panel panel-default col-xs-12">
            <div class="panel-body row">
                <div class="form-group col-xs-6">
                    <label class="control-label">This is a...</label>
                    <label class="form-control">{{ Form::radio('vacancy_type', 'NEW', Input::old('vacancy_type') == 'NEW', array('id' => 'new_vacancy')) }} New position in the company</label>
                    <label class="form-control">{{ Form::radio('vacancy_type', 'REPLACEMENT', Input::old('vacancy_type') == 'REPLACEMENT', array('id' => 'replacement')) }} Replacement for existing position</label>
                </div>
                <div class="form-group col-xs-3">
                    <label class="control-label">it is...</label><br />
                    <label class="form-text">{{ Form::checkbox('permanent', 1, Input::old('permanent') == 1) }} Permanent</label>
                    <label class="form-text">{{ Form::checkbox('temporary', 1, Input::old('temporary') == 1) }} Temporary</label>
                    <label class="form-text">{{ Form::checkbox('full_time', 1, Input::old('full_time') == 1) }} Full-Time</label>
                    <label class="form-text">{{ Form::checkbox('part_time', 1, Input::old('part_time') == 1) }} Part-Time</label>
                </div>
                <div class="form-group col-xs-3">
                    <label>Number of vacancies</label>
                    {{ Form::select('vacancy_count', array_combine(range(1, 15), range(1, 15)), Input::old('vacancy_count'), array('class' => 'form-control')) }}
                </div>
            </div>
        </div>
        
        <div class="new-position form-group col-xs-12" style="display: {{ Input::old('vacancy_type') == 'NEW' ? 'block' : 'none' }}">
            <label>What is the aim for the position?</label>
            {{ Form::textarea('aim_for_position', Input::old('aim_for_position'), array('class' => 'form-control', 'rows' => '4', 'placeholder' => 'Please state the aim behind this new position')) }}
        </div>
        <div class="replacement-position form-group col-xs-12" style="display: {{ Input::old('vacancy_type') == 'REPLACEMENT' ? 'block' : 'none' }}">
            <label>Please list the names of the last two (2) incumbents</label>
        </div>
        <div class="replacement-position form-group col-xs-6" style="display: {{ Input::old('vacancy_type') == 'REPLACEMENT' ? 'block' : 'none' }}">
            {{ Form::text('last_incumbent1', Input::old('last_incumbent1'), array('class' => 'form-control')) }}
        </div>
        <div class="replacement-position form-group col-xs-6" style="display: {{ Input::old('vacancy_type') == 'REPLACEMENT' ? 'block' : 'none' }}">
            {{ Form::text('last_incumbent2', Input::old('last_incumbent2'), array('class' => 'form-control')) }}
        </div>
        <div class="replacement-position form-group col-xs-12" style="display: {{ Input::old('vacancy_type') == 'REPLACEMENT' ? 'block' : 'none' }}">
            <label>Reason the position is vacant</label>
            {{ Form::textarea('reason_vacant', Input::old('reason_vacant'), array('class' => 'form-control editable', 'rows' => 4)) }}
        </div>
        <div class="form-group col-xs-12">
            <label>What is the personality profile of their immediate supervisor?</label>
            {{ Form::textarea('supervisor_profile', Input::old('supervisor_profile'), array('class' => 'form-control editable', 'rows' => 4)) }}
        </div>
        <div class="form-group col-xs-12">
            <label>What are the expectations of their immediate supervisor?</label>
            {{ Form::textarea('supervisor_expectation', Input::old('supervisor_expectation'), array('class' => 'form-control editable', 'rows' => 4)) }}
        </div>
        <div class="form-group col-xs-12">
            <label>What is the company's culture?</label>
            {{ Form::textarea('company_culture', Input::old('company_culture'), array('class' => 'form-control editable', 'rows' => 4)) }}
            <hr />
        </div>
        <div class="form-group col-xs-6">
            <div class="checkbox">
                <label>
                    {{ Form::checkbox('advertised_before', Input::old('advertised_before'), Input::old('advertised_before') == 'yes', array('id' => 'advertised_before')) }} Tick if you have advertised this position before.
                </label>
            </div>
        </div>
        <div class="form-group col-xs-6 advertised_before" style="display: {{ Input::old('advertised_before') == 'yes' ? 'block' : 'none' }}">
            <label>Date advertised</label>
            {{ Form::text('advertised_dt', Input::old('advertised_dt'), array('class' => 'form-control')) }}
        </div>
        <div class="form-group col-xs-6">
            <div class="checkbox">
                <label>
                    {{ Form::checkbox('agency_before', Input::old('agency_before'), Input::old('agency_before') == 'yes', array('id' => 'agency_before')) }} Tick if any other agency been asked to fill this position before.
                </label>
            </div>
        </div>
        <div class="form-group col-xs-6 agency_before" style="display: {{ Input::old('advertised_before') == 'yes' ? 'block' : 'none' }}">
            <label>Which agency?</label>
            {{ Form::text('other_agency', Input::old('other_agency'), array('class' => 'form-control')) }}
        </div>
        
        <div class="form-group col-xs-12">
            <hr />
            <label class="control-label">Are there any special working conditions?</label>
            {{ Form::textarea('work_conditions', Input::old('work_conditions'), array('class' => 'form-control', 'rows' => 3)) }}
        </div>
        <div class="form-group col-xs-12">
            <label for="comments">Special instructions to recruiters</label>
        {{ Form::textarea('comments', Input::old('comments'), array('class' => 'form-control editable', 'rows' => 4, 'placeholder' => 'Any special instructions?')) }}
        </div>
    </div>
    </div>
    <div class="form-group col-xs-12"> 
        <hr />
        {{ Form::submit('Save Draft', array('class' => 'btn btn-default btn-lg', 'name' => 'draft')) }}
<!--
        {{ Form::submit('Review & Post', array('class' => 'btn btn-primary btn-lg', 'name' => 'review')) }}
-->
{{ Form::close() }}
    </div>
    
            </div>
                </div>
            </div>
        </div>
    </div> <!-- tab-content -->
@stop
@section('scripts')
    <script type="text/javascript">
        $(function() {
            $('#recruitment').change(function() {
                $('.direct-recruitment-form').fadeToggle($(this).is(':checked'));
            });

            var radioToggle = function() {
                $('.new-position').toggle($('#new_vacancy').is(':checked'));
                $('.replacement-position').toggle($('#replacement').is(':checked'));
            };

            $('#advertised_before').change(function() {
                $('.advertised_before').fadeToggle($(this).is(':checked'));
            });

            $('#agency_before').change(function() {
                $('.agency_before').fadeToggle($(this).is(':checked'));
            });

            $('#new_vacancy').change(radioToggle);
            $('#replacement').change(radioToggle);

        });
    </script>
    <!-- /scripts -->
@stop
