    {{ Form::model($language, array('url' => action('LanguagesController@store'), 'data-async' => 'true', 'data-target' => '#dialog')) }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Add a new Language</h4>
        </div>
        <div class="modal-body clearfix">
        {{ HTML::ul($errors->all()) }}
            <div class="form-group col-xs-12">
                {{ Form::text('name', Input::old('name'), array('class' => 'form-control', 'placeholder' => 'Name')) }}
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {{ Form::submit('Add Language', array('class' => 'btn btn-primary')) }}
        </div>
    {{ Form::close() }}
