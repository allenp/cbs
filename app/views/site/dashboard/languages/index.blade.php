@if(count($languages) > 0)
  @foreach($languages as $language)
    @if ($viewonly)
        <a href="javascript:false;" class="btn btn-default" >{{{ $language->name }}}  </a>
    @else
        <a href="javascript:false;" class="btn btn-default confirm-delete" data-delete-url="{{ action('LanguagesController@delete', $language->id) }}">{{{ $language->name }}}  <span class="glyphicon glyphicon-trash"></span></a>
    @endif
  @endforeach
@else
    @if ($viewonly)
        <p>Applicant has not added any languages yet.</p>
    @else
        <p>You have not added any languages yet.</p>
    @endif
@endif
