@if($certification->id > 0)
{{ Form::model($certification, array('method' => 'PUT', 'url' => action('CertificationsController@update', array('id' => $certification->id)), 'data-async' => 'true', 'data-target' => '#dialog')) }}
@else
{{ Form::model($certification, array('url' => action('CertificationsController@update', array('id' => $certification->id)), 'data-async' => 'true', 'data-target' => '#dialog')) }}
@endif
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Certification</h4>
    </div>
    <div class="modal-body clearfix">
        @if($errors->count())
        <div class="alert alert-warning">
        {{ HTML::ul($errors->all()) }}
        </div>
        @endif
        <div class="form-group col-md-6">
            {{ Form::select('type', array('Training' => 'Training', 'Certification' => 'Certification', 'License' => 'License'), Input::old('type'), array('class' => 'form-control')) }}
        </div>
        <div class="form-group col-xs-6">
            {{ Form::text('year_issued', Input::old('year_issued'), array('class' => 'form-control', 'placeholder' => 'Year Issued')) }}
            <span class="help-block">Format: 2008</span>
        </div>
        <div class="form-group col-xs-12">
            {{ Form::text('name', Input::old('name'), array('class' => 'form-control', 'placeholder' => 'Name')) }}
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {{ Form::submit('Save Certification', array('class' => 'btn btn-primary')) }}
    </div>
{{ Form::close() }}
