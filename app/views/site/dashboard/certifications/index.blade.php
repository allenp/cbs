<div class="col-xs-12">
<h4>Certifications</h4>
</div>
@if(count($certifications) > 0)
@foreach($certifications as $certification)
    <div class="dp-x-lg clearfix school-info">
        <div class="col-xs-3">
            <label>{{{ $certification->year_issued }}}</label>
        </div>
        <div class="col-xs-9">
            {{{ $certification->name }}}  {{{ $certification->type }}}
            @if (!$viewonly)
            <a class="item-edit" data-toggle="modal" data-target="#dialog" href="{{ action('CertificationsController@edit', $certification->id)           }}"><span class="glyphicon glyphicon-pencil"></span></a>
            <a class="item-edit confirm-delete" href="#" data-delete-url="{{ action('CertificationsController@delete', $certification->id) }}"><span      class="glyphicon glyphicon-trash"></span></a>
            @endif
        </div>
    </div>
@endforeach
@else
    <div class="col-xs-12">
    @if ($viewonly)
        <p>Applicant has not added any certification yet.</p>
    @else
        <p>You have not added any certifications.</p>
    @endif
    </div>
@endif
