@extends('site.layouts.company')

{{-- Title --}}
@section('title')
    {{{ $user->company }}} - CBS Company Profile
@stop

@section('content')
<div class="tab-content">
    <div id="company" class="row tab-pane active">
    @include('site.dashboard.company.profile')
    </div>
</div> <!-- tab-content -->

@stop
