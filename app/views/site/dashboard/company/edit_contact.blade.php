    {{ Form::model($user, array('url' => action('CompanyController@postEdit'), 'role' => 'form', 'data-async' => 'true', 'data-target' => '#dialog')) }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Edit Primary Contact</h4>
        </div>
        <div class="modal-body clearfix">
            @if($errors->count())
            <div class="alert alert-warning">
                {{ HTML::ul($errors->all()) }}
            </div>
            @endif
           <div class="form-group col-xs-12">
                <label>Primary Contact</label>
            </div>
            <div class="form-group col-xs-12 col-sm-6">
                {{ Form::text('first_name', Input::old('first_name'), array('class' => 'form-control', 'placeholder' => 'First name')) }}
            </div>
            <div class="form-group col-xs-12 col-sm-6">
                {{ Form::text('last_name', Input::old('last_name'), array('class' => 'form-control', 'placeholder' => 'Last name')) }}
            </div>
            <div class="form-group col-xs-12 col-sm-6">
                {{ Form::text('title', Input::old('title'), array('class' => 'form-control', 'placeholder' => 'Mr. / Mrs. / Ms. / Dr.')) }}
            </div>
            <div class="form-group col-xs-12 col-sm-6">
                {{ Form::text('position', Input::old('position'), array('class' => 'form-control', 'placeholder' => 'Job title or position')) }}
            </div>
            <div class="form-group col-md-6 col-xs-12">
                <label>Company phone</label>
                {{ Form::text('workphone', Input::old('workphone'), array('class' => 'form-control', 'placeholder' => '876 555 5555')) }}
            </div>
            <div class="form-group col-md-6 col-xs-12">
                <label>Contact email</label>
                {{ Form::text('contact_email', Input::old('contact_email'), array('class' => 'form-control', 'placeholder' => 'john@company.com')) }}
            </div>
            <div class="form-group col-md-6 col-xs-12">
                <label>Fax</label>
                {{ Form::text('fax', Input::old('fax'), array('class' => 'form-control', 'placeholder' => '876 555 5555')) }}
            </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {{ Form::submit('Save Contact', array('class' => 'btn btn-primary')) }}
        </div>
    {{ Form::close() }}
