@extends('site.layouts.company')

{{-- Title --}}
@section('title')
    Job Openings for {{{ $user->company }}} - CBS Company
@stop
@section('content')
<div class="tab-content">
    <div class="row tab-pane active" id="tab1">
        <div class="col-sm-8 col-xs-12">
            <div class="section clearfix">
                <div class="col-xs-12">
                    <ul class="nav navbar-nav pull-right">
                        <li><a class="" href="{{{ action('JobsController@create') }}}" ><span class="glyphicon glyphicon-bullhorn"></span> <strong>Post new listing</strong></a></li>
                        </li>
                    </ul>
                </div>
                <div class="title">Job Posts by <strong>{{ $user->company }}</strong></div>
                <div id="jobs">
                    {{ Form::open(array('method' => 'GET', 'class' => 'form-horizontal')) }}
                        <div class="form-group">
                            <div class="col-xs-10">
                                {{ Form::text('search', Input::get('search'), array('class' => 'form-control', 'placeholder' => '')) }}
                                <p class="help-block">Search by job title</p>
                            </div>
                            <div class="col-xs-2">
                                <input type="submit" value="Search" class="btn btn-block btn-default" />
                            </div>
                        </div>
                        <hr />
                    {{ Form::close() }}
                    <ul id="listings">
                        @foreach($jobs as $job)
                        <li class="{{ $job->job_type_classes() }} clearfix">
                            <a href="{{ action('JobsController@getReview', $job->id) }}">
                                <span>
                                    <div class="role">
                                        <h3>{{ $job->title }}</h3>
                                    </div>
                                    <span class="location">{{ $job->location() }}
                                    </span>
                                    <div class="meta">
                                        @if ($job->full_time)
                                        <strong class="type">{{ $job->preferred_job_type() }}</strong>
                                        @endif
                                        @if ($job->isRecent())
                                        <span class="new">New</span>
                                        @else
                                        <span class="timestamp">{{ $job->created_at->diffForHumans() }}</span>
                                        @endif
                                        <span class="label label-{{ $job->get_label() }}">{{ $job->status }}</span>
                                    </div>
                                </span>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </div> <!-- tab-content -->
</div> <!-- container -->
@stop
