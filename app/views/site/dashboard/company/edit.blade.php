    {{ Form::model($user, array('url' => action('CompanyController@postEdit'), 'role' => 'form', 'data-async' => 'true', 'data-target' => '#dialog')) }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Edit Company Profile</h4>
        </div>
        <div class="modal-body clearfix">
            @if($errors->count())
            <div class="alert alert-warning">
                {{ HTML::ul($errors->all()) }}
            </div>
            @endif
            <div class="form-group  col-xs-12">
                {{ Form::text('company', Input::old('company'), array('class' => 'form-control', 'placeholder' => 'Company name')) }}
            </div>
            <div class="form-group  col-xs-12">
                {{ Form::text('about', Input::old('about'), array('class' => 'form-control', 'placeholder' => 'Company tag line or slogan')) }}
            </div>
            <div class="form-group col-xs-12">
                <label>Company Address</label>
            </div>
            <div class="form-group col-md-6 col-xs-12">
                {{ Form::text('address1', Input::old('address1'), array('class' => 'form-control', 'placeholder' => 'Suite / Lot #')) }}
            </div>
            <div class="form-group col-md-6 col-xs-12">
                {{ Form::text('address2', Input::old('address2'), array('class' => 'form-control', 'placeholder' => 'Location')) }}
            </div>
            <div class="form-group col-md-6 col-xs-12">
                {{ Form::text('address3', Input::old('address3'), array('class' => 'form-control', 'placeholder' => 'Town')) }}
            </div>
            <div class="form-group col-md-6 col-xs-12">
                {{ Form::text('city', Input::old('city'), array('class' => 'form-control', 'placeholder' => 'City')) }}
            </div>
            <div class="form-group col-md-6 col-xs-12">
            {{ Form::select('parish', $parish, Input::old('parish'), array('class' => 'form-control')) }} 
            </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {{ Form::submit('Save Profile', array('class' => 'btn btn-primary')) }}
        </div>
    {{ Form::close() }}
