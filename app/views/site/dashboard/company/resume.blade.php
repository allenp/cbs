{{ Form::open(array('url' => action('DashboardController@postResumeUpload', '_='. time()), 'data-async' => 'true', 'data-target' => '#dialog')) }}
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="avatarModalLabel">Upload your résumé</h4>
    </div>
    <div class="modal-body">
        @if ($errors->count())
        <div class="alert alert-warning">
            {{ HTML::ul($errors->all()) }}
        </div>
        @endif
        <p>Please upload a PDF or a Microsoft Word document.</p>
        <div id="uploader">
            <span class="btn btn-default btn-block fileinput-button">
                <i class="glyphicon glyphicon-plus"></i>
                <span>Select the document for upload</span>
                {{ Form::file('profile_resume', array('id' => 'profile_resume', 'class' => 'file-upload')) }}
                {{ Form::hidden('uploadurl', action('DashboardController@postResumeUpload', '_='.time())) }}
            </span>
            <br />
            <div id="progress" class="progress">
                <div class="progress-bar progress-bar-success"></div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
{{ Form::close() }}
