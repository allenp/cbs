        <div class="col-sm-8 col-xs-12">
            <div class="section clearfix">
                <div class="col-xs-12">
                    <h1 class="profile-title">{{{ $user->company }}}</h1>
                    {{ $user->about }}
                    <hr />
                </div>
                <div class="col-xs-12">
                    <ul class="nav navbar-nav pull-right">
                        <li><a data-toggle="modal" href="{{{ action('CompanyController@getEdit') }}}" data-target="#dialog"><span class="glyphicon glyphicon-pencil"></span> Edit profile</a></li>
                        <li><a data-toggle="modal" href="{{{ action('CompanyController@getEditContact') }}}" data-target="#dialog"><span class="glyphicon glyphicon-pencil"></span> Edit Contact</a></li>
                        <li><a class="" href="{{{ action('JobsController@create') }}}" ><span class="glyphicon glyphicon-bullhorn"></span> <strong>Post new listing</strong></a></li>
                        </li>
                    </ul>
                </div>

                <hr />

                <div id="profile" class="col-xs-12">
                    <div>
                        <h4>Primary Contact</h4>
                        {{ $user->title }} {{ $user->first_name }} {{ $user->last_name }} <br />
                        {{ $user->contact_email }} <br />
                        {{ $user->cellphone }}
                        <div class="dp-md">
                            @if (strlen($user->address1) >0)
                                <p>
                                    {{ $user->address1 }}<br />
                                    {{ $user->address2 }}<br />
                                    {{ $user->address3 }}<br /> 
                                    {{ $user->city }}<br />
                                    {{ $user->parish }}
                                </p>
                            @endif
                        </div>
                        <div>
                            @if (strlen($user->workphone) > 0)
                                <span class="tab-md">Tel: {{{ $user->workphone }}}</span>
                            @endif
                            @if(strlen($user->fax) > 0)
                                <span>Fax: {{{ $user->fax }}}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

