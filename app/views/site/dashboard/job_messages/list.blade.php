@if(count($job_messages) > 0)
    @foreach($job_messages as $jm)
    <div class="job-message">
        <div class="meta">
            <span class="who">{{ $jm->author() }}</span>
            @if(null == $jm->status)
            <span class="when"> commented {{ $jm->when() }}</span>
            @else
            <span class="when"> changed this job's status {{ $jm->when() }}</span>
            @endif
        </div>
        <div class="comment-body">
        @if(null == $jm->status)
        {{ $jm->msg }}
        @else
        Status changed to <span class="label label-{{ $jm->get_label() }}">{{ $jm->status->status }}</span>
        @endif
        </div>
        @if(null !== $jm->files)
        <div class="files">
        <table class="table">
        <tbody>
        @foreach($jm->files as $file)
        <tr>
            <td><a href="{{ url('uploads/jobs/' . $file->path) }}">{{ $file->path }}</a></td>
            <td>{{ $file->human_filesize() }}</td>
        </tr>
        @endforeach
        </tbody>
        </table>
        </div>
        @endif
    </div>
    @endforeach
@endif
