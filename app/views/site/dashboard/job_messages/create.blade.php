{{ Form::open(
    [
        'id' => 'commentform',
        'url' => action('JobMessageController@postCreate'),
        'class' => 'form-vertical',
        'data-async' => 'true',
        'data-target' => '#comment',
        'enctype' => 'multipart/form-data' 
    ]) }}
{{ Form::hidden('job_id', $job->id) }}
{{ Form::hidden('timestamp', time()) }}
<div class="form-group">
    <label>Add comment</label>
    <textarea class="form-control" name="msg" rows="8"></textarea>
</div>
<div id="filegroup" class="form-group">
<span class="btn btn-sm fileinput-button pull-right">
    <i class="glyphicon glyphicon-plus"></i>
    <span>Add files...</span>
    <!-- The file input field used as target for the file upload widget -->
    <input id="commentfile" type="file" name="files[]" multiple="">
    <ul id="file_lists">
    </ul>
</span>
</div>
<div class="form-group">
    <input type="submit" class="btn btn-default btn-sm" value="Comment" />
</div>
{{ Form::close() }}
