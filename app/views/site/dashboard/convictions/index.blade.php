<div class="col-xs-12">
@if(count($convictions) > 0)
  @foreach($convictions as $conviction)
  <div class="col-xs-3">
  <strong>
      {{{ date("F", mktime(0, 0, 0, $conviction->month_occured, 10)) }}} {{{ $conviction->year_occured }}}
</strong>
    </div>
  <div class="col-xs-9">
      {{{ $conviction->sentence }}}
        @if (!$viewonly)
              <a class="item-edit" data-toggle="modal" data-target="#dialog" href="{{ action('ConvictionController@edit', $conviction->id) }}"><span class="glyphicon glyphicon-pencil"></span></a>
              <a class="item-edit confirm-delete" href="#" data-delete-url="{{ action('ConvictionController@delete', $conviction->id) }}"><span class="glyphicon glyphicon-trash"></span></a> <br />
        @endif
      {{{ $conviction->details }}}<br />
  </div>
  @endforeach
@else
    @if($viewonly)
        <p class="">No criminal convictions added.</p>
    @else
        <p class="">If you have had any criminal convictions, please add them here.</p>
    @endif
@endif
</div>
