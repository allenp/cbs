@if($conviction->id > 0)
{{ Form::model($conviction, array('method' => 'PUT', 'url' => action('ConvictionController@update', array('id' => $conviction->id)), 'data-async' => 'true', 'data-target' => '#dialog')) }}
@else
    {{ Form::model($conviction, array('url' => action('ConvictionController@store'), 'data-async' => 'true', 'data-target' => '#dialog')) }}
@endif
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Conviction</h4>
    </div>
    <div class="modal-body clearfix">
        @if($errors->count())
        <div class="alert alert-warning">
        {{ HTML::ul($errors->all()) }}
        </div>
        @endif
        <div class="form-group col-xs-6">
            <label for="month_occured">Month of sentencing</label>
            {{ Form::select('month_occured', $months,  Input::old('month_occured'), array('class' => 'form-control', 'placeholder' => 'Month of Conviction')) }}
        </div>
        <div class="form-group col-xs-6">
            <label for="month_occured">Year of sentencing</label>
            {{ Form::text('year_occured', Input::old('year_occured'), array('class' => 'form-control', 'placeholder' => '')) }}
        </div>
        <div class="form-group col-xs-12">
            <label for="sentence">Duration of sentence</label> 
            {{ Form::text('sentence', Input::old('sentence'), array('class' => 'form-control', 'placeholder' => 'How many years, months or days')) }}
        </div>
        <div class="form-group col-xs-12">
            <label for="sentence">Details of sentence</label> 
            {{ Form::textarea('details', Input::old('details'), array('class' => 'form-control editable', 'placeholder' => 'Why were you sentenced. Did you do community service?')) }}
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {{ Form::submit('Save conviction', array('class' => 'btn btn-primary')) }}
    </div>
{{ Form::close() }}
