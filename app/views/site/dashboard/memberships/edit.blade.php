@if($membership->id > 0)
{{ Form::model($membership, array('method' => 'PUT', 'url' => action('MembershipsController@update', $membership->id), 'data-async' => 'true', 'data-target' => '#dialog')) }}
@else
{{ Form::model($membership, array('url' => action('MembershipsController@store'), 'data-async' => 'true', 'data-target' => '#dialog')) }}
@endif
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Add a new membership</h4>
    </div>
    <div class="modal-body clearfix">
        @if ($errors->count())
        <div class="alert alert-warning">
            {{ HTML::ul($errors->all()) }}
        </div>
        @endif
    <div class="form-group col-xs-6">
    <label for="name">Name of club or society</label>
    {{ Form::text('name', Input::old('name'), array('class' => 'form-control', 'placeholder' => 'John Doe')) }}
    </div>
    <div class="form-group col-xs-6">
    <label for="since">Member Since</label>
    {{ Form::text('since', Input::old('since'), array('class' => 'form-control', 'placeholder' => 'YYYY-MM-DD')) }}
    </div>
    <div class="form-group col-xs-12">
    <label>Describe your involvement</label>
    {{ Form::textarea('description', Input::old('description'), array('class' => 'form-control', 'placeholder' => 'Description')) }}
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    {{ Form::submit('Add Membership', array('class' => 'btn btn-primary')) }}
    </div>
{{ Form::close() }}

