    @if($transport->id > 0)
        {{ Form::model($transport, array('method' => 'PUT', 'url' => action('TransportsController@update', array('id' => $transport->id)), 'data-async' => 'true', 'data-target' => '#dialog')) }}
    @else
        {{ Form::model($transport, array('url' => action('TransportsController@store'), 'data-async' => 'true', 'data-target' => '#dialog')) }}
    @endif
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Transport Information</h4>
    </div>
    <div class="modal-body clearfix">
        @if($errors->count())
        <div class="alert alert-warning">
        {{ HTML::ul($errors->all()) }}
        </div>
        @endif
        <div class="form-group col-xs-12">
            <label>
            {{ Form::hidden('has_license', 0) }}
            {{ Form::checkbox('has_license', 1, Input::old('has_lincese') == true) }}
            Do you have a driver's license?
            </label>
        </div>
        <div class="form-group col-xs-6">
            <label>License number</label> 
            {{ Form::text('license_no', Input::old('license_no'), array('class' => 'form-control', 'placeholder' => 'License Number')) }}
        </div>
        <div class="form-group col-xs-6">
            <label>Date expire</label> 
            {{ Form::text('expiry_dt', Input::old('expiry_dt'), array('class' => 'form-control', 'placeholder' => 'YYYY-DD-MM')) }}
        </div>
        <div class="form-group col-xs-6">
            <label>Means of transportation to work</label>
            {{ Form::text('method', Input::old('method'), array('class' => 'form-control', 'placeholder' => 'Drive own car, public transport, walk, cycle')) }}
        </div>
        <div class="form-group col-xs-6">
            {{ Form::text('state_issued', Input::old('state_issued'), array('class' => 'form-control', 'placeholder' => 'City Of Issue')) }}
        </div>
        <div class="form-group col-xs-12">
            <div class="form-group col-xs-6">
                <label>
                {{ Form::hidden('operator', 0) }}
                {{ Form::checkbox('operator', 1, Input::old('operator') == true) }}
                Operator
                </label>
            </div>
            <div class="form-group col-xs-6">
                <label>
                {{ Form::hidden('chauffeur', 0) }}
                {{ Form::checkbox('chauffeur', 1, Input::old('chauffer') == true) }}
                Chauffeur
                </label>
            </div>
            <div class="form-group col-xs-6">
                <label>
                {{ Form::hidden('commercial', 0) }}
                {{ Form::checkbox('commercial', 1, Input::old('commercial') == true) }}
                Commercial(CDL)
                </label>
            </div>
        </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {{ Form::submit('Save transport', array('class' => 'btn btn-primary')) }}
    </div>
    {{ Form::close() }}
