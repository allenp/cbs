@if(count($transports) > 0)
  @foreach($transports as $transport)
        <div class="form-group dp-sm col-md-6 col-xs-12">
            <label>Driver's License.</label>
            <div class="clearfix">
                <label>Do you have a driver's license?</label>
                    {{ Form::checkbox('has_license', Input::old('has_lincese'), $transport->has_license, array('disabled')) }}
                <br />
                @if( $transport->has_license )
                    <span>License no: {{ $transport->license_no}}</span>
                    <span class="col-sm-offset-1">City of issue:{{ $transport->state_issued }}</span>
                @endif
            </div>
        </div>
        @if( $transport->has_license )
            <div class="form-group dp-sm col-xs-12">
                <div class="form-group col-xs-4">
                    <label>Operator</label>
                        {{ Form::checkbox('operator', Input::old('operator'), $transport->operator, array('disabled')) }}
                </div>
                <div class="form-group col-xs-4">
                    <label>Commercial(CDL)</label>
                        {{ Form::checkbox('commercial', Input::old('commercial'), $transport->commercial,  array('disabled')) }}
                </div>
                <div class="form-group col-xs-4">
                    <label>Chauffeur</label>
                        {{ Form::checkbox('chauffeur', Input::old('chauffer'), $transport->chauffer,  array('disabled')) }}
                </div>
            </div>
        @endif
  @endforeach
@else
    @if ($viewonly)
        <p>Applicant has not added any transportation information yet.</p>
    @else
        <p>You have not added any transportation information yet.</p>
    @endif
@endif
