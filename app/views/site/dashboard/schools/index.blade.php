<div class="col-xs-12">
<hr />
<h4>Schools</h4>
</div>
@if(count($schools) > 0)
    @foreach($schools as $school)
    <div class="dp-md clearfix school-info">
        <div class="col-xs-3">
            <span>{{ $school->period() }}</span>
        </div>
        <div class="col-xs-9">
            <h4>
                {{ $school->name }} 
                @if (!$viewonly)
                    <a class="item-edit" data-toggle="modal" data-target="#dialog" href="{{ action('SchoolsController@edit', $school->id) }}"><span class="glyphicon glyphicon-pencil"></span></a>
                    <a class="item-edit confirm-delete" href="#" data-delete-url="{{ action('SchoolsController@delete', $school->id) }}"><span class="glyphicon glyphicon-trash"></span></a>
                @endif
            </h4>
            <div >
                {{ $school->location }}
            </div>
            <div class="dp-sm">
                <strong>{{ $school->major }}, {{ $school->minor }}</strong>
            </div>
            <div>
                {{ $school->level_attained }}
            </div>
            {{ $school->details }}
        </div>
    </div>
    @endforeach
@else
    @if ($viewonly)
        <p class="text-center">Applicant has not added any education history yet.</p>
    @else
        <p class="text-center">You have not added your education history yet.</p>
    @endif
@endif
