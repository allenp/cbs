@if($school->id > 0)
{{ Form::model($school, array('method' => 'PUT', 'action' => array('SchoolsController@update', $school->id), 'data-async' => 'true', 'data-target' => '#dialog')) }}
@else
{{ Form::model($school, array('action' => array('SchoolsController@store'), 'data-async' => 'true', 'data-target' => '#dialog', 'class' => 'form-vertical')) }}
@endif
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Add education</h4>
    </div>
    <div class="modal-body clearfix">
        @if($errors->count())
        <div class="alert alert-warning">
            {{ HTML::ul($errors->all()) }}
        </div>
        @endif
        <div class="form-group col-xs-12">
            <label for="type">School name</label>
            {{ Form::text('name', Input::old('name'), array('class' => 'form-control', 'placeholder' => 'Name of school')) }}
        </div>
        <div class="form-group col-xs-12">
            <label for="location">Mailing address</label>
            {{ Form::text('location', Input::old('location'), array('class' => 'form-control', 'placeholder' => 'Complete mailing address for school')) }}
        </div>
        <div class="form-group col-xs-6">
            <label for="type">Type of school</label>
            {{ Form::select('type', $school_types, Input::old('type'), array('class' => 'form-control')) }}
        </div>
        
        <div class="form-group col-xs-6">
            <label>Certification attained</label>
            {{ Form::select('level_attained', $level_attained, Input::old('level_attained'), array('class' => 'form-control')) }}
        </div>
        <div class="form-group col-xs-12 col-sm-6">
            <label>Major area of study</label>
            {{ Form::text('major', Input::old('major'), array('class' => 'form-control')) }}
        </div>
        <div class="form-group col-xs-12 col-sm-6">
            <label>Minor?</label>
            {{ Form::text('minor', Input::old('minor'), array('class' => 'form-control')) }}
        </div>

        <div class="form-group col-xs-12 col-sm-6">
            <div class="row">
                <label for="started" class="col-xs-12">Started</label>
                <div class="col-xs-7">
                    {{ Form::select('start_month', $months, Input::old('start_month'), array('class' => 'form-control')) }}
                </div>
                <div class="col-xs-5">
                    {{ form::text('start_year', Input::old('start_year'), array('class' => 'form-control', 'placeholder' => 'year')) }}
                </div>
            </div>
        </div>

        <div class="form-group col-xs-12 col-sm-6">
            <div class="row">
                <label for="started" class="col-xs-12">Ended</label>
                <div class="col-xs-7">
                    {{ Form::select('end_month', $months, Input::old('end_month'), array('class' => 'form-control')) }}
                </div>
                <div class="col-xs-5">
                    {{ form::text('end_year', Input::old('end_year'), array('class' => 'form-control', 'placeholder' => 'year')) }}
                </div>
            </div>
        </div>

        <div class="form-group col-xs-12">
           <label>Details of your education attained</label>
            {{ Form::textarea('details', Input::old('details'), array('class' => 'form-control editable', 'rows' => 5, 'placeholder' => 'List your subjects or degrees here')) }}
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {{ Form::submit('Save Education', array('class' => 'btn btn-primary')) }}
    </div>
{{ Form::close() }}
