{{ Form::open(array('url' => action('DashboardController@postUpload', '_='. time()), 'data-async' => 'true', 'data-target' => '#dialog')) }}
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        @if ($type == 'resume')
        <h4 class="modal-title" id="avatarModalLabel">Upload your résumé</h4>
        @elseif ($type == 'culture')
        <h4 class="modal-title" id="avatarModalLabel">Upload your culture test</h4>
        @endif
    </div>
    <div class="modal-body">
        @if ($errors->count())
        <div class="alert alert-warning">
            {{ HTML::ul($errors->all()) }}
        </div>
        @endif
        @if ($type == 'resume')
        <p>Please upload a PDF or a Microsoft Word document.</p>
        @elseif ($type == 'culture')
        <p>Please upload the Microsoft Excel document with the Culture Test you completed.</p>
        @endif
        <div id="uploader">
            <span class="btn btn-default btn-block fileinput-button">
                <i class="glyphicon glyphicon-plus"></i>
                <span>Select the document for upload</span>
                {{ Form::file('profile_upload', array('id' => 'profile_resume', 'class' => 'file-upload')) }}
                {{ Form::hidden('uploadurl', action('DashboardController@postUpload', '_='.time())) }}
                {{ Form::hidden('type', $type) }}
            </span>
            <br />
            <div id="progress" class="progress">
                <div class="progress-bar progress-bar-success"></div>
            </div>
            <div class="errors">
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
{{ Form::close() }}
