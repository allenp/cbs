{{ Form::model($user, array('url' => action('DashboardController@postEditAddress'), 'role' => 'form', 'data-async' => 'true', 'data-target' => '#dialog')) }}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Where do you live?</h4>
</div>
<div class="modal-body clearfix">
    @if ($errors->count())
    <div class="alert alert-warning">
        {{ HTML::ul($errors->all()) }}
    </div>
    @endif
    <div class="col-sm-6 col-xs-12">
        <div class="form-group">
            <label>Present Address</label>
            {{ Form::text('address1', Input::old('address1'), array('class' => 'form-control', 'placeholder' => 'Address line 1')) }}
        </div>
        <div class="form-group">
            {{ Form::text('address2', Input::old('address2'), array('class' => 'form-control', 'placeholder' => 'Address line 2')) }}
        </div>
        <div class="form-group">
            {{ Form::text('address3', Input::old('address3'), array('class' => 'form-control', 'placeholder' => 'Address line 3')) }}
        </div>
        <div class="form-group">
            {{ Form::text('city', Input::old('city'), array('class' => 'form-control', 'placeholder' => 'City')) }}
        </div>
        <div class="form-group">
            {{ Form::select('parish', $parish, Input::old('parish'), array('class' => 'form-control')) }} 
        </div>
        <div class="form-group">
            <label>How long have you lived here?</label>
            {{ Form::text('how_long', Input::old('how_long'), array('class' => 'form-control', 'placeholder' => 'Number of years')) }}
        </div>
    </div>
    <div class="col-sm-6 col-xs-12">
        <label>Alternate Address</label>
        <div class="form-group">
            {{ Form::text('baddress1', Input::old('baddress1'), array('class' => 'form-control', 'placeholder' => 'Address line 1')) }}
        </div>
        <div class="form-group">
            {{ Form::text('baddress2', Input::old('baddress2'), array('class' => 'form-control', 'placeholder' => 'Address line 2')) }}
        </div>
        <div class="form-group">
            {{ Form::text('baddress3', Input::old('baddress3'), array('class' => 'form-control', 'placeholder' => 'Address line 3')) }}
        </div>
        <div class="form-group">
            {{ Form::text('bcity', Input::old('city'), array('class' => 'form-control', 'placeholder' => 'City')) }}
        </div>
        <div class="form-group">
            {{ Form::select('bparish', $parish, Input::old('bparish'), array('class' => 'form-control')) }} 
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary">Save Address</button>
</div>
{{ Form::close() }}
