<hr />
<h3 class="col-xs-12">{{{ $user->fullName() }}} </h3>
<div class="col-xs-12 col-sm-6 dp-md">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>
                    Sex
                </th>
                <th>
                    NIS
                </th>
                <th>
                    TRN
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    {{{ $user->sex }}}
                </td>
                <td>
                    {{{ $user->nis }}}
                </td>
                <td>
                    {{{ $user->trn }}}
                </td>
            </tr>
        </tbody>
    </table>
    <label>Contact</label>
    <div class="dp-sm">
        <span>{{{ $user->email }}}</span>
    </div>
    <div class="dp-md">
        @if (strlen($user->homephone) > 0)
        <span class="tab-md">Home: {{{ $user->homephone }}}</span>
        @endif
        @if(strlen($user->cellphone) > 0)
        <span>Cell: {{{ $user->cellphone }}}</span>
        @endif
    </div>
    <div class="dp-md">
        @if (strlen($user->address1))
        <p>
            {{ $user->address1 }}<br />
            {{ $user->address2 }}<br />
            {{ $user->address3 }}<br /> 
            {{ $user->city }}<br />
            {{ $user->parish }}
        </p>
        @endif
    </div>
    <div class="dp-sm">
        @if(strlen($user->baddress1))
        <p>
            {{ $user->baddress1 }}<br />
            {{ $user->baddress2 }}<br />
            {{ $user->baddress3 }}<br /> 
            {{ $user->bcity }}<br />
            {{ $user->bparish }}
        </p>
        @endif
    </div>
</div>

<div class="col-xs-12 col-sm-6 dp-md hidden-print">
    
    @if (strlen($user->resume) > 0)
    <div>
        <a class="btn btn-default btn-block" href="{{ url('uploads/'.$user->resume) }}">View your uploaded résumé</a>
    </div>
    @else
        @if ($viewonly)
        <div class="alert alert-warning">
            <p>Applicant has no résumé as yet</p>
        </div>
        @else
        <div class="alert alert-warning">
            <p>Please upload your résumé.</p>
        </div>
        @endif
    @endif
    @if (strlen($user->culture) > 0)
    <div class="dp-md">
        <a class="btn btn-default btn-block" href="{{ url('uploads/'.$user->culture) }}">View your uploaded culture test</a>
    </div>
    @else
        @if ($viewonly)
        <div class=" dp-md alert alert-warning">
            <p>Applicant has not completed their culture test yet.</p>
        </div>
        @else
        <div class=" dp-md alert alert-warning">
            <ul><li>Step 1: Please <a href="/culture_test.xlsx" alt="Culture Test">download our culture test</a>, complete it, then upload the results (using upload culture test button).</li></ul>
        </div>
        @endif
    @endif
    @if (strlen($user->values) || strlen($user->disc))
        @if(strlen($user->disc))
        <div class="dp-md">
            <a class="btn btn-default btn-block" href="{{ url('uploads/'.$user->disc) }}">View your uploaded DISC INDEX</a>
        </div>
        @endif
        @if(strlen($user->values))
        <div class="dp-md">
            <a class="btn btn-default btn-block" href="{{ url('uploads/'.$user->values) }}">View your uploaded VALUES INDEX</a>
        </div>
    @endif
    @else
        @if ($viewonly && strlen($user->values) == 0)
        <div class="dp-md alert alert-warning">
            <p>Applicant has not uploaded their VALUES psychometric test.</p>
        </div>
        @endif
        @if ($viewonly && strlen($user->disc) == 0)
        <div class="dp-md alert alert-warning">
            <p>Applicant has not uploaded their DISC psychometric test.</p>
        </div>
        @endif

        @if(!$viewonly)
        <div class="dp-md alert alert-warning">
            <ul>
            <li>Step 2: Please <a href="http://www.tonyrobbins.com/ue/disc-profile.php" target="_blank" alt="Psychometric Test">visit Tony Robbin's Psychometric testing website and complete the test.</a> There is no wrong answer; it is all about you as a person and your preferences. Upon completion, you will be given the option to download your DISC INDEX and your VALUES INDEX. Save them to your computer and upload them here.</li>
            </ul>
        </div>
        @endif

    @endif
</div>
