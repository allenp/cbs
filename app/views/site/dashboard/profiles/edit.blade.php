    {{ Form::model($user, array('url' => action('DashboardController@postEdit'), 'role' => 'form', 'data-async' => 'true', 'data-target' => '#dialog')) }}
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Edit Profile Information</h4>
        </div>
        <div class="modal-body clearfix">
            @if($errors->count())
            <div class="alert alert-warning">
                {{ HTML::ul($errors->all()) }}
            </div>
            @endif
            <div class="form-group col-xs-12">
                <label>Name</label>
            </div>
            <div class="form-group col-sm-6 col-xs-12">
                {{ Form::text('first_name', Input::old('first_name'), array('class' => 'form-control', 'placeholder' => 'First name')) }}
            </div>
            <div class="form-group col-sm-6 col-xs-12">
                {{ Form::text('last_name', Input::old('last_name'), array('class' => 'form-control', 'placeholder' => 'Last name')) }}
            </div>
            <div class="form-group col-sm-6 col-xs-12">
                {{ Form::text('middle_name', Input::old('middle_name'), array('class' => 'form-control', 'placeholder' => 'Middle name')) }}
            </div>
            <div class="form-group col-sm-6 col-xs-12">
                {{ Form::text('maiden_name', Input::old('maiden_name'), array('class' => 'form-control', 'placeholder' => 'Maiden name')) }}
            </div>
            <div class="form-group col-sm-6 col-xs-12">
                <label>Email address</label>
                {{ Form::email('email', Input::old('email'), array('class' => 'form-control', 'placeholder' => 'Email address')) }}
            </div>
            <div class="form-group col-xs-12 col-sm-6">
                <label style="display:block">Sex</label>
                <label class="radio">
                    {{ Form::radio('sex', 'Male', Input::old('sex') === 'Male') }}
                    Male
                </label>
                <label class="radio">
                    {{ Form::radio('sex', 'Female', Input::old('sex') === 'Female') }}
                    Female
                </label>
            </div>
            <div class="form-group col-sm-6 col-xs-12 dp-md">
                <label>TRN</label>
                {{ Form::text('trn', Input::old('trn'), array('class' => 'form-control')) }}
                <span class="help-block">9 numbers (spaces optional)</span>
            </div>
            <div class="form-group col-sm-6 col-xs-12 dp-md">
                <label>NIS</label>
                {{ Form::text('nis', Input::old('nis'), array('class' => 'form-control')) }}
                <span class="help-block">Format: K999999</span>
            </div>
            <div class="form-group col-sm-6 col-xs-12 dp-md">
                <label class="col-xs-12">Home Phone</label>
                {{ Form::text('homephone', Input::old('homephone'), array('class' => 'form-control')) }}
                <span class="help-block">Format: 1876 5551234</span>
            </div>
            <div class="form-group col-sm-6 col-xs-12 dp-md">
                <label class="col-xs-12">Cell Phone</label>
                {{ Form::text('cellphone', Input::old('cellphone'), array('class' => 'form-control')) }}
                <span class="help-block">Format: 1876 5551234</span>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {{ Form::submit('Save Profile', array('class' => 'btn btn-primary')) }}
        </div>
    {{ Form::close() }}
