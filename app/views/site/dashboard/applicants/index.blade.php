<div class="col-xs-12">
    @if(count($applicants) > 0)
    <hr />
    <h4>Jobs Applied for</h4>
    <ul>
        @foreach($applicants as $job)
        <li>{{ $job->title }}
        @if (!$viewonly)
                      <a class="item-edit" data-toggle="modal" data-target="#dialog" href="{{ action('ApplicantsController@edit', $job->id) }}"><span class="glyphicon glyphicon-pencil"></span></a>
                      <a class="item-edit confirm-delete" href="#" data-delete-url="{{ action('ApplicantsController@delete', $job->id) }}"><span class="glyphicon glyphicon-trash"></span></a>
                @endif
        </li>
        @endforeach
    </ul>
    @elseif ($viewonly)
        <p>No applications made yet.</p>
    @endif
</div>
