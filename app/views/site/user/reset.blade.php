@extends('site.layouts.login')

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('user/user.forgot_password') }}} ::
@parent
@stop

{{-- Content --}}
@section('content')
<div id="login-wrapper" class="container">
    <h1 class="logo-title">Choice Business Solutions</h1>
    <p class="text-center">Reset your password</p>
{{ Confide::makeResetPasswordForm($token)->render() }}
</div>
<div class="login-help container">
    <p class="text-center dp-md"><strong>Help: </strong><a href="{{ action('UserController@getLogin') }}">I've changed my mind. Take me back to the login page</a></p>
</div>
@stop
