@extends('site.layouts.login')

{{-- Web site Title --}}
@section('title')
@parent
@stop

{{-- Content --}}
@section('content')
<div id="login-wrapper" class="container">
    <div class="col-xs-12">
        <h1 class="logo-title">Choice Business Solutions</h1>
        @if ( Session::get('error') )
        <div class="alert alert-danger">{{ Session::get('error') }}</div>
        @endif

        @if ( Session::get('notice') )
        <div class="alert alert-info">{{ Session::get('notice') }}</div>
        @endif
        <p class="text-center">Sign in to your CBS Profile</p>
        {{ Form::open(array('url' => action('UserController@postLogin'), 'class' => 'form-vertical')) }}
            <div class="form-group">
                {{ Form::email('email', Input::old('email'), array('class' => 'form-control', 'placeholder' => 'Email address', 'autocomplete' => 'on', 'autocapitalize' => 'off', 'autocorrect' => 'off')) }}
            </div>
            <div class="form-group">
                {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password', 'autocomplete' => 'on', 'autocapitalize' => 'off', 'autocorrect' => 'off')) }}
            </div>
            <div class="checkbox">
                <label>{{ Form::checkbox('remember', Input::old('remember')) }} Remember me on this computer</label>
            </div>
            <hr />
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </div>
        {{ Form::close() }}
    </div>
</div>
<div class="login-help container">
    <p class="text-center dp-md"><strong>Help: </strong></em><a href="{{ action('UserController@getForgot') }}">I can't sign in to my CBS Profile</a></p>
</div>
@stop
