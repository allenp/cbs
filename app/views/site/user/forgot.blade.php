@extends('site.layouts.login')

{{-- Web site Title --}}
@section('title')
{{{ Lang::get('user/user.forgot_password') }}} ::
@parent
@stop

{{-- Content --}}
@section('content')
<div id="login-wrapper" class="container">
    <h1 class="logo-title">Choice Business Solutions</h1>
    <p class="text-center">Recover your password</p>
    {{ Form::open(array('url' => action('UserController@postForgot'), 'class' => 'form-vertical')) }}
        <div class="form-group">
            {{ Form::email('email', Input::old('email'), array('class' => 'form-control', 'placeholder' => 'Enter email address', 'autocomplete' => 'on', 'autocapitalize' => 'off', 'autocorrect' => 'off')) }}
        </div>
        <div class="form-group">
            <button class="btn btn-lg btn-primary btn-block" type="submit">Recover my password</button>
        </div>
    {{ Form::close() }}
</div>
<div class="login-help container">
    <p class="text-center dp-md"><strong>Help: </strong><a href="{{ action('UserController@getLogin') }}">Take me back to the login page</a></p>
</div>
@stop
