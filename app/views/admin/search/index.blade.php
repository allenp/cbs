@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
	{{{ $title }}} :: @parent
@stop

{{-- Content --}}
@section('content')
<div class="page-header">
<h3>
    Search based on skills, education
</h3>
</div>
<table id="skills" class="table table-bordered table-hover">
<thead>
<tr>
    <th>Id</th>
    <th>First Name</th>
    <th>Last Name</th>
    <th>Skills</th>
    <th>Major</th>
    <th>Minor</th>
    <th></th>
</tr>
</thead>
<tbody>
</tbody>
</table>

@stop

{{-- Scripts --}}
@section('scripts')
<script type="text/javascript">
var oTable;
$(document).ready(function() {
oTable = $('#skills').dataTable({
    "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
    "sPaginationType": "bootstrap",
    "oLanguage": {
        "sLengthMenu": "_MENU_ records per page"
    },
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": "{{ URL::to('admin/search/data') }}"/*,
    "aoColumnDefs": [
        { "bSearchable" : false, "aTargets": [ 6 ] }
        ]
     */
});
});

</script>
@stop
