	{{-- Create Role Form --}}
	<form class="form-horizontal" data-async="true" data-target="#dialog" method="post" action="{{ action('AdminRolesController@postCreate') }}" autocomplete="off">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Create Role</h4>
    </div>
    <div class="modal-body clearfix">
<!-- Tabs -->
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
			<li><a href="#tab-permissions" data-toggle="tab">Permissions</a></li>
		</ul>
	<!-- ./ tabs -->
		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- Tab General -->
			<div class="tab-pane active" id="tab-general">
				<!-- Name -->
				<div class="form-group {{{ $errors->has('name') ? 'error' : '' }}}">
					<label class="col-md-2 control-label" for="name">Name</label>
                    <div class="col-md-10">
    					<input class="form-control" type="text" name="name" id="name" value="{{{ Input::old('name') }}}" />
    					{{{ $errors->first('name', '<span class="help-inline">:message</span>') }}}
                    </div>
				</div>
				<!-- ./ name -->
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
			</div>
			<!-- ./ tab general -->

	        <!-- Permissions tab -->
	        <div class="tab-pane" id="tab-permissions">
                <div class="form-group">
                    @foreach ($permissions as $permission)
                    <label>
                        <input class="control-label" type="hidden" id="permissions[{{{ $permission['id'] }}}]" name="permissions[{{{ $permission['id'] }}}]" value="0" />
                        <input class="form-control" type="checkbox" id="permissions[{{{ $permission['id'] }}}]" name="permissions[{{{ $permission['id'] }}}]" value="1"{{{ (isset($permission['checked']) && $permission['checked'] == true ? ' checked="checked"' : '')}}} />
                        {{{ $permission['display_name'] }}}
                    </label>
                    @endforeach
                </div>
	        </div>
	        <!-- ./ permissions tab -->
		</div>
		<!-- ./ tabs content -->
        </div> <!-- modal content -->

		<!-- Form Actions -->
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {{ Form::submit('Create Role', array('class' => 'btn btn-primary')) }}
        </div>
		<!-- ./ form actions -->
	</form>
