{{ Form::model($role, array('action' => array('AdminRolesController@postDelete', $role->id), 'data-async' => 'true', 'data-target' => '#dialog', 'class' => 'form-vertical')) }}
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Delete Role?</h4>
    </div>
    <div class="modal-body clearfix">
        {{ Form::hidden('id') }}
        <p>Are you sure you want to delete this role? <br />
        <strong>{{{ $role->name }}}</strong>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {{ Form::submit('Delete Role Now', array('class' => 'btn btn-danger')) }}
        </div>
    </div>
{{ Form::close() }}
