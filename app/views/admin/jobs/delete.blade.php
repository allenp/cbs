{{ Form::model($job, array('action' => array('AdminJobsController@postDelete', $job->id), 'data-async' => 'true', 'data-target' => '#dialog', 'class' => 'form-vertical')) }}
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Delete Job?</h4>
    </div>
    <div class="modal-body clearfix">
        {{ Form::hidden('id') }}
        <p>Are you sure you want to delete this job? <br />
        <strong>{{{ $job->title }}}</strong>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {{ Form::submit('Delete Job Now', array('class' => 'btn btn-danger')) }}
        </div>
    </div>
{{ Form::close() }}
