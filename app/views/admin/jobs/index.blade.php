@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
	{{{ $title }}} :: @parent
@stop

{{-- Content --}}
@section('content')
	<div class="page-header">
		<h3>
			Job Postings
			<div class="pull-right">
				<a href="{{{ URL::to('jobs/create') }}}" class="btn btn-small btn-info"><span class="glyphicon glyphicon-plus-sign"></span> Create</a>
			</div>
		</h3>
	</div>

	<table id="jobs" class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="col-md-5">Job Title</th>
				<th class="col-md-2">Status</th>
				<th class="col-md-3">Created</th>
				<th class="col-md-2">Actions</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
@stop

{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
				oTable = $('#jobs').dataTable( {
				"sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
				"sPaginationType": "bootstrap",
				"oLanguage": {
					"sLengthMenu": "_MENU_ records per page"
				},
				"bProcessing": true,
		        "bServerSide": true,
		        "sAjaxSource": "{{ URL::to('admin/jobs/data') }}",
		        "fnDrawCallback": function ( oSettings ) {
	     		}
			});
		});

        $(document).on('jobs.created jobs.deleted jobs.updated', function(e, response) {
            oTable.fnReloadAjax();
            $dialog = $('#dialog');
            if ($dialog.hasClass('in')) {
                $dialog.modal('hide');
            }
        });
	</script>
@stop
