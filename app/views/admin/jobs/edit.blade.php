@if($job->id > 0)
{{ Form::model($job, array('method' => 'POST', 'action' => array('AdminJobsController@postEdit', $job->id), 'data-async' => 'true', 'data-target' => '#dialog')) }}
@else
{{ Form::model($job, array('action' => array('AdminJobsController@postCreate'), 'data-async' => 'true', 'data-target' => '#dialog', 'class' => 'form-vertical')) }}
@endif
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Add job</h4>
    </div>
    <div class="modal-body clearfix">
        @if($errors->count())
        <div class="alert alert-warning">
            {{ HTML::ul($errors->all()) }}
        </div>
        @endif
        <div class="form-group col-xs-12">
            <label for="type">Job title</label>
            {{ Form::text('title', Input::old('title'), array('class' => 'form-control', 'placeholder' => 'Title of job')) }}
        </div>
        <div class="form-group col-xs-12">
            <label for="type">Description</label>
            {{ Form::textarea('roles', Input::old('roles'), array('class' => 'form-control editable', 'rows' => 4, 'placeholder' => 'Description of opening')) }}
        </div>
       <div class="form-group col-xs-12">
            <label for="type">Qualifications Required</label>
            {{ Form::textarea('requires', Input::old('requires'), array('class' => 'form-control editable', 'rows' => 4, 'placeholder' => 'Qualifications required')) }}
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        {{ Form::submit('Save Job', array('class' => 'btn btn-primary')) }}
    </div>
{{ Form::close() }}
