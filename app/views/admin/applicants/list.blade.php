@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
	{{{ $title }}} :: @parent
@stop

{{-- Content --}}
@section('content')
<div class="page-header">
		<h3>
		    Applicants	
		</h3>
	</div>
<table id="applicants" class="table table-striped table-hover">
<thead>
<tr><th>Id</th><th>First Name</th><th>Last Name</th><th>Email</th><th>Phone #</th><th>Resume</th><th>Job Applied for</th><th>Actions</th></tr>
</thead>
<tbody>
</tbody>
</table>
@stop

@section('scripts')
<script type="text/javascript">
var oTable;
$(document).ready(function() {
oTable = $('#applicants').dataTable({
    "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
    "sPaginationType": "bootstrap",
    "oLanguage": {
        "sLengthMenu": "_MENU_ records per page"
    },
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": "{{ URL::to('admin/applicants/data') }}"/*,
    "aoColumnDefs": [
        { "bSearchable" : false, "aTargets": [ 6 ] }
        ]
     */
});
});

</script>
@stop
