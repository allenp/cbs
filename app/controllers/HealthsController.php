<?php

class HealthsController extends BaseController {

    public $health = null;
    public $user = null;

    public function __construct(Health $health, User $user)
    {
        $this->health = $health;
        $this->user = $user;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $healths = $this->health->where('user_id', '=', $this->user->currentUser()->id)->get();
        $viewonly = false;
        return View::make('site.dashboard.healths.index', compact('healths', 'viewonly'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $healths = $this->health->where('user_id', '=', $this->user->currentUser()->id)->get();
        if ($healths->isEmpty()  ){
            $health = $this->health;
        } else {
            $health = $healths[0];
        }
        return View::make('site.dashboard.healths.edit', compact('health'));

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
        $validator = Validator::make(Input::all(), $this->health->getRules());

        if ($validator->fails()) {
            return Redirect::to('healths/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $health = $this->health->create(
                array_merge(Input::all(), array('user_id' => $this->user->currentUser()->id))
            );

            return $this->getAllHealthsAsJson('health.created');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('site.dashboard.healths.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $health = $this->health->find($id);
        return View::make('site.dashboard.healths.edit', compact('health'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $validator = Validator::make(Input::all(), $this->health->getRules());

        if ($validator->fails()) {
            return Redirect::to('healths/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            $health = $this->health
                ->where('user_id', '=', $this->user->currentUser()->id)
                ->where('id', '=', $id)
                ->first();

            $health->fill(Input::all());
            $health->save();

            return $this->getAllHealthsAsJson('health.updated');
        }
	}

    /**
     * Soft delete the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function delete($id)
    {
        $health = $this->health->where('id', '=', $id)
                              ->where('user_id', '=', $this->user->currentUser()->id);
        $health->delete();
        return $this->getAllHealthsAsJson('health.deleted');
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
        $health = $this->health->where('user_id', '=', $this->user->currentUser()->id)
                                ->where('id', '=', $id)->get();
        $health->forceDelete();
	}

    private function getAllHealthsAsJson($broadcast)
    {
        $healths = $this->health->where('user_id', '=', $this->user->currentUser()->id)->get();
        $viewonly = false;
        $ref_view =  View::make('site.dashboard.healths.index', compact('healths', 'viewonly'))->render();

        return Response::json(
            array(
                'success' => true,
                'broadcast' => $broadcast,
                'pojo' => (array)$healths,
                'html' => $ref_view
            )
        );
    }
}
