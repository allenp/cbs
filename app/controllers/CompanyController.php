<?php

class CompanyController extends BaseController {

    public $user = null;
    public $job = null;
    static $complete = array(
        'company' => 15,
        'logo' => 15,
        'address' => 20,
        'first_name' => 10,
        'last_name' => 10,
        'job_title' => 10,
        'phone' => 10,
        'email' => 10
    );

    public function __construct(User $user, Job $job)
    {
        $this->user = $user;
        $this->job = $job;
    }

    public function getIndex()
    {
        $user = $this->user->currentUser();
        return View::make('site/dashboard/company/index', compact('user'));
    }

    public function getOpenings()
    {
        $user = $this->user->currentUser();

        $job = $this->job;
        $query = $job->orderBy('jobs.created_at', 'desc');
        $query->where('company_id', '=', $user->id);

        $search = Input::get('search');
        if (strlen($search)) {
            $query->where('title', 'like', "%$search%");
        }
        $jobs = $query->get();

        return View::make('site.layouts.company', compact('user'))
            ->nest(
                'jobs',
                'site.dashboard.company.jobs',
                compact('jobs', 'user')
            );
    }

    public function getEditContact()
    {
        $user = $this->user->currentUser();
        return View::make(
            'site.dashboard.company.edit_contact',
            compact('user')
        );
    }

    public function getEdit()
    {
        $user = $this->user->currentUser();
        $parish = array(
            'Select Parish',
            'Clarendon',
            'Hanover',
            'Kingston',
            'Manchester',
            'Portland',
            'St. Andrew',
            'St. Ann',
            'St. Catherine',
            'St. Elizabeth',
            'St. James',
            'St. Mary',
            'St. Thomas',
            'Trelawny',
            'Westmoreland',
        );

        $parish = array_combine($parish, $parish);
        return View::make('site.dashboard.company.edit', compact('user', 'parish'));
    }

    public function postEdit()
    {
        $user = $this->user->currentUser();
        $rules = array(
            'company' => 'required',
        );

        $messages = array(
            'workphone.regex' => 'The work phone format is invalid. format: 876 777 7777',
            'fax.regex' => 'The fax format is invalid. format: 876 777 7777',
        );

        $validator = Validator::make(Input::all(), $rules, $messages);

        if ($validator->passes()) {
            $oldUser = clone $user;
            $user->company = Input::get('company');
            $user->about = Input::get('about');
            $user->address1 = Input::get('address1');
            $user->address2 = Input::get('address2');
            $user->address3 = Input::get('address3');
            $user->city = Input::get('city');
            $user->parish = Input::get('parish');
            $user->amend($rules);
            return $this->getCompanyAsJson('company.updated');
        } else {
            return Redirect::to('company/edit')
                ->withInput()
                ->withErrors($validator);
        }
    }

    public function postEditContact()
    {
         $user = $this->user->currentUser();
        $rules = array(
            'workphone' => 'regex:"^\(?([0-9]{3,4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$"',
            'fax' => 'regex:"^\(?([0-9]{3,4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$"',
        );

        $messages = array(
            'workphone.regex' => 'The work phone format is invalid. format: 876 777 7777',
            'fax.regex' => 'The fax format is invalid. format: 876 777 7777',
        );

        $validator = Validator::make(Input::all(), $rules, $messages);

        if ($validator->passes()) {
            $oldUser = clone $user;
            $user->first_name = Input::get('first_name');
            $user->last_name = Input::get('last_name');
            $user->contact_email = Input::get('contact_email');
            $user->fax = Input::get('fax');
            $user->title = Input::get('title');
            $user->position = Input::get('position');
            $user->workphone = Input::get('workphone');
            $user->amend($rules);
            return $this->getCompanyAsJson('company.updated');
        } else {
            return Redirect::to('company/edit')
                ->withInput()
                ->withErrors($validator);
        }
    }

    private function getCompanyAsJson($broadcast)
    {
        $user = $this->user->currentUser();
        $view = View::make('site.dashboard.company.profile', compact('user'));
        $json = $view->render();
        return Response::json(
            array(
                'success' => true,
                'broadcast' => $broadcast,
                'html' => $json
            )
        );
    }
}
