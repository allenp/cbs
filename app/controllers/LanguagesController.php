<?php

class LanguagesController extends BaseController {

    public $language = null;
    public $user = null;

    public function __construct(Language $lang, User $user)
    {
        $this->language = $lang;
        $this->user = $user;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $languages = $this->language->where('user_id', '=', $this->user->currentUser()->id)->get();
        $viewonly = false;
        return View::make('site.dashboard.languages.index', compact('languages', 'viewonly'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $language = $this->language;
        return View::make('site.dashboard.languages.create', compact('language'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
        $validator = Validator::make(Input::all(), $this->language->getRules());

        if ($validator->fails()) {
            return Redirect::to('languages/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $language = $this->language->create(
                array_merge(Input::all(), array('user_id' => $this->user->currentUser()->id))
            );

            return $this-> getAllLanguagesAsJson('language.created');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('languages.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return View::make('languages.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        $language = $this->language->where('id', '=', $id)
            ->where('user_id', '=', $this->user->currentUser()->id);
        $language->delete();
        return $this-> getAllLanguagesAsJson('language.deleted');
    }
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function destroy($id)
    {
        //
        $language = $this->language->where('user_id', '=', $this->user->currentUser()->id)
            ->where('id', '=', $id)->get();
        $language->forceDelete();
    }

    private function  getAllLanguagesAsJson($broadcast)
    {
        $languages = $this->language->where('user_id', '=', $this->user->currentUser()->id)->get();
        $viewonly = false;
        $ref_view =  View::make('site.dashboard.languages.index', compact('languages', 'viewonly'))->render();

        return Response::json(
            array(
                'success' => true,
                'broadcast' => $broadcast,
                'pojo' => (array)$languages,
                'html' => $ref_view
            )
        );
    }
}
