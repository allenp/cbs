<?php

class JobsController extends BaseController {

    protected $job = null;
    protected $user = null;

    public function __construct(Job $job, User $user)
    {
        $this->job = $job;
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $job = $this->job;
        $query = $job->orderBy('jobs.created_at', 'desc');

        $search = Input::get('search');
        $location = Input::get('location');

        $query->where('status', '=', 'APPROVED');
        $query->where('end_dt', '>=', DB::raw('FROM_UNIXTIME('.time().')'));

        if (strlen($search)) {
            $query->where('title', 'like', "%$search%");
        }

        if (strlen($location)) {
            $query->leftjoin('countries', 'countries.id', '=', "jobs.country_id");
            $query = $query->where(function($query) use($location) {
                $query->where('location', 'like', "%$location%");
                $query->orWhere('countries.name', 'like', "%$location%");
            });
        }

        $query->select('jobs.*');
        $jobs = $query->get();

        return View::make('site.dashboard.jobs.index', compact('jobs'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $job = $this->job;
        $user = $this->user->currentUser();

        //safe defaults that could save time for repeat posts
        $job->email = $user->contact_email;
        $job->primary_contact_name = $user->first_name . ' ' . $user->last_name;
        $job->primary_contact_phone = $user->workphone;

        return $this->setupEditor($job, $user);
    }

    private function setupEditor($job, $user)
    {
        //TODO: list of industries

        $temp = Country::select('id', 'name')->orderBy('default', 'DESC')->get()->toArray();
        $countries = array_column($temp, 'name', 'id');

        $temp = Currency::select('id', 'name')->orderBy('default', 'DESC')->get()->toArray();
        $currencies = array_column($temp, 'name', 'id');

        return View::make(
            'site.dashboard.jobs.edit',
            compact('job', 'user', 'countries', 'currencies')
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validator = Validator::make(Input::all(), $this->job->getRules());


        if ($validator->fails()) {
            return Redirect::to('jobs/create')
                ->withErrors($validator)
                ->withInput();
        } else {

            $job = $this->job;

            $job->fill(
                array_merge(Input::all(), ['status' => 'DRAFT'])
            );

            $job->company_id = $this->user->currentUser()->id;
            if ($job->save()) {
                return Redirect::to('jobs/'.$job->id.'/review');
            } else {
                return Redirect::to('jobs/create')
                    ->withInput()
                    ->withErrors(array('title' => 'Something went wrong!'));
            }
        }
    }

    /**
     * Review options selected. Choose payment method.
     *
     * @return Response.
     */
    public function getReview($job)
    {
        $job_messages = $job->messages()->with('files', 'status', 'user')->orderBy('id', 'asc')->get();
        return View::make('site.dashboard.jobs.review', compact('job'))
            ->nest('messages', 'site.dashboard.job_messages.list', compact('job', 'job_messages'));
    }

    public function getApply($job)
    {
        return View::make('site.dashboard.jobs.apply', compact('job'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($job)
    {
        if($job->start_dt == null) {
            $job->start_dt = Carbon::now();
        }
        return View::make('site.dashboard.jobs.show', compact('job'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $job = $this->job->find($id);
        $user = $this->user;
        return $this->setupEditor($job, $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $validator = Validator::make(Input::all(), $this->job->getRules());

        if ($validator->fails()) {
            return Redirect::to('jobs/edit/'.$id)
                ->withErrors($validator)
                ->withInput();
        } else {

            $query = $this->job->where('id', '=', $id);
            if( ! Auth::user()->hasRole('admin')) {
                $query->where(
                    'company_id', '=', $this->user->currentUser()->id);
            }

            $job = $query->first();

            if (null != $job) {
                $job->fill(Input::all());
                $job->save();
                return Redirect::to('jobs/'.$job->id.'/review');
            }
        }
    }

    public function getStatusChange($job)
    {
        $status = Input::get('status');
        return View::make(
            'site/dashboard/jobs/approve', compact('job', 'status'));
    }

    public function postStatusChange($job)
    {
        $new_status = strtoupper(Input::get('status'));
        switch ($new_status)
        {
        case 'APPROVED':
            $job->status = 'APPROVED';
            $job->start_dt = Carbon::now();
            $job->end_dt = Carbon::now()->addDays(max($job->num_days, 60));
            if($job->num_days <= 0) {
                $job->num_days = 60;
            }
            $job->save();
            break;

        case 'RECRUITING':
        case 'DRAFT':
        case 'SHORTLIST':
        case 'CLOSED':
            $job->status = $new_status;
            $job->save();
            break;
        }

        $job_message = new JobMessage();
        $job_message->user_id = Auth::user()->id;
        $job_message->job_id = $job->id;
        $job_message->msg = 'Status changed to ' . $new_status;
        $job_message->save();
        $job_message->status()->save(new JobMessageStatus(['status' => $new_status]));

        return Response::json(
                [ 
                    'success' => true,
                    'broadcast' => 'job.'.strtolower($new_status),
                    'pojo' => [],
                ]
            );
    }

    /**
     * Soft delete the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function delete($id)
    {
        $job = $this->job->where('id', '=', $id)
            ->where('user_id', '=', $this->user->currentUser()->id);

        $job->delete();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $job = $this->job
            ->where('id', '=', $id)
            ->where('user_id', '=', $this->user->currentUser()->id)
            ->get();

        $job->forceDelete();
    }
}
