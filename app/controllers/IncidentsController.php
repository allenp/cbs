<?php

class IncidentsController extends BaseController {

    public $incident = null;
    public $user = null;

    public function __construct(Incident $incident, User $user)
    {
        $this->incident = $incident;
        $this->user = $user;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $incidents = $this->incident
            ->where('user_id', '=', $this->user->currentUser()->id)
            ->orderBy('is_accident', 'desc')
            ->orderBy('year_occured', 'desc')
            ->orderBy('month_occured', 'desc')
            ->get();

        $accidents = array_filter(
            $incidents, function($item) use($incidents) {
            return $item->is_accident == 1;
        });

        $violations = array_filter(
            $incidents, function($item) use($incidents) {
            return $item->is_accident == 0;
        });

        return View::make(
            'site.dashboard.incidents.index',
            compact('accidents', 'violations')
        );
	}

    public function violation()
    {
        return $this->incident(false);
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return $this->incident(true);
	}

    private function incident($is_accident)
    {
        $incident = $this->incident;
        $years = range(date('Y', strtotime('-3 years', time())), date('Y'));

        foreach (range(1, 12) as $i) {
            $months[$i] = date("F", mktime(0,0,0, $i+1, 0,0));
        }

        return View::make(
            'site.dashboard.incidents.create',
            compact('incident', 'is_accident', 'months', 'years')
        );
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
        $validator = Validator::make(Input::all(), $this->incident->getRules());

        if ($validator->fails()) {
            $action = Input::get('is_accident') == 1 ? 'create' : 'violation';
            return Redirect::to('incidents/'.$action)
                ->withErrors($validator)
                ->withInput();
        } else {
            $incident = $this->incident->create(
                array_merge(Input::all(), array('user_id' => $this->user->currentUser()->id))
            );

            return $this->getAllIncidentsAsJson('incident.created');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('site.dashboard.incidents.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $incident = $this->incident->find($id);
        return View::make('site.dashboard.incidents.edit', compact('incident'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $validator = Validator::make(Input::all(), $this->incident->getRules());

        if ($validator->fails()) {
            return Redirect::to('incidents/edit/'.$id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $incident = $this->incident
                ->where('user_id', '=', $this->user->currentUser()->id)
                ->where('id', '=', $id)
                ->first();

            $incident->fill(Input::all());
            $incident->save();

            return $this->getAllIncidentsAsJson('incident.updated');
        }
	}

    /**
     * Soft delete the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function delete($id)
    {
        $incident = $this->incident->where('id', '=', $id)
                              ->where('user_id', '=', $this->user->currentUser()->id);
        $incident->delete();
        return $this->getAllIncidentsAsJson('incident.deleted');
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
        $incident = $this->incident->where('user_id', '=', $this->user->currentUser()->id)
                                ->where('id', '=', $id)->get();
        $incident->forceDelete();
	}

    private function getAllIncidentsAsJson($broadcast)
    {
        $incidents = $this->incident
            ->where('user_id', '=', $this->user->currentUser()->id)
            ->orderBy('is_accident', 'desc')
            ->orderBy('year_occured', 'desc')
            ->orderBy('month_occured', 'desc')
            ->get();

        $ref_view =  View::make('site.dashboard.incidents.index', compact('incidents'))->render();

        return Response::json(
            array(
                'success' => true,
                'broadcast' => $broadcast,
                'pojo' => (array)$incidents,
                'html' => $ref_view
            )
        );
    }
}
