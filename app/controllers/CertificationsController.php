<?php

class CertificationsController extends BaseController {

    public $certification = null;
    public $user = null;

    public function __construct(Certification $cert, User $user)
    {
        $this->certification = $cert;
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $certifications = $this->certification->where('user_id', '=', $this->user->currentUser()->id)->orderBy('year_issued', 'DESC')->get();
        return View::make('site.dashboard.certifications.index', compact('certifications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $certification = $this->certification;
        return View::make('site.dashboard.certifications.edit', compact('certification'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
        $validator = Validator::make(Input::all(), $this->certification->getRules());

        if ($validator->fails()) {
            return Redirect::to('certifications/create')
                ->withErrors($validator)
                ->withInput();
        }

        $certification = $this->certification->create(
            array_merge(Input::all(), array('user_id' => $this->user->currentUser()->id))
        );

        return $this->getAllCertificationsAsJson('certifications.created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return View::make('site.dashboard.certifications.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $certification = $this->certification->find($id);
        return View::make('site.dashboard.certifications.edit', compact('certification'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $validator = Validator::make(Input::all(), $this->certification->getRules());

        if ($validator->fails()) {
            return Redirect::to('certifications/edit/'.$id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $certification = $this->certification
                ->where('user_id', '=', $this->user->currentUser()->id)
                ->where('id', '=', $id)
                ->first();

            $certification->fill(Input::all());
            $certification->save();

            return $this->getAllCertificationsAsJson('certifications.updated');
        }
    }

    /**
     * Soft delete the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function delete($id)
    {
        $certification = $this->certification->where('id', '=', $id)
            ->where('user_id', '=', $this->user->currentUser()->id);
        $certification->delete();
        return $this->getAllCertificationsAsJson('certifications.deleted');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $certification = $this->certification->where('user_id', '=', $this->user->currentUser()->id)
            ->where('id', '=', $id)->get();
        $certification->forceDelete();
    }

    private function getAllCertificationsAsJson($broadcast)
    {
        $certifications = $this->certification->where('user_id', '=', $this->user->currentUser()->id)->orderBy('year_issued', 'DESC')->get();
        $viewonly = false;
        $ref_view =  View::make('site.dashboard.certifications.index', compact('certifications','viewonly'))->render();

        return Response::json(
            array(
                'success' => true,
                'broadcast' => $broadcast,
                'pojo' => (array)$certifications,
                'html' => $ref_view
            )
        );
    }
}
