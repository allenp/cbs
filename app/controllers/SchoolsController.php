<?php

class SchoolsController extends BaseController {

    protected $school = null;
    protected $user = null;

    public function __construct(School $school, User $user)
    {
        $this->school = $school;
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return View::make('schools.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $school = $this->school;

        $school_types = array(
            '',
            'High School',
            'College',
            'Business or Trade School'
        );

        $level_attained = array(
            '',
            'High School Certificate',
            'Associate Degree',
            'Degree',
            'Post Graduate'
        );

        $months = array();

        foreach (range(1, 12) as $i) {
            $months[$i] = date("F", mktime(0,0,0, $i+1, 0,0));
        }

        $school_types = array_combine($school_types, $school_types);

        $level_attained = array_combine($level_attained, $level_attained);

        return View::make(
            'site.dashboard.schools.edit',
            compact('school', 'school_types', 'level_attained', 'months')
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validator = Validator::make(Input::all(), $this->school->getRules());

        if($validator->fails()) {
            return Redirect::to('schools/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $school = $this->school->create(
                array_merge(
                    Input::all(),
                    array('user_id' => $this->user->currentUser()->id)
                )
            );

            return $this->getAllSchoolsAsJson('schools.created');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return View::make('schools.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $school = $this->school->find($id);
        $school_types = array(
            '',
            'High School',
            'College',
            'Business or Trade School'
        );

        $level_attained = array(
            '',
            'High School Certificate',
            'Associate Degree',
            'Degree',
            'Post Graduate'
        );


        $months = array();

        foreach (range(1, 12) as $i) {
            $months[$i] = date("F", mktime(0,0,0, $i+1, 0,0));
        }

        $school_types = array_combine($school_types, $school_types);

        $level_attained = array_combine($level_attained, $level_attained);

        return View::make('site.dashboard.schools.edit', compact('school', 'school_types', 'level_attained', 'months'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $validator = Validator::make(Input::all(), $this->school->getRules());

        if ($validator->fails()) {
            return Redirect::to('schools/edit/'.$id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $school = $this->school
                ->where('user_id', '=', $this->user->currentUser()->id)
                ->where('id', '=', $id)
                ->first();

            $school->fill(Input::all());
            $school->save();

            return $this->getAllSchoolsAsJson('schools.updated');
        }
    }

    /**
     * Soft delete the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function delete($id)
    {
        $school = $this->school->where('id', '=', $id)
            ->where('user_id', '=', $this->user->currentUser()->id);

        $school->delete();
        return $this->getAllSchoolsAsJson('schools.deleted');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $school = $this->school
            ->where('id', '=', $id)
            ->where('user_id', '=', $this->user->currentUser()->id)
            ->get();

        $school->forceDelete();
    }

    public function getAllSchoolsAsJson($broadcast)
    {
        $schools = $this->school
            ->where('user_id', '=', $this->user->currentUser()->id)
            ->get();

        $viewonly = false;
        $school_view = View::make('site.dashboard.schools.index', compact('schools', 'viewonly'))
            ->render();

        return Response::json(
            array(
                'success' => true,
                'broadcast' => $broadcast,
                'pojo' => (array)$schools,
                'html' => $school_view
            )
        );
    }
}
