<?php

class MembershipsController extends BaseController {

    public $membership = null;
    public $user = null;

    public function __construct(Membership $memb, User $user)
    {
        $this->membership = $memb;
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $memberships = $this->membership->where('user_id', '=', $this->user->currentUser()->id)->get();
        $viewonly = false;
        return View::make('site.dashboard.memberships.index', compact('memberships', 'viewonly'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $membership = $this->membership;
        return View::make('site.dashboard.memberships.edit', compact('membership'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
        $validator = Validator::make(Input::all(), $this->membership->getRules());

        if ($validator->fails()) {
            return Redirect::to('memberships/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $membership = $this->membership->create(
                array_merge(Input::all(), array('user_id' => $this->user->currentUser()->id))
            );

            return $this->getAllMembershipsAsJson('membership.created');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return View::make('site.dashboard.memberships.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $membership = $this->membership->find($id);
        return View::make('site.dashboard.memberships.edit', compact('membership'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $validator = Validator::make(Input::all(), $this->membership->getRules());

        if ($validator->fails()) {
            return Redirect::to('memberships/edit'.$id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $membership = $this->membership
                ->where('id', '=', $id)
                ->where('user_id', '=', $this->user->currentUser()->id)
                ->first();

            $membership->fill(Input::all());
            $membership->save();

            return $this->getAllMembershipsAsJson('membership.updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        $membership = $this->membership->where('id', '=', $id)
            ->where('user_id', '=', $this->user->currentUser()->id);
        $membership->delete();
        return $this->getAllMembershipsAsJson('membership.deleted');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $membership = $this->membership->where('user_id', '=', $this->user->currentUser()->id)
            ->where('id', '=', $id)->get();
        $membership->forceDelete();
    }

    private function  getAllMembershipsAsJson($broadcast)
    {
        $memberships = $this->membership->where('user_id', '=', $this->user->currentUser()->id)->get();
        $viewonly = false;
        $ref_view =  View::make('site.dashboard.memberships.index', compact('memberships', 'viewonly'))->render();

        return Response::json(
            array(
                'success' => true,
                'broadcast' => $broadcast,
                'pojo' => (array)$memberships,
                'html' => $ref_view
            )
        );
    }

}
