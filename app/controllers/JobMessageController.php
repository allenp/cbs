<?php

use Allenp\Helpers\String as AllenString;

class JobMessageController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getCreate($job)
    {
        return View::make('site.dashboard.job_messages.create', compact('job'));
    }

    public function postCreate()
    {
        $validator = Validator::make(Input::all(), JobMessage::$rules);

        if ($validator->fails()) {
            return Redirect::action('JobMessageController@getCreate', [Input::get('job_id')])
                ->withErrors($validator)
                ->withInput();
        }

        $msg = Input::get('msg');
        $job_id = Input::get('job_id');
        $user = Auth::user();

        $job = Job::find($job_id);

        DB::transaction(function() use($msg, $job, $job_id, $user) {

            $job_message = JobMessage::create([
                'msg' => $msg,
                'job_id' => $job_id,
                'user_id' => $user->id
                ]);

            if (Input::hasFile('files')) {
                $files = Input::file('files');
                foreach ($files as $file) {
                    $filename = $file->getClientOriginalName() . '_';
                    $filename .= $job->id . '_' . $job_message->id;
                    $filename = AllenString::slug($filename);
                    $filename .= '.' . $file->getClientOriginalExtension();

                    $cbsfile = new Attachment([
                        'user_id' => $user->id,
                        'type' => $file->getMimeType(),
                        'size' => $file->getSize(),
                        'path' => $filename,
                        ]);

                    $cbsfile->save();

                    $job_message->files()->attach($cbsfile->id);

                    $destinationPath = public_path() . '/uploads/jobs/';
                    $file->move($destinationPath, $filename);
                }
            }
        }); //Transaction

        $timestamp = Input::get('timestamp');
        if (!is_numeric($timestamp)) {
            $timestamp = time() - (10 * 60 * 60); 
        }

        $time = Carbon::createFromTimestamp($timestamp);

        $job_messages = $job->messages()
                            ->where('created_at', '>', $time)
                            ->with('files', 'status', 'user')
                            ->orderBy('id', 'asc')
                            ->get();

        $ref_view = View::make(
            'site.dashboard.job_messages.list',
            compact('job', 'job_messages'))->render();

        $comment_form = View::make(
            'site.dashboard.job_messages.create',
            compact('job'))->render();

        return Response::json(
            [
            'success' => true,
            'broadcast' => 'comment.created',
            'pojo' => ['timestamp' => time()],
            'html' => $ref_view,
            'commentform' => $comment_form
            ]
        );
    }
}
