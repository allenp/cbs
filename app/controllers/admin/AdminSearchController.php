<?php

class AdminSearchController extends AdminController {


    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     * @return Response
     */
    public function getIndex()
    {
        $title = 'Search for users';
        return View::make('admin/search/index', compact('title'));
    }

    public function getData()
    {
        $jobs = User::join(DB::raw('
            (select users.id, group_concat(distinct skills.name) skill,
                group_concat(distinct schools.major) major,
                group_concat(distinct schools.minor) minor
                from users
                left join skills on users.id = skills.user_id
                and skills.deleted_at is null
                left join schools on users.id = schools.user_id
                and schools.deleted_at is null
                group by users.id) sub_users'
            ),
            'sub_users.id', '=', 'users.id'
        )
        ->select(array(
            'users.id',
            'users.first_name',
            'users.last_name',
            'sub_users.skill',
            'sub_users.major',
            'sub_users.minor'
        ));
        return Datatables::of($jobs)
            ->add_column('actions', '<a class="btn btn-sm btn-primary" href="{{  action(\'AdminApplicantsController@getApplicant\', $id)  }}">View profile</a>')
            ->make();
    }
}
