<?php

class AdminJobsController extends AdminController {


    /**
     * Role Model
     * @var Role
     */
    protected $job;


    public function __construct(Job $job)
    {
        parent::__construct();
        $this->job = $job;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        // Title
        //$title = Lang::get('admin/roles/title.role_management');
        $title = 'Manage Jobs';

        $jobs = $this->job->all();

        // Show the page
        return View::make('admin/jobs/index', compact('jobs', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        // Title
        $title = Lang::get('admin/jobs/title.create_a_new_job');

        $job = $this->job;
        // Show the page
        return View::make('admin/jobs/edit', compact('job', 'title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate()
    {
        $validator = Validator::make(Input::all(), $this->job->getRules());
        if ($validator->passes())
        {
            $this->job->fill(Input::except('csrf_token'));
            $this->job->active = 1;
            $this->job->save();

            if ($this->job->id)
            {
                //return Redirect::to('admin/jobs/' . $this->job->id . '/edit')->with('success', Lang::get('admin/roles/messages.create.success'));
                return $this->getDataAsJson('jobs.created');
            }
            return Redirect::to('admin/jobs/create')->with('error', Lang::get('admin/roles/messages.create.error'));
        }

        return Redirect::to('admin/jobs/create')->withInput()->withErrors($validator);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return Response
     */
    public function getShow($id)
    {
        // redirect to the frontend
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $job
     * @return Response
     */
    public function getEdit($job)
    {
        if(empty($job))
        {
            // Redirect to the roles management page
            return Redirect::to('admin/jobs')->with('error', Lang::get('admin/jobs/messages.does_not_exist'));
        }

        // Title
        $title = Lang::get('admin/jobs/title.role_update');

        // Show the page
        return View::make('admin/jobs/edit', compact('job', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $role
     * @return Response
     */
    public function postEdit($job)
    {
        $validator = Validator::make(Input::all(), $this->job->getRules());
        if ($validator->passes())
        {
            $job->fill(Input::except('csrf_token'));

            if ($job->save())
            {
                //return Redirect::to('admin/jobs/' . $job->id . '/edit')->with('success', Lang::get('admin/jobs/messages.update.success'));
                return $this->getDataAsJson('jobs.updated');
            }
            else
            {
                return Redirect::to('admin/jobs/' . $job->id . '/edit')->with('error', Lang::get('admin/jobs/messages.update.error'));
            }
        }

        return Redirect::to('admin/jobs/' . $job->id . '/edit')->withInput()->withErrors($validator);
    }


    /**
     * Remove user page.
     *
     * @param $role
     * @return Response
     */
    public function getDelete($job)
    {
        // Title
        $title = Lang::get('admin/jobs/title.role_delete');

        // Show the page
        return View::make('admin/jobs/delete', compact('job', 'title'));
    }

    /**
     * Remove the specified user from storage.
     *
     * @param $role
     * @internal param $id
     * @return Response
     */
    public function postDelete($job)
    {
            // Was the job deleted?
            if($job->delete()) {
                return $this->getDataAsJson('jobs.deleted');
                //return Redirect::to('admin/jobs')->with('success', Lang::get('admin/jobs/messages.delete.success'));
            }

            // There was a problem deleting the job
            return Redirect::to('admin/jobs')->with('error', Lang::get('admin/jobs/messages.delete.error'));
    }

    /**
     * Show a list of all the roles formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $jobs = $this->job
            ->leftjoin('users', 'users.id', '=', 'jobs.company_id')
            ->orderBy('jobs.created_at', 'desc')
            ->select(array('jobs.id', 'jobs.title', 'users.company', 'jobs.status', 'jobs.created_at'));

        return Datatables::of($jobs)
            ->edit_column('title', '<h4>{{ $title }}</h4><span class="company">{{ $company }}</span>')
            ->edit_column('status', '<span class="label label-{{ get_label_for_status($status) }}">{{ $status }}</span>')
            ->edit_column('created_at','{{ Carbon::createFromFormat(\'Y-m-d H:i:s\', $created_at)->diffForHumans() }}')
            ->add_column('actions', '<a href="{{ action(\'JobsController@getReview\', $id) }}" class="btn btn-xs btn-default">Review</a>
                                <a href="{{ URL::to(\'admin/jobs/\' . $id . \'/delete\' ) }}" data-toggle="modal" data-target="#dialog" class="btn btn-xs btn-danger">archive</a>
                    ')
            ->remove_column('id')
            ->remove_column('company')
            ->make();
    }

    private function getDataAsJson($broadcast)
    {
        return Response::json(
            array(
                'success' => true,
                'broadcast' => $broadcast,
                'pojo' => [],
            )
        );
    }

}

if ( ! function_exists('get_label_for_status')) {
    /*
     * Helper function for setting the correct class
     * on the status labels in the datatable
     */
    function get_label_for_status($status)
    {
        $status = strtolower($status);
        $labels = array(
            'draft' => 'default',
            'submitted' => 'warning',
            'approved' => 'success',
            'rejected' => 'danger',
            'recruiting' => 'info',
            'shortlist' => 'primary',
            'closed' => 'primary'
        );
        return isset($labels[$status]) ? $labels[$status] : 'default';
    }
}
