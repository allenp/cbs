<?php

class AdminApplicantsController extends AdminController {

    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
        parent::__construct();
    }

    public function getIndex()
    {
        $user = $this->user->currentUser();
        $title = 'Applicants List';
        return View::make('admin.applicants.list', compact('user', 'title'));
    }

    public function getData()
    {
        $users = $this->user
            ->join(DB::raw('(select users.id, group_concat(distinct jobs.title) as title
                from users inner join applicants a on a.user_id = users.id
                inner join jobs on jobs.id = a.job_id and jobs.deleted_at is null
                where users.user_type = \'applicant\'
                group by users.id
            ) sub_users'), 'sub_users.id', '=', 'users.id')
            ->select(array(
                'users.id',
                'users.first_name',
                'users.last_name',
                'users.email',
                'users.cellphone',
                'users.resume',
                'sub_users.title'
            ));

        return Datatables::of($users)
            ->add_column('actions', '<a class="btn btn-sm btn-primary" href="{{  action(\'AdminApplicantsController@getApplicant\', $id)  }}">View profile</a>')
            ->edit_column('resume', '@if (strlen($resume))
            <div class="dp-lg">
            <a class="btn btn-default btn-sm" href="{{ url(\'uploads/\'.$resume) }}">View résumé</a>
            </div>
            @else
                <p>None yet</p>
                @endif'
            )->make();
    }

    public function getApplicant($id)
    {
        $user = User::where('id', '=', $id)->where('user_type', '=', 'applicant')->firstOrFail();
        $viewonly = ($this->user->currentUser()->id != $id);
        return $this->getUserView($user, $viewonly);
    }

    public function getDownload($id)
    {
        $user = User::where('id', '=', $id)->where('user_type', '=', 'applicant')->firstOrFail();
        $viewonly = ($this->user->currentUser()->id != $id);
        $pdf = app('dompdf');
        $pdf->loadHTML($this->getUserView($user, $viewonly));
        return $pdf->download($user->getPresenter()->fullName() .'-PROFILE.pdf');
    }

    private function getUserView($user, $viewonly = false)
    {
        $references = Reference::where('user_id', '=', $user->id)->get();
        $schools = School::where('user_id', '=', $user->id)->get();
        $workexperiences = WorkExperience::where('user_id', '=', $user->id)->get();
        $memberships = Membership::where('user_id', '=', $user->id)->get();
        $certifications = Certification::where('user_id', '=', $user->id)->orderBy('year_issued', 'DESC')->get();
        $languages = Language::where('user_id', '=', $user->id)->get();
        $skills = Skill::where('user_id', '=', $user->id)->get();
        $certifications = Certification::where('user_id', '=', $user->id)->get();
        $transports = Transport::where('user_id', '=', $user->id)->get();
        $healths = Health::where('user_id', '=', $user->id)->get();
        $convictions = Conviction::where('user_id', '=', $user->id)->get();
        $applicants = Applicant::join('jobs', 'applicants.job_id', '=', 'jobs.id')
            ->where('user_id', '=', $user->id)
            ->select(
                'applicants.id',
                'applicants.user_id',
                'applicants.job_id',
                'jobs.title',
                'applicants.desired_salary'
            )->get();

        if (!file_exists(public_path() . '/uploads/' . $user->resume)) {
            $user->resume = '';
        }

        $layout = $viewonly ? 'site.layouts.applicant' : 'site.layouts.employment';

        return View::make($layout, compact('user', 'languages'))
                    ->nest('references', 'site.dashboard.references.index', compact('references', 'viewonly'))
                    ->nest('profile', 'site.dashboard.profiles.index', compact('user', 'viewonly'))
                    ->nest('education', 'site.dashboard.schools.index', compact('schools', 'viewonly'))
                    ->nest('workexperiences', 'site.dashboard.workexperiences.index', compact('workexperiences', 'viewonly'))
                    ->nest('memberships', 'site.dashboard.memberships.index', compact('memberships', 'viewonly'))
                    ->nest('languages', 'site.dashboard.languages.index', compact('languages', 'viewonly'))
                    ->nest('skills', 'site.dashboard.skills.index', compact('skills', 'viewonly'))
                    ->nest('certifications', 'site.dashboard.certifications.index', compact('certifications', 'viewonly'))
                    ->nest('transports', 'site.dashboard.transports.index', compact('transports', 'viewonly'))
                    ->nest('healths', 'site.dashboard.healths.index', compact('healths', 'viewonly'))
                    ->nest('convictions', 'site.dashboard.convictions.index', compact('convictions', 'viewonly'))
                    ->nest('applicants', 'site.dashboard.applicants.index', compact('applicants', 'viewonly'));
    }

    public function getCreate($id)
    {
        $user = User::where('id', '=', $id)->firstOrFail();
        $skill = new Skill;
        $skill->user_id = $user->id;
        return View::make('site.dashboard.skills.admin_edit', compact('skill'));
    }

    public function postCreate()
    {
        $skill = new Skill;
        $validator = Validator::make(Input::all(), $skill->getRules());

        if ($validator->fails()) {
            return Redirect::to('admin/applicant/create/'.$Input::get('user_id'))
                ->withErrors($validator)
                ->withInput();
        } else {
            $skill = $skill->fill(
                array_merge(Input::all(), array('hidden' => true))
            );
            $skill->save();

            return $this->getAllSkillsAsJson('skills.created', Input::get('user_id'));
        }
    }

    /**
     * Soft delete the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function delete($id)
    {
        $skill = Skill::where('id', '=', $id)->first();
        if ($skill !== null) {
            $skill->delete();
            return $this->getAllSkillsAsJson('skills.deleted', $skill->user_id);
        }
    }

    private function getAllSkillsAsJson($broadcast, $user_id)
    {
        $skills = Skill::where('user_id', '=', $user_id)->get();
        $viewonly = true;
        $ref_view =  View::make('site.dashboard.skills.index', compact('skills', 'viewonly'))->render();

        return Response::json(
            array(
                'success' => true,
                'broadcast' => $broadcast,
                'pojo' => (array)$skills,
                'html' => $ref_view
            )
        );
    }
}
