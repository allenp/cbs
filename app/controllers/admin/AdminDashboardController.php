<?php

class AdminDashboardController extends AdminController {

	/**
	 * Admin dashboard
	 *
	 */
	public function getIndex()
	{
        return View::make('admin/dashboard');
	}

    public function getContacts()
    {
        $contacts = User::where('user_type', '=', 'applicant')
            ->select('email', 'first_name', 'last_name')
            ->get();

        $output = '';
        foreach($contacts as $contact) {
            $output .= implode(',', $contact->toArray()) . PHP_EOL;
        }

        $headers = array(
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="contacts-'. date('Y-m-d') . '.csv"',
        );

        return Response::make(rtrim($output, '\n'), 200, $headers);
    }
}
