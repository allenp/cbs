<?php

class ConvictionController extends BaseController {

    public $conviction = null;
    public $user = null;

    public function __construct(Conviction $conviction, User $user)
    {
        $this->conviction = $conviction;
        $this->user = $user;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $convictions = $this->conviction->where('user_id', '=', $this->user->currentUser()->id)->get();
        $viewonly = false;
        return View::make('site.dashboard.convictions.index', compact('convictions', 'viewonly'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $conviction = $this->conviction;
        $months = array();
        foreach (range(1, 12) as $i) {
            $months[$i] = date("F", mktime(0,0,0, $i+1, 0,0));
        }
        return View::make('site.dashboard.convictions.edit', compact('conviction', 'months'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
        $validator = Validator::make(Input::all(), $this->conviction->getRules());

        if ($validator->fails()) {
            return Redirect::to('conviction/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $conviction = $this->conviction->create(
                array_merge(Input::all(), array('user_id' => $this->user->currentUser()->id))
            );

            return $this->getAllConvictionsAsJson('conviction.created');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('site.dashboard.convictions.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $conviction = $this->conviction->find($id);
        $months = array();
        foreach (range(1, 12) as $i) {
            $months[$i] = date("F", mktime(0,0,0, $i+1, 0,0));
        }
        return View::make('site.dashboard.convictions.edit', compact('conviction', 'months'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $validator = Validator::make(Input::all(), $this->conviction->getRules());

        if ($validator->fails()) {
            return Redirect::to('conviction/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            $conviction = $this->conviction
                ->where('user_id', '=', $this->user->currentUser()->id)
                ->where('id', '=', $id)
                ->first();

            $conviction->fill(Input::all());
            $conviction->save();

            return $this->getAllConvictionsAsJson('conviction.updated');
        }
	}

    /**
     * Soft delete the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function delete($id)
    {
        $conviction = $this->conviction->where('id', '=', $id)
                              ->where('user_id', '=', $this->user->currentUser()->id);
        $conviction->delete();
        return $this->getAllConvictionsAsJson('conviction.deleted');
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
        $conviction = $this->conviction->where('user_id', '=', $this->user->currentUser()->id)
                                ->where('id', '=', $id)->get();
        $conviction->forceDelete();
	}

    private function getAllConvictionsAsJson($broadcast)
    {
        $convictions = $this->conviction->where('user_id', '=', $this->user->currentUser()->id)->get();
        $viewonly = false;
        $ref_view =  View::make('site.dashboard.convictions.index', compact('convictions', 'viewonly'))->render();

        return Response::json(
            array(
                'success' => true,
                'broadcast' => $broadcast,
                'pojo' => (array)$convictions,
                'html' => $ref_view
            )
        );
    }
}
