<?php

class SkillsController extends BaseController {

    public $skill = null;
    public $user = null;

    public function __construct(Skill $skill, User $user)
    {
        $this->skill = $skill;
        $this->user = $user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $skills = $this->skill->where('user_id', '=', $this->user->currentUser()->id)->get();
        $viewonly = false;
        return View::make('site.dashboard.skills.index', compact('skills', 'viewonly'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $skill = $this->skill;
        return View::make('site.dashboard.skills.edit', compact('skill'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
        $validator = Validator::make(Input::all(), $this->skill->getRules());

        if ($validator->fails()) {
            return Redirect::to('skills/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $skill = $this->skill->create(
                array_merge(Input::all(), array('user_id' => $this->user->currentUser()->id))
            );

            return $this->getAllSkillsAsJson('skills.created');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return View::make('site.dashboard.skills.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $skill = $this->skill->find($id);
        return View::make('site.dashboard.skills.edit', compact('skill'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
        $validator = Validator::make(Input::all(), $this->skill->getRules());

        if ($validator->fails()) {
            return Redirect::to('skills/edit'.$id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $skill = $this->skill
                ->where('user_id', '=', $this->user->currentUser()->id)
                ->where('id', '=', $id)
                ->first();

            $skill->fill(Input::all());
            $skill->save();

            return $this->getAllSkillsAsJson('skills.updated');
        }
    }

    /**
     * Soft delete the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function delete($id)
    {
        $skill = $this->skill->where('id', '=', $id)
            ->where('user_id', '=', $this->user->currentUser()->id);
        $skill->delete();
        return $this->getAllSkillsAsJson('skills.deleted');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $skill = $this->skill->where('user_id', '=', $this->user->currentUser()->id)
            ->where('id', '=', $id)->get();
        $skill->forceDelete();
    }

    private function getAllSkillsAsJson($broadcast)
    {
        $skills = $this->skill->where('user_id', '=', $this->user->currentUser()->id)->get();
        $viewonly = false;
        $ref_view =  View::make('site.dashboard.skills.index', compact('skills', 'viewonly'))->render();

        return Response::json(
            array(
                'success' => true,
                'broadcast' => $broadcast,
                'pojo' => (array)$skills,
                'html' => $ref_view
            )
        );
    }
}
