<?php

class ApplicantsController extends BaseController {

    public $applicant = null;
    public $user = null;
    public $jobs = null;

    public function __construct(Applicant $applicant, Job $jobs, User $user)
    {
        $this->applicant = $applicant;
        $this->user = $user;
        $this->jobs = $jobs;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $applicants = $this->applicant
            ->join('jobs', 'applicants.job_id', '=', 'jobs.id')
            ->where('user_id', '=', $this->user->currentUser()->id)
            ->orderBy('created_at')
            ->select(
                'applicants.*',
                'jobs.title'
            )->get();
        $viewonly = false;
        return View::make('site.dashboard.applicants.index', compact('applicants', 'viewonly'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($job)
	{
        $applicant = $this->applicant;

        return View::make(
            'site.dashboard.applicants.edit', compact('applicant', 'job'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $validator = Validator::make(Input::all(), $this->applicant->getRules());

        if ($validator->fails()) {
            return Redirect::to('applicants/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $applicant = $this->applicant->create(
                array_merge(Input::all(), array('user_id' => $this->user->currentUser()->id))
            );
            return $this->getAllApplicantsAdJson('applicant.created');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('site.dashboard.applicants.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $applicant = $this->applicant->find($id);
        $job = $this->jobs->find($applicant->job_id);
        return View::make('site.dashboard.applicants.edit', compact('applicant', 'job'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $validator = Validator::make(Input::all(), $this->applicant->getRules());

        if ($validator->fails()) {
            return Redirect::to('applicant/edit'.$id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $applicant = $this->applicant
                ->where('id', '=', $id)
                ->where('user_id', '=', $this->user->currentUser()->id)
                ->first();

            $applicant->fill(Input::all());
            $applicant->save();

            return $this->getAllApplicantsAdJson('applicant.updated');
        }
	}

    /**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete($id)
	{
        $applicant = $this->applicant->where('id', '=', $id)
            ->where('user_id', '=', $this->user->currentUser()->id);
        $applicant->delete();
        return $this->getAllApplicantsAdJson('applicant.deleted');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
    {
        $applicant = $this->applicant->where('id', '=', $id)
            ->where('user_id', '=', $this->user->currentUser()->id);
        $applicant->forceDelete();
	}

    private function getAllApplicantsAdJson($broadcast)
    {
        $applicants = $this->applicant
            ->join('jobs', 'applicants.job_id', '=', 'jobs.id')
            ->where('user_id', '=', $this->user->currentUser()->id)
            ->orderBy('created_at')
            ->select(
                'applicants.*',
                'jobs.title'
            )->get();
        $viewonly = false;
        $ref_view = View::make('site.dashboard.applicants.index', compact('applicants', 'viewonly'))->render();

        return Response::json(
            array(
                'success' => true,
                'broadcast' => $broadcast,
                'pojo' => (array)$applicants,
                'html' => $ref_view
            )
        );
    }

}
