<?php

class ReferencesController extends BaseController {

    public $reference = null;
    public $user = null;

    public function __construct(Reference $ref, User $user)
    {
        $this->reference = $ref;
        $this->user = $user;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $references = $this->reference->where('user_id', '=', $this->user->currentUser()->id)->get();
        $viewonly = false;
        return View::make('site.dashboard.references.index', compact('references', 'viewonly'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $reference = $this->reference;
        return View::make('site.dashboard.references.edit', compact('reference'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
        $validator = Validator::make(Input::all(), $this->reference->getRules());

        if ($validator->fails()) {
            return Redirect::to('references/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $reference = $this->reference->create(
                array_merge(Input::all(), array('user_id' => $this->user->currentUser()->id))
            );

            return $this->getAllReferencesAsJson('reference.created');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('site.dashboard.references.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $reference = $this->reference->find($id);
        return View::make('site.dashboard.references.edit', compact('reference'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $validator = Validator::make(Input::all(), $this->reference->getRules());

        if ($validator->fails()) {
            return Redirect::to('references/edit/'.$id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $reference = $this->reference
                ->where('user_id', '=', $this->user->currentUser()->id)
                ->where('id', '=', $id)
                ->first();

            $reference->fill(Input::all());
            $reference->save();

            return $this->getAllReferencesAsJson('reference.updated');
        }
	}

    /**
     * Soft delete the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function delete($id)
    {
        $reference = $this->reference->where('id', '=', $id)
                              ->where('user_id', '=', $this->user->currentUser()->id);
        $reference->delete();
        return $this->getAllReferencesAsJson('reference.deleted');
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
        $reference = $this->reference->where('user_id', '=', $this->user->currentUser()->id)
                                ->where('id', '=', $id)->get();
        $reference->forceDelete();
	}

    private function getAllReferencesAsJson($broadcast)
    {
        $references = $this->reference->where('user_id', '=', $this->user->currentUser()->id)->get();
        $viewonly = false;
        $ref_view =  View::make('site.dashboard.references.index', compact('references', 'viewonly'))->render();

        return Response::json(
            array(
                'success' => true,
                'broadcast' => $broadcast,
                'pojo' => (array)$references,
                'html' => $ref_view
            )
        );
    }
}
