<?php

class TransportsController extends BaseController {

    public $transport = null;
    public $user = null;

    public function __construct(Transport $trans, User $user)
    {
        $this->transport = $trans;
        $this->user = $user;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $transports = $this->transport->where('user_id', '=', $this->user->currentUser()->id)->get();
        $viewonly = false;
        return View::make('site.dashboard.transports.index', compact('transports', 'viewonly'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $transport = $this->transport->where('user_id', '=', $this->user->currentUser()->id)->get();
        if ($transport->isEmpty()  ){
            $transport = $this->transport;
            return View::make('site.dashboard.transports.create', compact('transport'));
        } else {
            $transport = $transport[0];
            return View::make('site.dashboard.transports.edit', compact('transport'));
        }

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
        $validator = Validator::make(Input::all(), $this->transport->getRules());

        if ($validator->fails()) {
            return Redirect::to('transports/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $transport = $this->transport->create(
                array_merge(Input::all(), array('user_id' => $this->user->currentUser()->id))
            );

            return $this->getAllTransportsAsJson('transport.created');
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('site.dashboard.transports.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $transport = $this->transport->find($id);
        return View::make('site.dashboard.transports.edit', compact('transport'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $validator = Validator::make(Input::all(), $this->transport->getRules());

        if ($validator->fails()) {
            return Redirect::to('transports/edit/'.$id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $transport = $this->transport
                ->where('user_id', '=', $this->user->currentUser()->id)
                ->where('id', '=', $id)
                ->first();

            $transport->fill(Input::all());
            $transport->save();

            return $this->getAllTransportsAsJson('transport.updated');
        }
	}

    /**
     * Soft delete the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function delete($id)
    {
        $transport = $this->transport->where('id', '=', $id)
                              ->where('user_id', '=', $this->user->currentUser()->id);
        $transport->delete();
        return $this->getAllTransportsAsJson('transport.deleted');
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
        $transport = $this->transport->where('user_id', '=', $this->user->currentUser()->id)
                                ->where('id', '=', $id)->get();
        $transport->forceDelete();
	}

    private function getAllTransportsAsJson($broadcast)
    {
        $transports = $this->transport->where('user_id', '=', $this->user->currentUser()->id)->get();
        $viewonly = false;
        $ref_view =  View::make('site.dashboard.transports.index', compact('transports', 'viewonly'))->render();

        return Response::json(
            array(
                'success' => true,
                'broadcast' => $broadcast,
                'pojo' => (array)$transports,
                'html' => $ref_view
            )
        );
    }
}
