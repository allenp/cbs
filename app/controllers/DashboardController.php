<?php

class DashboardController extends BaseController {

    public $user = null;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getIndex()
    {
        $user = $this->user->currentUser();
        return $this->getUserView($user);
    }

    public function getApplicant($id)
    {
        $user = User::where('id', '=', $id)->where('user_type', '=', 'applicant')->firstOrFail();
        $viewonly = ($this->user->currentUser()->id != $id);
        return $this->getUserView($user, $viewonly);
    }

    private function getUserView($user, $viewonly = false)
    {
        $references = Reference::where('user_id', '=', $user->id)->get();
        $schools = School::where('user_id', '=', $user->id)->get();
        $workexperiences = WorkExperience::where('user_id', '=', $user->id)->get();
        $memberships = Membership::where('user_id', '=', $user->id)->get();
        $certifications = Certification::where('user_id', '=', $user->id)->orderBy('year_issued', 'DESC')->get();
        $languages = Language::where('user_id', '=', $user->id)->get();
        $skills = Skill::where('user_id', '=', $user->id)->get();
        $certifications = Certification::where('user_id', '=', $user->id)->get();
        $transports = Transport::where('user_id', '=', $user->id)->get();
        $healths = Health::where('user_id', '=', $user->id)->get();
        $convictions = Conviction::where('user_id', '=', $user->id)->get();
        $applicants = Applicant::join('jobs', 'applicants.job_id', '=', 'jobs.id')
            ->where('user_id', '=', $user->id)
            ->select(
                'applicants.id',
                'applicants.user_id',
                'applicants.job_id',
                'jobs.title',
                'applicants.desired_salary'
            )->get();

        if (!file_exists(public_path() . '/uploads/' . $user->resume)) {
            $user->resume = '';
        }

        $layout = $viewonly ? 'site.layouts.applicant' : 'site.layouts.employment';

        return View::make($layout, compact('user', 'languages'))
                    ->nest('references', 'site.dashboard.references.index', compact('references', 'viewonly'))
                    ->nest('profile', 'site.dashboard.profiles.index', compact('user', 'viewonly'))
                    ->nest('education', 'site.dashboard.schools.index', compact('schools', 'viewonly'))
                    ->nest('workexperiences', 'site.dashboard.workexperiences.index', compact('workexperiences', 'viewonly'))
                    ->nest('memberships', 'site.dashboard.memberships.index', compact('memberships', 'viewonly'))
                    ->nest('languages', 'site.dashboard.languages.index', compact('languages', 'viewonly'))
                    ->nest('skills', 'site.dashboard.skills.index', compact('skills', 'viewonly'))
                    ->nest('certifications', 'site.dashboard.certifications.index', compact('certifications', 'viewonly'))
                    ->nest('transports', 'site.dashboard.transports.index', compact('transports', 'viewonly'))
                    ->nest('healths', 'site.dashboard.healths.index', compact('healths', 'viewonly'))
                    ->nest('convictions', 'site.dashboard.convictions.index', compact('convictions', 'viewonly'))
                    ->nest('applicants', 'site.dashboard.applicants.index', compact('applicants', 'viewonly'));
    }

    private function getProfileAsJson($broadcast)
    {
        $user = $this->user->currentUser();
        $viewonly = false;
        $view = View::make('site.dashboard.profiles.index', compact('user', 'viewonly'));
        $json = $view->render();
        return Response::json(
            array(
                'success' => true,
                'broadcast' => $broadcast,
                'html' => $json
            )
        );
    }

    public function getEdit()
    {
        $user = $this->user->currentUser();
        return View::make('site.dashboard.profiles.edit', compact('user'));
    }

    public function postEdit()
    {
        $user = $this->user->currentUser();
        $rules = array(
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'sex' => 'required',
            'cellphone' => 'regex:"^\(?([0-9]{3,4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$"',
            'homephone' => 'regex:"^\(?([0-9]{3,4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$"',
            'trn' => 'regex:"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})$"',
            'nis' => 'regex:"[a-zA-Z]{1}\d{6}$"',
        );

        $messages = array(
            'homephone.regex' => 'The homephone format is invalid. format: 1876 7777777',
            'cellphone.regex' => 'The cellphone format is invalid. format: 1876 7777777',
            'trn.regex' => 'The trn format is invalid. format: 116 999 999',
            'nis.regex' => 'The nis format is invalid. format: K999999',
        );

        $validator = Validator::make(Input::all(), $rules, $messages);

        if ($validator->passes()) {
            $oldUser = clone $user;
            $user->first_name = Input::get('first_name');
            $user->last_name = Input::get('last_name');
            $user->middle_name = Input::get('middle_name');
            $user->maiden_name = Input::get('maiden_name');
            $user->trn = Input::get('trn');
            $user->nis = Input::get('nis');
            $user->cellphone = Input::get('cellphone');
            $user->homephone = Input::get('homephone');
            $user->sex = Input::get('sex');
            $user->username = Input::get('email');
            $user->email = Input::get('email');
            $user->prepareRules($oldUser, $user);
            $user->amend($rules);
            return $this->getProfileAsJson('profile.updated');
        } else {
            return Redirect::to('profile/edit')
                ->withInput()
                ->withErrors($validator);
        }
    }

    public function getEditAddress()
    {
        $user = $this->user->currentUser();
        $parish = array(
            'Select Parish',
            'Clarendon',
            'Hanover',
            'Kingston',
            'Manchester',
            'Portland',
            'St. Andrew',
            'St. Ann',
            'St. Catherine',
            'St. Elizabeth',
            'St. James',
            'St. Mary',
            'St. Thomas',
            'Trelawny',
            'Westmoreland',
        );

        $parish = array_combine($parish, $parish);
        return View::make('site.dashboard.profiles.address', compact('user', 'parish'));
    }

    public function postEditAddress()
    {
        $user = $this->user->currentUser();
        $rules = array(
            'address1' => 'required',
            'how_long' => 'required|numeric'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes()) {
            $oldUser = clone $user;
            $user->address1 = Input::get('address1');
            $user->address2 = Input::get('address2');
            $user->address3 = Input::get('address3');
            $user->city = Input::get('city');
            $user->parish = Input::get('parish');
            $user->how_long = Input::get('how_long');
            $user->baddress1 = Input::get('baddress1');
            $user->baddress2 = Input::get('baddress2');
            $user->baddress3 = Input::get('baddress3');
            $user->bcity = Input::get('bcity');
            $user->bparish = Input::get('bparish');
            $user->prepareRules($oldUser, $user);
            $user->amend($rules);
            return $this->getProfileAsJson('profile.updated');
        } else {
            return Redirect::to('profile/address')
                ->withInput()
                ->withErrors($validator);
        }
    }

    public function getUpload($type='resume')
    {
        return View::make('site.dashboard.profiles.upload',
            compact('type'));
    }

    public function postUpload()
    {
        $option = Input::get('type');

        $rules = array(
            'type' => 'in:resume,culture,values,disc',
            'profile_upload' => 'required|mimes:doc,docx,pdf,txt,xlsx,xls|between:1,5120',
        );

        $rulesForUser = array(
            'type' => $rules['type']
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes()) {

            $user = $this->user->currentUser();
            $destinationPath = public_path() . '/uploads/';
            $document = Input::file('profile_upload');
            $extension = $document->getClientOriginalExtension();
            $filename = $user->uniqueUrl() . strtoupper($option) . '.'. $extension;
            $success = $document->move($destinationPath, $filename);
            $oldUser = clone $user;
            $user->{$option} = $filename;
            $user->prepareRules($oldUser, $user, $rulesForUser);

            if($user->amend($rulesForUser)) {
                return $this->getProfileAsJson($option.'.uploaded');
            } else {
                return Response::json(array(
                    'success' => false,
                    'errors' => $user->validationErrors->all()
                ));
            }

        } else {

            return Response::json(array(
                'success' => false,
                'errors' => $validator->errors()->all()
            ));

        }
    }
}
