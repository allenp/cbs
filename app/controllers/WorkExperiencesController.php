<?php

class WorkExperiencesController extends BaseController {

    public $workexperience = null;
    public $user = null;

    public function __construct(WorkExperience $workexperience, User $user)
    {
        $this->workexperience = $workexperience;
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $workexperience = $this->workexperience->where('user_id', '=', $this->user->currentUser()->id)->get();
        $viewonly = false;
        return View::make('site.dashboard.workexperiences.index', compact('workexperience', 'viewonly'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $workexperience = $this->workexperience;
        $months = array();

        foreach(range(1,12) as $i){
            $months[$i] = date("F", mktime(0,0,0, $i+1, 0,0));
        }

        return View::make('site.dashboard.workexperiences.edit', compact('workexperience', 'months'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
        $validator = Validator::make(Input::all(), $this->workexperience->getRules());

        if ($validator->fails()) {
            return Redirect::to('workexperiences/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $workexperience = $this->workexperience->create(
                array_merge(Input::all(), array('user_id' => $this->user->currentUser()->id))
            );

            return $this->getAllWorkExperiencesAsJson('workexperience.created');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return View::make('site.dashboard.workexperiences.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $workexperience = $this->workexperience->find($id);
        $months = array();

        foreach(range(1,12) as $i){
            $months[$i] = date("F", mktime(0,0,0, $i+1, 0,0));
        }
        return View::make('site.dashboard.workexperiences.edit', compact('workexperience', 'months'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $validator = Validator::make(Input::all(), $this->workexperience->getRules());

        if ($validator->fails()) {
            return Redirect::to('workexperiences/'.$id.'/edit/')
                ->withErrors($validator)
                ->withInput();
        } else {
            $workexperience = $this->workexperience
                ->where('user_id', '=', $this->user->currentUser()->id)
                ->where('id', '=', $id)
                ->first();

            $workexperience->fill(Input::all());
            $workexperience->save();

            return $this->getAllWorkexperiencesAsJson('workexperience.updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {
        $workexperience = $this->workexperience->where('id', '=', $id)
            ->where('user_id', '=', $this->user->currentUser()->id);
        $workexperience->delete();
        return $this->getAllWorkExperiencesAsJson('workexperience.deleted');
    }

    private function getAllWorkExperiencesAsJson($broadcast)
    {
        $workexperiences = $this->workexperience->where('user_id', '=', $this->user->currentUser()->id)->get();
        $viewonly = false;
        $ref_view =  View::make('site.dashboard.workexperiences.index', compact('workexperiences', 'viewonly'))->render();

        return Response::json(
            array(
                'success' => true,
                'broadcast' => $broadcast,
                'pojo' => (array)$workexperiences,
                'html' => $ref_view
            )
        );
    }
}
