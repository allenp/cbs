<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AssignDocuments extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'documents:assign';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $extensions = array(
            'culture' => array('xlsx'),
            'resume' => array('pdf', 'doc', 'docx'),
            'disc' => array('pdf'),
            'values' => array('pdf')
        );

        $docs = array('culture', 'resume', 'disc', 'values');

        User::chunk(50, function($users) use($extensions, $docs) {
            $this->info('Retrieving some records...');
            foreach ($users as $user) {

                $oldUser = clone $user;
                $amend = false;
                $rules = array();

                foreach ($docs as $doc) {
                    if(strlen($user->{$doc}) == 0) {
                        if($this->findUserDocument($user, $doc, $extensions[$doc]) == true) {
                            $amend = true;
                            $rules[$doc] = 'required';
                        }
                    }
                }

                if ($amend == true) {
                    $this->info('Amending record for ' . $user->uniqueUrl());
                    $user->prepareRules($oldUser, $user, $rules);
                    if ( !$user->amend($rules)) {
                        foreach($user->validationErrors->all() as $msg) {
                            $this->error($msg);
                        }
                    }
                }
            }
        });
        $this->info('Updating process completed.');
	}

    private function findUserDocument($user, $doc, $extensions)
    {
        foreach($extensions as $ext) {
            $variations = array($ext, strtoupper($ext));
            foreach($variations as $variation) {
                $filename = $user->uniqueUrl() . strtoupper($doc). '.' .$variation;
                if (file_exists(public_path() . '/uploads/' . $filename)) {
                    $user->{$doc} = $filename;
                    return true;
                }
            }
        }
        return false;
    }

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
        return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
